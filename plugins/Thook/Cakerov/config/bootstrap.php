<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/14/2014
 * Time: 9:23 PM
 */

use Cake\Core\Configure,
    Cake\Core\Configure\Engine\PhpConfig;

try {
    Configure::config('cakerov', new PhpConfig());
    Configure::load('data-types.php', 'cakerov', FALSE);
}
catch (\Exception $e) {
    die($e->getMessage());
}
