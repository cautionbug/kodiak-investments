<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/14/2014
 * Time: 8:51 PM
 */

$config = [
    'DataTypes' => [
        'Bool' => [
            'Status' => [
                0 => 'Inactive',
                1 => 'Active'
            ],
            'YesNo' => [
                0 => 'No',
                1 => 'Yes'
            ]
        ]
    ],
];
