<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/14/2014
 * Time: 7:02 PM
 */

namespace Thook\Cakerov\Database\Type;

use Cake\Database\Driver,
		Cake\Database\Type,
		PDO;

class BitType extends Type
{
	/**
	 * @param int|string $value
	 * @param Driver     $driver
	 *
	 * @return null|string
	 */
	public function toDatabase($value, Driver $driver)
	{
		if (is_int($value) || (is_string($value) && ctype_digit($value))) {
			return '0x' . $value;
		}
		else {
			return null;
		}
	}

	/**
	 * @param mixed  $value
	 * @param Driver $driver
	 *
	 * @return bool|int
	 */
	public function toPHP($value, Driver $driver)
	{
		$value = (int)$value;
		if ($value === 1 || $value === 0) {
			return (bool)$value;
		}
		else {
			return $value;
		}
	}

	/**
	 * @param mixed  $value
	 * @param Driver $driver
	 *
	 * @return int|mixed
	 */
	public function toStatement($value, Driver $driver)
	{
		if ($value === true || $value === false) {
			return PDO::PARAM_BOOL;
		}
		else {
			return $value === null ? PDO::PARAM_NULL : PDO::PARAM_STR;
		}
	}
}
