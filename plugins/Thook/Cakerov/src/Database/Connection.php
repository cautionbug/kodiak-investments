<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/14/2014
 * Time: 10:14 PM
 */

namespace Thook\Cakerov\Database;

use Cake\Database\Connection as CakeConnection;

/**
 * Class Connection
 *
 * Poor effort to implement table prefixes per https://github.com/cakephp/cakephp/issues/2666
 * but i don't have the time or patience.
 *
 * @package Thook\Database
 */
class Connection extends CakeConnection
{

}
