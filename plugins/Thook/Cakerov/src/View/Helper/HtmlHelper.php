<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/7/2014
 * Time: 6:03 PM
 */

namespace Thook\Cakerov\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper\HtmlHelper as CakeHtmlHelper;
use Cake\Routing\Router;
use Cake\View\View;
use Thook\Cakerov\Utility\Inflector;
//use Cake\Log\Log;

class HtmlHelper extends CakeHtmlHelper
{
    protected $_productionMode = FALSE;

    public function __construct(View $View, array $config = [])
    {
        $this->_productionMode = Configure::read('debug') === FALSE;
        parent::__construct($View, $config);
    }

    public function charset($charset = NULL)
    {
        return $this->tag('meta', NULL, ['charset' => $charset]);
    }

    /**
     * @param array  $menu
     * @param string $idPrefix
     * @param string $active
     * @param string $menuType Don't be an asshole, self. If you're going to use a menu, provide a builder for it.
     * @param string $userRole
     *
     * @return string
     */
    //public function parseMenu(array $menu, $idPrefix, array &$active = [], $menuType = 'top-bar')
    public function parseMenu(array $menu, $idPrefix, &$active = '', $menuType = 'top-bar', $userRole = '')
    {
        $menuMethod = '_' . Inflector::lowerCamelize($menuType, '-') . 'Menu';

        return $this->$menuMethod($menu, $idPrefix, $active, $userRole);
    }

    /**
     * @param array  $menu
     * @param string $idPrefix
     * @param string $routeStr
     * @param string $userRole
     *
     * @return string
     */
    private function _topBarMenu(array $menu, $idPrefix, &$routeStr, $userRole = '')
    {
        $menuHtml = '';
        while ($element = array_shift($menu)) {
        //foreach ($menu as /*$href =>*/ $element) {
            if (!isset($element['roles']) || in_array($userRole, (array)$element['roles'])) {
                $subHtml = '';
                $href    = isset($element['url'])
                    ? (is_array($element['url'])
                        ? Router::url($element['url'])
                        : (string)$element['url'])
                    : FALSE;
                if (isset($element['sub'])) {
                    $element['li-attrs']['class'] .= ' has-dropdown ' . ('');
                    $subHtml = $this->tag('ul',
                                          call_user_func_array([$this, __FUNCTION__], [$element['sub'], $idPrefix, $routeStr, $userRole]),
                                          ['class' => 'dropdown']
                    );
                }
                // Check if the current <LI> ID matches the $_active item:
                if (isset($element['li-attrs']['id'])) {
                    // Short-circuit if an ID match has been made
                    if ($routeStr !== NULL && $routeStr === $element['url']) {
                        $element['li-attrs']['class'] .= ' active';
                        $routeStr = NULL;
                        //$routeParam = NULL;
                    }
                    // Prefix the ID to ensure unique menu structures
                    $element['li-attrs']['id'] = $idPrefix . '-' . $element['li-attrs']['id'];
                }
                if (isset($element['li-attrs']['id'])) {
                    // Build the <LI> and append any descendants.
                    // (For the link builder, use a Controller/Action array if available, else the array key)
                    // If neither is defined, don't include href attribute.
                    $menuHtml .= $this->tag('li',
                                            $this->tag('a',
                                                       $element['text'],
                                                       $element['a-attrs'] + [
                                                           'escape' => FALSE,
                                                           'href'   => $href
                                                       ]
                                            ) . $subHtml . (isset($element['moreHtml']) ? $element['moreHtml'] : ''),
                                            $element['li-attrs']
                    );
                }
            }
        }

        return $menuHtml;
    }

    /**
     * Append .min to script paths for non-debug mode.
     *
     * @param array|string $url
     * @param array        $options
     *
     * @return mixed|void
     */
    public function script($url, array $options = [])
    {
        foreach ((array)$url as &$i) {
            // Must be Production Mode, '.min' not already at the end of string (force-min), and not an external URL.
            if ($this->_productionMode && substr($i, -4) !== '.min' && strpos('//', $i) === FALSE) {
                $i .= '.min';
            }
        }

        return parent::script($url, $options);
    }

    /**
     * Append .min to css paths for non-debug mode.
     *
     * @param array|string $path
     * @param array        $options
     *
     * @return string|void
     */
    public function css($path, array $options = [])
    {
        foreach ((array)$path as &$i) {
            // Must be Production Mode, '.min' not already at the end of string (force-min), and not an external URL.
            if ($this->_productionMode && substr($i, -4) !== '.min' && strpos('//', $i) === FALSE) {
                $i .= '.min';
            }
        }

        return parent::css($path, $options);
    }
}
