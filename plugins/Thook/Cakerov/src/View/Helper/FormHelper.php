<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 12/3/2014
 * Time: 12:16 AM
 */

namespace Thook\Cakerov\View\Helper;

use Cake\Core\Configure;
use Cake\View\Helper\FormHelper as CakeFormHelper;
//use Cake\Routing\Router;
use Thook\Cakerov\Utility\Inflector;

//use Cake\Log\Log;

class FormHelper extends CakeFormHelper
{
    /**
     * Cake doesn't convert ID attributes provided in $options, so this provides public access to the protected method.
     *
     * @param $value
     *
     * @return string
     */
    public function domId($value)
    {
        return $this->_domId($value);
    }
}
