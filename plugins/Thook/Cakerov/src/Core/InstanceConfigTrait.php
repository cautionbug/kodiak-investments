<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 11/7/2014
 * Time: 1:04 AM
 */

namespace Thook\Cakerov\Core;

/**
 * Trait InstanceConfigTrait
 *
 * @package Thook\Cakerov\Core
 *
 * @todo Build accessor methods to differentiate properties here from properties in Cake's version
 */
trait InstanceConfigTrait {
    use \Cake\Core\InstanceConfigTrait {
        _configRead as cake_ConfigRead;
        config as cake_Config;
    }

    /**
     * @param null $key
     * @param null $value
     * @param bool $merge
     *
     * @return array|mixed
     */
    public function config($key = NULL, $value = NULL, $merge = TRUE)
    {
        if (is_array($key) && func_num_args() === 1) {
            return $this->_configRead($key);
        }
        else {
            return $this->cake_Config($key, $value, $merge);
        }

    }

    /**
     * @param $key
     *
     * @return array|mixed
     */
    protected function _configRead($key)
    {
        if (is_array($key)) {
            $return = [];
            foreach ($key as $k) {
                $return[] = $this->cake_ConfigRead($k);
            }
        }
        else {
            $return = $this->cake_ConfigRead($key);
        }

        return $return;
    }
}
