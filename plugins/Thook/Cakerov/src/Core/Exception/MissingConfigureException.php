<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/28/2014
 * Time: 4:24 PM
 */

namespace Thook\Cakerov\Core\Exception;

use Cake\Core\Exception\Exception as CakeException;

class MissingConfigureException extends CakeException {
    protected $_messageTemplate = 'Configuration option "%s" could not be found.';
}
