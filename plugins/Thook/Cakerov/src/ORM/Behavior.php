<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 11/3/2014
 * Time: 11:32 PM
 */

namespace Thook\Cakerov\ORM;

use Cake\ORM\Behavior as CakeBehavior;
use Thook\Cakerov\Core\InstanceConfigTrait;

/**
 * Class Behavior
 *
 * @package Thook\Cakerov\ORM
 *
 * @todo Build accessor methods to differentiate properties here from properties in Cake's version
 */
class Behavior extends CakeBehavior {
    use InstanceConfigTrait;
}
