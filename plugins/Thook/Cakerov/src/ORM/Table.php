<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 7/22/2014
 * Time: 11:03 PM
 */

namespace Thook\Cakerov\ORM;

use Cake\ORM\Query;
use Cake\ORM\Table as CakeTable;
use Cake\Log\Log;

class Table extends CakeTable
{
    public function findRandom(Query $query, $options = [])
    {
        // Random should always order by RAND(). Duuuhhhh.
        $options['order'] = 'RAND()';
        if (!isset($options['limit'])) {
            $options['limit'] = 1;
        }

        return $this->query()->applyOptions($options)->all();
    }

    public function findActive(Query $query, array $options = [])
    {

        // TODO (JLT): Remove before production
        Log::debug($this->_table);

        $query->where(
            [
                $this->_table . '.active' => TRUE
            ]
        );
    }
}
