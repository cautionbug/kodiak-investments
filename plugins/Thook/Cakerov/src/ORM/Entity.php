<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 8/11/2014
 * Time: 7:42 PM
 */

namespace Thook\Cakerov\ORM;

use Cake\Core\Configure;
use Cake\ORM\Entity as CakeEntity;
//use Cake\I18n\Time;
//use Cake\Error\Debugger;

class Entity extends CakeEntity
{
    public function entityName()
    {
        $classArray = explode('\\', get_class($this));

        return array_pop($classArray);
    }

    protected function _getStatus()
    {
        // isset() covers NULL; can't use $active ?: 1 (because 0 == false)
        return Configure::read('DataTypes.Bool.Status')[isset($this->active) ? $this->active : 1];
    }

    protected function _getCreatedFmt()
    {
        return empty($this->created) ? $this->created : $this->created->format('m/d/Y h:i A');
    }

    protected function _getModifiedFmt()
    {
        return empty($this->modified) ? $this->modified : $this->modified->format('m/d/Y h:i A');
    }
}
