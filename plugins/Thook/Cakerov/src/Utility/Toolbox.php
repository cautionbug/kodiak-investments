<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 7/26/2014
 * Time: 8:46 PM
 */

namespace Thook\Cakerov\Utility;

class Toolbox
{
	/**
	 * Checks if a value is a number, not numeric string.
	 *
	 * @param $value
	 *
	 * @return bool
	 */
	public static function is_number($value) {
		return is_numeric($value) && !is_string($value);
	}

	/**
	 * @param string $kvSeparator
	 * @param string $pairSeparator
	 * @param array  $array
	 * @param bool   $deep
	 * @param string $deepWrapper : If length > 2, only first 2 characters will be used
	 *
	 * @return string
	 */
	public static function kimplode($kvSeparator, $pairSeparator, array $array, $deep = FALSE, $deepWrapper = '[]') {
		$implode = [];
		$deepSplit = str_split($deepWrapper);
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$v = $deepSplit[0] .
				     ($deep
						     ? self::kimplode($kvSeparator, $pairSeparator, $v, $deep, $deepWrapper)
						     : gettype($v)) .
				     ($deepSplit[1] ?: $deepSplit[0]);
			}
			$implode[] = (string)$k . $kvSeparator . $v;
		}
		return implode($pairSeparator, $implode);
	}
}
