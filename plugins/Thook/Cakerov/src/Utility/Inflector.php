<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/27/2014
 * Time: 1:27 AM
 */

namespace Thook\Cakerov\Utility;

use Cake\Utility\Inflector as CakeInflector;

/**
 * Class Inflector
 *
 * @package Thook\Utility
 */
class Inflector extends CakeInflector
{
    private static $__highlyUnlikely = '{{thisStringIsHighlyUnlikelyToExistInTheTarget}}';

    /**
     * Cake's default camelize returns UpperCamel and only allows snake_separation.
     * This helps "fix" that.
     *
     * @param string $nonAlphaSeparatedWord
     * @param string $separator
     *
     * @return string
     */
    public static function camelize($nonAlphaSeparatedWord, $separator = '_')
    {
        if (!($result = static::_cache(__FUNCTION__, $nonAlphaSeparatedWord))) {
            // Really wish CakePHP would get with the times & allow - separators.
            // If _ isn't the separator, protect existing _ by replacing with a highly unlikely protection string
            // Then replace $separator with _ that parent::camelize likes.
            $result = parent::camelize(
                '_' === $separator
                    ? $nonAlphaSeparatedWord
                    : str_replace($separator, '_', str_replace('_', self::$__highlyUnlikely, $nonAlphaSeparatedWord))
            );
            // After we have the Cake-camelized version, undo the protection
            // If $separator is _ we don't need to do it at all.
            '_' === $separator || ($result = str_replace(self::$__highlyUnlikely, '_', $result));
            parent::_cache(__FUNCTION__, $result);
        }

        return $result;
    }

    /**
     * "Fixes" the default Cake\Utility\Inflector::camelize behavior by also inflecting to lowerCamel.
     *
     * @param string $nonAlphaSeparatedWord
     * @param string $separator
     *
     * @return string
     */
    public static function lowerCamelize($nonAlphaSeparatedWord, $separator = '_')
    {
        if (!($result = static::_cache(__FUNCTION__, $nonAlphaSeparatedWord))) {
            $result = lcfirst(self::camelize($nonAlphaSeparatedWord, $separator));
        }

        return $result;
    }

    /**
     * Since this is the default behavior of Cake\Utility\Inflector::camelize, avoid using this.
     * It's included for completeness.
     *
     * @param string $nonAlphaSeparatedWord
     * @param string $separator
     *
     * @return string
     */
    public static function upperCamelize($nonAlphaSeparatedWord, $separator = '_')
    {
        if (!($result = static::_cache(__FUNCTION__, $nonAlphaSeparatedWord))) {
            $result = self::camelize($nonAlphaSeparatedWord, $separator);
        }

        return $result;
    }

    /**
     * @param array  $nonAlphaSeparatedWordArray
     * @param string $separator
     * @param bool   $lower
     *
     * @return array
     */
    public static function camelizeArray(array $nonAlphaSeparatedWordArray, $separator = '_', $lower = FALSE)
    {
        // Since the actual camelizers don't use references, we have to loop the array instead of walking it.
        foreach ($nonAlphaSeparatedWordArray as &$word) {
            if (is_array($word)) {
                $word = self::camelizeArray($word, $separator, $lower);
            }
            else {
                $word = call_user_func_array(
                    ['self', $lower ? 'lowerCamelize' : 'upperCamelize'],
                    [$word, $separator, $lower]
                );
            }
        }

        return $nonAlphaSeparatedWordArray;
    }

    /**
     * @param string $nonAlphaSeparatedWord
     * @param string $separator
     *
     * @return mixed|null|string
     */
    public static function humanize($nonAlphaSeparatedWord, $separator = '_')
    {
        if (!($result = static::_cache(__FUNCTION__, $nonAlphaSeparatedWord))) {
            // Really wish CakePHP would get with the times & allow - separators.
            // If _ isn't the separator, protect existing _ by replacing with a highly unlikely protection string
            // Then replace $separator with _ that parent::camelize likes.
            $result = parent::humanize(
                '_' === $separator
                    ? $nonAlphaSeparatedWord
                    : str_replace($separator, '_', str_replace('_', self::$__highlyUnlikely, $nonAlphaSeparatedWord))
            );
            // After we have the Cake-camelized version, undo the protection
            // If $separator is _ we don't need to do it at all.
            '_' === $separator || ($result = str_replace(self::$__highlyUnlikely, '_', $result));
            parent::_cache(__FUNCTION__, $result);
        }

        return $result;
    }

    public static function humanizeArray($nonAlphaSeparatedWordArray, $separator = '_')
    {
        // Since humanizers don't use references, loop the array instead of walking it.
        // We're actually modifying the pieces WITHIN $nonAlphaSeparatedWordArray, so that's what we need to return.
        foreach ($nonAlphaSeparatedWordArray as &$word) {
            if (is_array($word)) {
                $word = self::humanizeArray($word, $separator);
            }
            else {
                $word = self::humanize($word, $separator);
            }
        }

        return $nonAlphaSeparatedWordArray;
    }
}
