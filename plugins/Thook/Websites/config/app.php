<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/29/2014
 * Time: 12:45 AM
 */

$config = [
    'Datasources' => [
        /**
         * Data objects shared among different sites are in a different DB
         */
        'websites_www' => [
            'className'     => 'Cake\Database\Connection',
            'driver'        => 'Cake\Database\Driver\Mysql',
            'persistent'    => FALSE,
            'host'          => 'localhost',
            'port'          => 33066,
            'username'      => 'websites_www',
            'password'      => 'JOTaz5L48u',
            'database'      => 'websites',
            //'encoding'      => 'utf8',
            //'timezone'      => 'SYSTEM',
            'cacheMetadata' => TRUE,
        ],
        'websites_admin' => [
            'className'     => 'Cake\Database\Connection',
            'driver'        => 'Cake\Database\Driver\Mysql',
            'persistent'    => FALSE,
            'host'          => 'localhost',
            'port'          => 33066,
            'username'      => 'websites_www_admin',
            'password'      => '1AXuWACuWu',
            'database'      => 'websites',
            //'encoding'      => 'utf8',
            //'timezone'      => 'SYSTEM',
            'cacheMetadata' => TRUE,
        ],
    ],
];
