<?php
namespace Thook\Websites\Model\Entity;

use Thook\Cakerov\ORM\Entity;

/**
 * Captcha Entity.
 */
class Captcha extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'question' => TRUE,
        'answers'  => TRUE,
    ];

}
