<?php
namespace Thook\Websites\Model\Entity;

use Thook\Cakerov\ORM\Entity;

/**
 * MimeType Entity.
 */
class MimeType extends Entity {

/**
 * Fields that can be mass assigned using newEntity() or patchEntity().
 *
 * @var array
 */
	protected $_accessible = [
		'file_ext' => true,
		'mime_type' => true,
		'description' => true,
	];

}
