<?php
namespace Thook\Websites\Model\Table;

//use Thook\Cakerov\ORM\Query;
//use Thook\Cakerov\ORM\Table;
use Cake\Validation\Validator;

/**
 * MimeTypes Model
 */
class MimeTypesTable extends AbstractWebsitesTable {

/**
 * Initialize method
 *
 * @param array $config The configuration for the Table.
 * @return void
 */
	public function initialize(array $config) {
		$this->table('mime_types');
		$this->displayField('id');
		$this->primaryKey('id');
	}

/**
 * Default validation rules.
 *
 * @param \Cake\Validation\Validator $validator
 * @return \Cake\Validation\Validator
 */
	public function validationDefault(Validator $validator) {
		$validator
			->add('id', 'valid', ['rule' => 'numeric'])
			->allowEmpty('id', 'create')
			->validatePresence('file_ext', 'create')
			->notEmpty('file_ext')
			->validatePresence('mime_type', 'create')
			->notEmpty('mime_type')
			->allowEmpty('description');

		return $validator;
	}

}
