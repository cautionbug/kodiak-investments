<?php
namespace Thook\Websites\Model\Table;

//use Thook\Cakerov\ORM\Query;
//use Thook\Cakerov\ORM\Table;
use Cake\Validation\Validator;

/**
 * Captchas Model
 */
class CaptchasTable extends AbstractWebsitesTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('captchas');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->validatePresence('question', 'create')
            ->notEmpty('question')
            ->validatePresence('answers', 'create')
            ->notEmpty('answers');

        return $validator;
    }
}
