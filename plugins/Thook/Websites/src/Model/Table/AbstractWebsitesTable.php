<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/30/2014
 * Time: 12:15 AM
 */

namespace Thook\Websites\Model\Table;

use Thook\Cakerov\ORM\Table;

abstract class AbstractWebsitesTable extends Table
{
    public static function defaultConnectionName()
    {
        return 'websites_www';
    }
}
