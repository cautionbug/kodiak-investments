<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 7/27/2014
 * Time: 1:38 AM
 *
 * @todo Move this to a Plugin
 */

namespace Thook\Websites\Controller;

use Cake\Error\NotFoundException;
use Cake\ORM\TableRegistry;
//use Cake\Log\Log;

class CaptchasController extends AppController
{
	public $components = [
			'RequestHandler'
	];
	public $layout     = 'ajax';

	public function check() { //}, $answer) {
		$id     = $this->request->query('id');
		$answer = strtolower($this->request->query('captcha'));
		//Log::debug(__LINE__ . ': ' . var_export($data));

		if (!$id || !$answer) {
			throw new NotFoundException(__('Invalid request data.'));
		}
		$captchaTable = TableRegistry::get('Thook/Websites.Captchas');
		$captcha = $captchaTable->get($id);
		$valid   = in_array($answer, explode('|', strtolower($captcha->answers)));

		// Pick up a new Captcha so they can't hammer the same one.
		if (!$valid) {
			$captcha = $captchaTable->find('random')->first();
		}
		else {
			$captcha = NULL;
		}

		$this->set(compact('captcha', 'valid'));
	}
}
