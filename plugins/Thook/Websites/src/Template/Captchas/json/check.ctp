<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 7/27/2014
 * Time: 1:10 AM
 *
 * @type boolean              $valid
 * @type null|Cake\ORM\Entity $captcha
 *
 * Testing:
 * @type array                $request
 */

$newCaptcha = $captcha === NULL
		? NULL
		: [
				'id'       => $captcha->id,
				'question' => $captcha->question
		];

echo json_encode(['valid' => $valid, 'captcha' => $newCaptcha]);
