<?php
namespace Thook\Websites\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Thook\Websites\Model\Table\CaptchasTable;
use Cake\TestSuite\TestCase;

/**
 * Thook/Websites\Model\Table\CaptchasTable Test Case
 */
class CaptchasTableTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('Captchas') ? [] : ['className' => 'Thook\Websites\Model\Table\CaptchasTable'];
		$this->Captchas = TableRegistry::get('Captchas', $config);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Captchas);

		parent::tearDown();
	}

}
