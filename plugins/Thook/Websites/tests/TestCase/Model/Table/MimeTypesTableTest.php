<?php
namespace Thook\Websites\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Thook\Websites\Model\Table\MimeTypesTable;
use Cake\TestSuite\TestCase;

/**
 * Thook/Websites\Model\Table\MimeTypesTable Test Case
 */
class MimeTypesTableTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('MimeTypes') ? [] : ['className' => 'Thook\Websites\Model\Table\MimeTypesTable'];
		$this->MimeTypes = TableRegistry::get('MimeTypes', $config);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->MimeTypes);

		parent::tearDown();
	}

}
