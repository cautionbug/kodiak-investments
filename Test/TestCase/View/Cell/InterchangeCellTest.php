<?php
namespace App\Test\TestCase\View\Cell;

use App\View\Cell\InterchangeCell;
use Cake\TestSuite\TestCase;

/**
 * App\View\Cell\InterchangeCell Test Case
 */
class InterchangeCellTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->request = $this->getMock('Cake\Network\Request');
		$this->response = $this->getMock('Cake\Network\Response');
		$this->Interchange = new InterchangeCell($this->request, $this->response);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Interchange);

		parent::tearDown();
	}

/**
 * testDisplay method
 *
 * @return void
 */
	public function testDisplay() {
		$this->markTestIncomplete('testDisplay not implemented.');
	}

}
