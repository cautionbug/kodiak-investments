##################
# Once these alterations are made on a DB, comment them out so they're not attempted again.
# Update the Create SQL file to match the changes here so they're part of the create process.
##################
USE `kodiak`;

## PROCEDURES
# DROP PROCEDURE IF EXISTS `AddColumnIfNotExists`;
# DROP PROCEDURE IF EXISTS `AddForeignKeyIfNotExists`;
# DELIMITER $$
# CREATE PROCEDURE `AddColumnIfNotExists`(
# 	IN dbName    TINYTEXT,
# 	IN tableName TINYTEXT,
# 	IN colName   TINYTEXT,
# 	IN colDef    TEXT)
# 	BEGIN
# 		IF NOT EXISTS(
# 				SELECT *
# 				FROM `information_schema`.`COLUMNS`
# 				WHERE `COLUMN_NAME` = colName
# 				      AND `TABLE_NAME` = tableName
# 				      AND `TABLE_SCHEMA` = dbName)
# 		THEN
# 			SET @ddl = CONCAT('ALTER TABLE ', dbName, '.', tableName,
# 			                  ' ADD COLUMN ', colName, ' ', colDef);
# 			PREPARE stmnt FROM @ddl;
# 			EXECUTE stmnt;
# 			DEALLOCATE PREPARE stmnt;
#
# 		END IF;
# 	END;
# $$
#
# CREATE PROCEDURE `AddForeignKeyIfNotExists`(
# 	IN dbName    TINYTEXT,
# 	IN tableName TINYTEXT,
# 	IN colName   TINYTEXT,
# 	IN dbRef     TINYTEXT,
# 	IN tableRef  TINYTEXT,
# 	IN colRef    TINYTEXT,
# 	IN onUpdate  TINYTEXT,
# 	IN onDelete  TINYTEXT)
# 	BEGIN
# 		IF ISNULL(onUpdate) THEN
# 			SET onUpdate = '';
# 		ELSE
# 			SET onUpdate = CONCAT(' ON UPDATE ', onUpdate);
# 		END IF;
# 		IF ISNULL(onDelete) THEN
# 			SET onDelete = '';
# 		ELSE
# 			SET onDelete = CONCAT(' ON DELETE ', onDelete);
# 		END IF;
#
# 		IF NOT EXISTS(
# 				SELECT *
# 				FROM `information_schema`.`KEY_COLUMN_USAGE`
# 				WHERE `COLUMN_NAME` = colName
# 				      AND `TABLE_NAME` = tableName
# 				      AND `TABLE_SCHEMA` = dbName
# 				      AND `REFERENCED_COLUMN_NAME` = colRef
# 				      AND `REFERENCED_TABLE_NAME` = tableRef
# 				      AND `REFERENCED_TABLE_SCHEMA` = dbRef)
# 		THEN
# 			SET @ddl = CONCAT('ALTER TABLE ', dbName, '.', tableName,
# 			                  ' ADD FOREIGN KEY (', colName, ') REFERENCES ',
# 			                  dbRef, '.', tableRef, '(', colRef, ')', onUpdate, onDelete);
# 			PREPARE stmnt FROM @ddl;
# 			EXECUTE stmnt;
# 			DEALLOCATE PREPARE stmnt;
# 		END IF;
# 	END;
# $$
# DELIMITER ;

# CHANGES
## ITEM ATTRIBUTES
# CALL `AddColumnIfNotExists`('kodiak', 'traits', 'abbrev',
#                             'VARCHAR(10) DEFAULT NULL AFTER `name`');
# ALTER IGNORE TABLE `item_traits` DROP INDEX `u_idx_traits_name`;
# ALTER TABLE `item_traits` ADD UNIQUE KEY `u_idx_traits_name`(`name`);
# ALTER IGNORE TABLE `item_traits` DROP INDEX `u_idx_traits_abbrev`;
# ALTER TABLE `item_traits` ADD UNIQUE KEY `u_idx_traits_abbrev`(`abbrev`);
# ALTER TABLE `item_traits` ADD `requires_value` TINYINT(1) DEFAULT 0 NOT NULL;
# ALTER TABLE `item_traits` ADD `active` TINYINT(1) NOT NULL DEFAULT 1 AFTER `requires_value`;
# ALTER TABLE `item_traits` MODIFY `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `requires_value`;
# ALTER TABLE `item_traits` MODIFY `requires_value` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0;

## LISTINGS
# ALTER IGNORE TABLE `listings` CHANGE `conditions_id` `condition_id` INT UNSIGNED DEFAULT NULL;
# ALTER TABLE `listings` MODIFY `county` VARCHAR(25) DEFAULT NULL;
# ALTER IGNORE TABLE `listings` ADD INDEX `idx_listing_condition_id`(`condition_id`);
# CALL `AddForeignKeyIfNotExists`('kodiak', 'listings', 'condition_id',
#                                 'kodiak', 'house_conditions', 'id',
#                                 'CASCADE', 'SET NULL');
# ALTER TABLE `listings` CHANGE `publish` `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1;
# ALTER TABLE `listings` MODIFY `info_body` LONGTEXT NOT NULL COMMENT 'Listing Information';

## PARKS
# ALTER TABLE `parks` ADD `file_name_no_ext` VARCHAR(100) DEFAULT NULL AFTER `info_body`;
# ALTER TABLE `parks` ADD UNIQUE KEY `u_idx_parks_file_name`(`file_name_no_ext`);
# ALTER TABLE `parks` ADD `active` TINYINT(1) NOT NULL DEFAULT 1 AFTER `file_name_no_ext`;
# ALTER TABLE `parks` MODIFY `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `file_name_no_ext`;

## PHOTOS
# ALTER TABLE `photos`
# ADD INDEX `idx_photos_mime_type_id`(`mime_type_id`),
# ADD FOREIGN KEY (`mime_type_id`) REFERENCES `websites`.`mime_types` (`id`)
# 	ON UPDATE CASCADE ON DELETE RESTRICT;

## LISTINGS_PHOTOS
# ALTER TABLE `listings_photos`
# ADD INDEX `idx_listings_photos_photo_id`(`photo_id`),
# ADD INDEX `idx_listings_photos_listing_id`(`listing_id`),
# ADD FOREIGN KEY (`photo_id`) REFERENCES `photos` (`id`)
# 	ON UPDATE CASCADE ON DELETE CASCADE,
# ADD FOREIGN KEY (`listing_id`) REFERENCES `listings` (`id`)
# 	ON UPDATE CASCADE ON DELETE CASCADE
# ALTER TABLE `listings_photos` ADD `position` SMALLINT UNSIGNED DEFAULT NULL;

## CONTACTS
# ALTER TABLE `contacts` ADD `ip_address` VARCHAR(15) DEFAULT NULL AFTER `home_traits`;
# ALTER TABLE `contacts` ADD `remove_hash` VARCHAR(25) DEFAULT NULL AFTER `comments`;

## CAPTCHA
# USE `websites`;

## HOUSE CONDITIONS
#ALTER TABLE `house_conditions` ADD `active` TINYINT(1) NOT NULL DEFAULT 1 AFTER `name`;
# ALTER TABLE `house_conditions` MODIFY `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `name`;
# ALTER TABLE `house_conditions` MODIFY `name` VARCHAR(50) NOT NULL;

## HOUSE STYLES
# ALTER TABLE `house_styles` ADD `active` TINYINT(1) NOT NULL DEFAULT 1 AFTER `name`;
# ALTER TABLE `house_styles` MODIFY `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `name`;

## TERMS
# ALTER TABLE `terms` ADD `active` TINYINT(1) NOT NULL DEFAULT 1 AFTER `fine_print`;
# ALTER TABLE `terms` MODIFY `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `fine_print`;

## PARKS
# ALTER TABLE `parks` MODIFY `county` VARCHAR(25) DEFAULT NULL COMMENT 'County';

## USERS (Admin)
# ALTER TABLE `users` ADD `last_login` DATETIME DEFAULT NULL AFTER `role`;
# ALTER TABLE `users` ADD `active` TINYINT(1) UNSIGNED DEFAULT 1 AFTER `last_login`;
# ALTER TABLE `users` MODIFY `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1;
# ALTER TABLE `users`
#   ADD `email` VARCHAR(100) NOT NULL AFTER `password`,
#   ADD UNIQUE KEY `email`(`email`);
# ALTER TABLE `users`
#   ADD `first_name` VARCHAR(50) NOT NULL AFTER `password`,
#   ADD `last_name` VARCHAR(50) NOT NULL AFTER `first_name`;
# ALTER TABLE `users`
#   DROP INDEX `user_id`,
#   CHANGE `user_id` `username` VARCHAR(50) NOT NULL UNIQUE;
# ALTER TABLE `users` MODIFY `id` INT UNSIGNED NOT NULL AUTO_INCREMENT;

## NEWS
# ALTER TABLE `news` ADD `headline` VARCHAR(100) NOT NULL AFTER `id`;
# ALTER TABLE `news` ADD `active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1 AFTER `expires`;

### RENAMING TABLES TO GET AWAY FROM 'TRAIT'
# CALL `DropForeignKeyIfExists`('fk_listings_traits__item_attrs_id', 'listings_traits', 'kodiak');
# CALL `DropForeignKeyIfExists`('fk_listings_traits__listings_id', 'listings_traits', 'kodiak');
# ALTER TABLE listings_traits
# 	DROP INDEX `idx_listings_traits_listing_id`,
# 	DROP INDEX `idx_listings_traits_trait_id`,
# 	DROP INDEX `idx_listings_traits_value`,
# 	DROP PRIMARY KEY;
# RENAME TABLE `listings_traits` TO `listings_attrs`;
# ALTER TABLE `listings_attrs` CHANGE `trait_id` `attr_id` INT UNSIGNED NOT NULL;
# ALTER TABLE `listings_attrs`
# 	ADD PRIMARY KEY (`listing_id`, `attr_id`),
# 	ADD INDEX `idx_listings_attrs_listing_id`(`listing_id`),
# 	ADD INDEX `idx_listings_attrs_attr_id`(`attr_id`),
# 	ADD INDEX `idx_listings_attrs_value`(`value`);
#
# ALTER TABLE `item_traits`
# 	MODIFY `name` VARCHAR(50) NOT NULL COMMENT 'Attribute Name',
# 	MODIFY `icon_class` VARCHAR(20) DEFAULT NULL COMMENT 'Attribute Icon',
# 	DROP INDEX `u_idx_traits_name`,
# 	DROP INDEX `u_idx_traits_abbrev`;
# ALTER TABLE `item_traits`
# 	ADD UNIQUE `u_idx_attrs_name`(`name`),
# 	ADD UNIQUE `u_idx_attrs_abbrev`(`abbrev`);
# RENAME TABLE `item_traits` TO `item_attrs`;
#
# ALTER TABLE `listings_attrs`
# 	ADD CONSTRAINT `fk_listings_attrs__listings_id`
# 		FOREIGN KEY (`listing_id`) REFERENCES `listings` (`id`)
# 		ON UPDATE CASCADE
# 		ON DELETE CASCADE,
# 	ADD CONSTRAINT `fk_listings_attrs__item_attrs_id`
# 		FOREIGN KEY (`attr_id`) REFERENCES `item_attrs` (`id`)
# 		ON UPDATE CASCADE
# 		ON DELETE CASCADE;
# RENAME TABLE `item_attrs` TO `attributes`;
# RENAME TABLE `listings_attrs` TO `listings_attributes`;
# ALTER TABLE `listings_attributes` CHANGE COLUMN `attr_id` `attribute_id` INT UNSIGNED NOT NULL;
# ALTER TABLE `listings_attributes`
#     DROP FOREIGN KEY `fk_listings_attrs__item_attrs`,
#     DROP FOREIGN KEY `fk_listings_attrs__listings`,
#     DROP INDEX `u_idx_listings_attrs_listing_attr`,
#     DROP INDEX `idx_listings_attrs_listing_id`,
#     DROP INDEX `idx_listings_attrs_attr_id`,
#     DROP INDEX `idx_listings_attrs_value`,
#     ADD INDEX `idx_listings_attributes_listing_id`(`listing_id`),
#     ADD INDEX `idx_listings_attributes_attribute_id`(`attribute_id`),
#     ADD INDEX `idx_listings_attributes_value`(`value`),
#     ADD UNIQUE INDEX `u_idx_listings_attributes_listing_attr`(`attribute_id`, `listing_id`),
#     ADD CONSTRAINT `fk_listings_attributes__attributes`
#         FOREIGN KEY(`attribute_id`) REFERENCES `attributes`(`id`) ON UPDATE CASCADE ON DELETE CASCADE,
#     ADD CONSTRAINT `fk_listings_attributes__listings`
#         FOREIGN KEY(`listing_id`) REFERENCES `listings`(`id`) ON UPDATE CASCADE ON DELETE CASCADE
# ;
