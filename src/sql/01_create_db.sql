/*
DATABASE PREPARATION

This is a DESTRUCTIVE database initialization file.
All tables for the project will be dropped, then re-created.

i'll eventually set up an ALTER TABLE IF EXISTS file that would be run after this.
 */

CREATE DATABASE IF NOT EXISTS `websites`;
CREATE DATABASE IF NOT EXISTS `kodiak`;

USE `kodiak`;

### EXTERMINATE!!!
SET @schema = 'kodiak';
SET @pattern = '%';

SELECT CONCAT('DROP TABLE ', GROUP_CONCAT(CONCAT(@schema, '.', `table_name`)), ';')
INTO @droplike
FROM `information_schema`.`tables`
WHERE `table_schema` = @schema AND `table_type` = 'BASE TABLE';
-- table_name LIKE @pattern;

SET FOREIGN_KEY_CHECKS = 0;
PREPARE stmt FROM @droplike;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET FOREIGN_KEY_CHECKS = 1;

# AUXILIARY TABLES

# This is in a different database, keeping it available to other projects...?
USE `websites`;
CREATE TABLE IF NOT EXISTS `mime_types` (
	`id`          INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`file_ext`    VARCHAR(15)  NOT NULL UNIQUE,
	`mime_type`   VARCHAR(50)  NOT NULL,
	`description` VARCHAR(150)          DEFAULT NULL,
	INDEX `idx_mime_types_mime_type`(`mime_type`)
);

CREATE TABLE IF NOT EXISTS `captchas` (
	`id`       INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	`question` VARCHAR(200) UNIQUE NOT NULL,
	`answers`  VARCHAR(200)        NOT NULL,
	`created`  DATETIME     DEFAULT NULL,
	`modified` DATETIME     DEFAULT NULL
);

CREATE TRIGGER `trig_captchas_BI` BEFORE INSERT ON `captchas` FOR EACH ROW BEGIN
	SET NEW.created = NOW();
END;
CREATE TRIGGER `trig_captchas_BU` BEFORE UPDATE ON `captchas` FOR EACH ROW BEGIN
	SET NEW.modified = NOW();
END;

USE `kodiak`;

CREATE TABLE IF NOT EXISTS `users` (
	`id`         INT UNSIGNED        NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`username`   VARCHAR(50)         NOT NULL UNIQUE,
	`password`   VARCHAR(255)        NOT NULL,
	`first_name` VARCHAR(50)         NOT NULL,
	`last_name`  VARCHAR(50)         NOT NULL,
	`email`      VARCHAR(100) UNIQUE NOT NULL,
	`role`       VARCHAR(20)         NOT NULL,
	`last_login` DATETIME                     DEFAULT NULL,
	`active`     TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
	`created`    DATETIME                     DEFAULT NULL,
	`modified`   DATETIME                     DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS `item_attrs` (
	`id`             INT UNSIGNED        NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name`           VARCHAR(50)         NOT NULL
	COMMENT 'Attribute Name',
	`abbrev`         VARCHAR(10)                  DEFAULT NULL
	COMMENT 'Abbreviation',
	`icon_class`     VARCHAR(20)                  DEFAULT NULL
	COMMENT 'Attribute Icon',
	`requires_value` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`active`         TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
	UNIQUE `u_idx_attrs_name`(`name`),
	UNIQUE `u_idx_attrs_abbrev`(`abbrev`)
);

CREATE TABLE IF NOT EXISTS `terms` (
	`id`         INT UNSIGNED        NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name`       VARCHAR(20)         NOT NULL UNIQUE
	COMMENT 'Transaction Terms',
	`fine_print` TEXT                         DEFAULT NULL,
	`active`     TINYINT(1) UNSIGNED NOT NULL DEFAULT 1
);

CREATE TABLE IF NOT EXISTS `house_conditions` (
	`id`     INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name`   VARCHAR(50)  NOT NULL UNIQUE
	COMMENT 'House Condition',
	`active` TINYINT(1)   NOT NULL DEFAULT 1
);

CREATE TABLE IF NOT EXISTS `house_styles` (
	`id`     INT UNSIGNED        NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name`   VARCHAR(50)         NOT NULL UNIQUE
	COMMENT 'House Style',
	`active` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0
);

# PRIMARY TABLES

CREATE TABLE IF NOT EXISTS `parks` (
	`id`               INT UNSIGNED        NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name`             VARCHAR(100)        NOT NULL
	COMMENT 'Park Name',
	`street`           VARCHAR(50)         NOT NULL
	COMMENT 'Street Address',
	`city`             VARCHAR(50)         NOT NULL
	COMMENT 'City',
	`county`           VARCHAR(25)                  DEFAULT NULL
	COMMENT 'County',
	`state`            CHAR(2)             NOT NULL
	COMMENT 'State',
	`zip`              VARCHAR(9)          NOT NULL
	COMMENT 'Zip Code',
	`lat`              DECIMAL(10, 7)               DEFAULT NULL
	COMMENT 'Latitude',
	`lng`              DECIMAL(10, 7)               DEFAULT NULL
	COMMENT 'Longitude',
	`info_body`        LONGTEXT                     DEFAULT NULL
	COMMENT 'Park Information',
	`file_name_no_ext` VARCHAR(100)                 DEFAULT NULL
	COMMENT 'Park Map File Name (ext removed)',
	`active`           TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
	`created`          DATETIME                     DEFAULT NULL,
	`modified`         DATETIME                     DEFAULT NULL,
	UNIQUE `u_idx_parks_file_name`(`file_name_no_ext`),
	FULLTEXT INDEX `ft_idx_parks_info_body`(`info_body`)
);

CREATE TABLE IF NOT EXISTS `listings` (
	`id`                 INT UNSIGNED            NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`title`              VARCHAR(200)            NOT NULL,
	`street`             VARCHAR(50)             NOT NULL
	COMMENT 'Street Address',
	`city`               VARCHAR(50)             NOT NULL
	COMMENT 'City',
	`county`             VARCHAR(25)                      DEFAULT NULL
	COMMENT 'County',
	`state`              CHAR(2)                 NOT NULL
	COMMENT 'State',
	`zip`                VARCHAR(9)              NOT NULL
	COMMENT 'Zip Code',
	`lat`                DECIMAL(10, 7)                   DEFAULT NULL
	COMMENT 'Latitude',
	`lng`                DECIMAL(10, 7)                   DEFAULT NULL
	COMMENT 'Longitude',
	`lot_no`             VARCHAR(10)                      DEFAULT NULL
	COMMENT 'Lot #',
	`area`               INT UNSIGNED                     DEFAULT 0
	COMMENT 'Square Footage',
	`bedrooms`           TINYINT UNSIGNED        NOT NULL DEFAULT 0,
	`bathrooms`          TINYINT UNSIGNED        NOT NULL DEFAULT 0,
	`price`              DECIMAL(10, 2) UNSIGNED NOT NULL DEFAULT 0
	COMMENT 'List Price',
	`house_condition_id` INT UNSIGNED                     DEFAULT NULL,
	`park_id`            INT UNSIGNED                     DEFAULT NULL,
	`term_id`            INT UNSIGNED                     DEFAULT NULL,
	`house_style_id`     INT UNSIGNED                     DEFAULT NULL,
	`info_body`          LONGTEXT                NOT NULL
	COMMENT 'Listing Information',
	`ready_date`         DATE                             DEFAULT NULL,
	`active`             TINYINT(1) UNSIGNED     NOT NULL DEFAULT 0
	COMMENT 'Publish Listing',
	`created`            DATETIME                         DEFAULT NULL,
	`modified`           DATETIME                         DEFAULT NULL,
	INDEX `idx_listings_title`(`title`),
	INDEX `idx_listings_area`(`area`),
	INDEX `idx_listings_condition_id`(`house_condition_id`),
	INDEX `idx_listings_parks_id`(`park_id`),
	INDEX `idx_listings_terms_id`(`term_id`),
	INDEX `idx_listings_type_id`(`house_style_id`),
	FULLTEXT INDEX `ft_idx_listings_info_body`(`info_body`),
	CONSTRAINT `fk_listings__house_conditions`
	FOREIGN KEY (`house_condition_id`) REFERENCES `house_conditions` (`id`)
		ON UPDATE CASCADE
		ON DELETE SET NULL,
	CONSTRAINT `fk_listings__parks`
	FOREIGN KEY (`park_id`) REFERENCES `parks` (`id`)
		ON UPDATE CASCADE
		ON DELETE SET NULL,
	CONSTRAINT `fk_listings__terms`
	FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`)
		ON UPDATE CASCADE
		ON DELETE SET NULL,
	CONSTRAINT `fk_listings__house_styles`
	FOREIGN KEY (`house_style_id`) REFERENCES `house_styles` (`id`)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

CREATE TABLE IF NOT EXISTS `photos` (
	`id`           INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`mime_type_id` INT UNSIGNED NOT NULL,
	`file_path`    VARCHAR(200) NOT NULL UNIQUE,
	`hash`         VARCHAR(64) COMMENT 'SHA256 Hash to check for duplicates',
	INDEX `idx_photos_mime_type_id`(`mime_type_id`),
	CONSTRAINT `fk_photos__ws_mime_types`
	FOREIGN KEY (`mime_type_id`) REFERENCES `websites`.`mime_types` (`id`)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE IF NOT EXISTS `contacts` (
	`id`            INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`name`          VARCHAR(50)  NOT NULL,
	`email`         VARCHAR(75)  NOT NULL,
	`phone`         VARCHAR(10)           DEFAULT NULL,
	`source`        VARCHAR(200)          DEFAULT NULL
	COMMENT 'How did you hear about us?',
	`move_deadline` DATE                  DEFAULT NULL
	COMMENT 'How soon do you need to move in?',
	`price_range`   INT UNSIGNED          DEFAULT NULL,
	`term_id`       INT UNSIGNED          DEFAULT NULL,
	`home_size`     VARCHAR(100)          DEFAULT NULL
	COMMENT 'What size home?',
	`family_size`   TINYINT UNSIGNED      DEFAULT NULL
	COMMENT 'How many people are moving?',
	`home_traits`   VARCHAR(200)          DEFAULT NULL
	COMMENT 'What kind of home are you looking for?',
	`ip_address`    VARCHAR(15)           DEFAULT NULL,
	`comments`      TEXT                  DEFAULT NULL,
	`remove_hash`   VARCHAR(50)           DEFAULT NULL,
	`created`       DATETIME              DEFAULT NULL,
# 	`modified`      DATETIME DEFAULT NULL,
	INDEX `idx_contacts_email`(`email`),
	INDEX `idx_contacts_terms_id`(`term_id`),
	INDEX `idx_contacts_price_range`(`price_range`),
	CONSTRAINT `fk_contacts__terms`
	FOREIGN KEY (`term_id`) REFERENCES `terms` (`id`)
		ON UPDATE CASCADE
		ON DELETE SET NULL
);

CREATE TABLE IF NOT EXISTS `news` (
	`id`       INT UNSIGNED        NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`headline` VARCHAR(100)        NOT NULL
	COMMENT 'News Headline',
	`content`  LONGTEXT            NOT NULL
	COMMENT 'News Content',
	`expires`  DATE                         DEFAULT NULL
	COMMENT 'Date to stop displaying the news item',
	`active`   TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,
	`created`  DATETIME                     DEFAULT NULL,
	`modified` DATETIME                     DEFAULT NULL
);

# BRIDGE TABLES

CREATE TABLE IF NOT EXISTS `listings_attrs` (
	`id`         INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`listing_id` INT UNSIGNED NOT NULL,
	`attr_id`    INT UNSIGNED NOT NULL,
	`value`      VARCHAR(250) DEFAULT NULL
	COMMENT '{{name}} Value',
	UNIQUE `u_idx_listings_attrs_listing_attr`(`listing_id`, `attr_id`),
	INDEX `idx_listings_attrs_listing_id`(`listing_id`),
	INDEX `idx_listings_attrs_attr_id`(`attr_id`),
	INDEX `idx_listings_attrs_value`(`value`),
	CONSTRAINT `fk_listings_attrs__listings`
	FOREIGN KEY (`listing_id`) REFERENCES `listings` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `fk_listings_attrs__item_attrs`
	FOREIGN KEY (`attr_id`) REFERENCES `item_attrs` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS `listings_photos` (
	`id`         INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	`listing_id` INT UNSIGNED NOT NULL,
	`photo_id`   INT UNSIGNED NOT NULL,
	`caption`    VARCHAR(150)      DEFAULT NULL,
	`position`   SMALLINT UNSIGNED DEFAULT NULL,
	UNIQUE `u_idx_listings_photos_listing_photo`(`listing_id`, `photo_id`),
	INDEX `idx_listings_photos_photo_id`(`photo_id`),
	INDEX `idx_listings_photos_listing_id`(`listing_id`),
	CONSTRAINT `fk_listings_photos__listings`
	FOREIGN KEY (`listing_id`) REFERENCES `listings` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE,
	CONSTRAINT `fk_listings_photos__photos`
	FOREIGN KEY (`photo_id`) REFERENCES `photos` (`id`)
		ON UPDATE CASCADE
		ON DELETE CASCADE
);

# CREATE TABLE IF NOT EXISTS `parks_attrs` (
#   `id`       INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
# 	`park_id`  INT UNSIGNED NOT NULL,
# 	`attr_id` INT UNSIGNED NOT NULL,
# 	`value`    VARCHAR(250) DEFAULT NULL
# 	COMMENT '{{name}} Value',
# 	UNIQUE `u_idx_parks_attrs_park_attr`(`park_id`, `attr_id`),
# 	INDEX `idx_parks_attrs_park_id`(`park_id`),
# 	INDEX `idx_parks_attrs_attr_id`(`attr_id`),
# 	INDEX `idx_parks_attrs_value`(`value`),
# 	FOREIGN KEY (`park_id`) REFERENCES `parks` (`id`)
# 		ON UPDATE CASCADE
# 		ON DELETE CASCADE,
# 	FOREIGN KEY (`attr_id`) REFERENCES `item_attrs` (`id`)
# 		ON UPDATE CASCADE
# 		ON DELETE CASCADE
# );

# CREATE TABLE IF NOT EXISTS `parks_photos` (
#   `id`       INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
# 	`park_id`  INT UNSIGNED NOT NULL,
# 	`photo_id` INT UNSIGNED NOT NULL,
# 	`caption`  VARCHAR(150) DEFAULT NULL,
# 	UNIQUE `u_idx_parks_photos_park_photo`(`park_id`, `photo_id`),
# 		INDEX `idx_listings_photos_photo_id`(`photo_id`),
# 	INDEX `idx_listings_photos_listing_id`(`park_id`),
# 	FOREIGN KEY (`photo_id`) REFERENCES `photos` (`id`)
# 		ON UPDATE CASCADE ON DELETE CASCADE,
# 	FOREIGN KEY (`park_id`) REFERENCES `parks` (`id`)
# 		ON UPDATE CASCADE ON DELETE CASCADE
# );

## PROCEDURES
DROP PROCEDURE IF EXISTS `AddColumnIfNotExists`;
DROP PROCEDURE IF EXISTS `AddForeignKeyIfNotExists`;
DROP PROCEDURE IF EXISTS `DropForeignKeyIfExists`;
DELIMITER $$
CREATE PROCEDURE `AddColumnIfNotExists`(
	IN dbName    TINYTEXT,
	IN tableName TINYTEXT,
	IN colName   TINYTEXT,
	IN colDef    TEXT)
	BEGIN
		IF NOT EXISTS(
				SELECT *
				FROM `information_schema`.`COLUMNS`
				WHERE `COLUMN_NAME` = colName
				      AND `TABLE_NAME` = tableName
				      AND `TABLE_SCHEMA` = dbName)
		THEN
			SET @ddl = CONCAT('ALTER TABLE ', dbName, '.', tableName,
			                  ' ADD COLUMN ', colName, ' ', colDef);
			PREPARE stmnt FROM @ddl;
			EXECUTE stmnt;
			DEALLOCATE PREPARE stmnt;

		END IF;
	END;
$$

CREATE PROCEDURE `AddForeignKeyIfNotExists`(
	IN dbName    TINYTEXT,
	IN tableName TINYTEXT,
	IN colName   TINYTEXT,
	IN dbRef     TINYTEXT,
	IN tableRef  TINYTEXT,
	IN colRef    TINYTEXT,
	IN onUpdate  TINYTEXT,
	IN onDelete  TINYTEXT)
	BEGIN
		IF ISNULL(onUpdate)
		THEN
			SET onUpdate = '';
		ELSE
			SET onUpdate = CONCAT(' ON UPDATE ', onUpdate);
		END IF;
		IF ISNULL(onDelete)
		THEN
			SET onDelete = '';
		ELSE
			SET onDelete = CONCAT(' ON DELETE ', onDelete);
		END IF;

		IF NOT EXISTS(
				SELECT *
				FROM `information_schema`.`KEY_COLUMN_USAGE`
				WHERE `COLUMN_NAME` = colName
				      AND `TABLE_NAME` = tableName
				      AND `TABLE_SCHEMA` = dbName
				      AND `REFERENCED_COLUMN_NAME` = colRef
				      AND `REFERENCED_TABLE_NAME` = tableRef
				      AND `REFERENCED_TABLE_SCHEMA` = dbRef)
		THEN
			SET @ddl = CONCAT('ALTER TABLE ', dbName, '.', tableName,
			                  ' ADD FOREIGN KEY (', colName, ') REFERENCES ',
			                  dbRef, '.', tableRef, '(', colRef, ')', onUpdate, onDelete);
			PREPARE stmnt FROM @ddl;
			EXECUTE stmnt;
			DEALLOCATE PREPARE stmnt;
		END IF;
	END;

CREATE PROCEDURE `DropForeignKeyIfExists`(
	IN fkName    TINYTEXT,
	IN tableName TINYTEXT,
	IN dbName    TINYTEXT)
	BEGIN
		IF EXISTS(
				SELECT *
				FROM `information_schema`.`TABLE_CONSTRAINTS`
				WHERE `CONSTRAINT_TYPE` = 'FOREIGN KEY'
				      AND `CONSTRAINT_NAME` = fkName
				      AND `TABLE_NAME` = tableName
				      AND `TABLE_SCHEMA` = dbName)
		THEN
			SET @ddl = CONCAT('ALTER TABLE ', dbName, '.', tableName,
			                  ' DROP FOREIGN KEY ', fkName);
			PREPARE stmnt FROM @ddl;
			EXECUTE stmnt;
			DEALLOCATE PREPARE stmnt;
		END IF;
	END;
$$
DELIMITER ;
