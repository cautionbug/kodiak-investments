USE `websites`;

INSERT IGNORE INTO `captchas` (`question`, `answers`) VALUES
	('Multiply 2 by 5, then add three:', 'thirteen|13'),
	('The day between Tuesday and Thursday is:', 'Wednesday'),
	('What popular holiday occurs in December?', 'Christmas|Xmas|X-mas|HumanLight'),
	('Give the missing number: 5 x __ = twenty:', 'four|4'),
	('What is the first three-letter word in this question?', 'the');

#----------

USE kodiak;

INSERT IGNORE INTO `terms` (`name`) VALUES
	('Sale Only'),
	('Rent/Lease Only'),
	('Terms Negotiable');

INSERT IGNORE INTO `item_traits` (`name`, `abbrev`, `requires_value`) VALUES
	('Parking', 'Parking', 1),
	('Bedrooms', 'BdRm', 1),
	('Bathrooms', 'Bath', 1),
	('Laundry Hookups', 'W/D', 0),
	('Furnished', 'Furnished', 0);

INSERT IGNORE INTO `house_conditions` (`name`) VALUES
	('New'),
	('Refurbished'),
	('Needs Work');

