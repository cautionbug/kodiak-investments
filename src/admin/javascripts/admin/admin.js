/**
 * Created by Joe Theuerkauf on 9/18/2014.
 *
 * This file is for Always-Run ADMIN code.
 * Every page uses this code consistently, or code here doesn't apply to the page and does nothing.
 */
'use strict';
(function ($, W, D, F, undefined) {
    var $this;
    $(D)
        .on('click', 'a[data-toggle-active]',
            function (event) {
                var $tr = ($this = $(this)).closest('tr'),
                    tdIdx = $this.closest('td').index(),
                    hrefData = this.href.replace(/\/$/, '').split('/'),
                    $trReplace;
                $.post(
                    $this.attr('href'), {
                        id    : hrefData.pop(),
                        active: hrefData.pop()
                    },
                    function (data) {
                        // To keep things clean, only show a modal if there's a problem.
                        // Otherwise, toggle the De/Activate link with a little color pulse.
                        if (data.pass) {
                            $tr.replaceWith($trReplace = $(data.content));
                            $($trReplace.children().get(tdIdx)).addClass(data.html.class);
                        }
                        if (data.reveal) {
                            $(data.reveal).stripTelLinks().appendTo('body').closest(D).foundation().end()
                                // Context is now $content, to open this specific one.
                                .foundation('reveal', 'open');
                        }
                    }, 'json'
                );
                return false;
            });
    //.on('click', 'input[data-requires-value]',
    //    function (event, triggered) {
    //        var checked = ($this = $(this)).is(':checked'),
    //            $text = $this.parent('label')
    //                .parent('div')[checked ? 'addClass' : 'removeClass']('required outside-left')
    //                .children('input[data-required-value]')
    //                    .prop({required: checked, disabled: !checked})
    //                    [checked ? 'removeClass' : 'addClass']('hide');
    //        (!triggered) && $text.not('.hide')[0].focus();
    //    })
    //.find('input[data-requires-value][checked]').trigger('click', [true]);
    Thook.CheckReveal.init('input[data-requires-value]');
    $('form:not(#login-form)')
        .find('input[name=password]')
        .on({
                'focus mouseover': function () {
                    $(this).attr('type', 'text');
                },
                'blur mouseout'  : function () {
                    if (!($this = $(this)).is(':focus')) {
                        $this.attr('type', 'password');
                    }
                }
            });
}(window.jQuery, window, document, Foundation));
