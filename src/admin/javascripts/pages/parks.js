/**
 * Created by Joe Theuerkauf on 2014-10-04.
 */
'use strict';
(function ($, W, D, undefined) {
    $('#name').on('change', function(){
        $('#file-name-no-ext').val($(this).val().replace(/[^\da-z ]/ig, '').replace(/\s+/g, '-'));
    });
}(window.jQuery, window, document));
