/**
 * Created by joe on 7/1/2014.
 */

// TODO: Find a way to move this into a CTP or something else where page-specific JS can be merged with it.
'use strict';
Modernizr.load([
                 {
                   test:window.jQuery,
                   nope:'/assets/jquery/jquery.min.js',
                   both:[
                     //'//cdn.jsdelivr.net/g/fastclick@1(fastclick.min.js)',
                     '/libs/jqueryui/jquery-ui.min.js',
                     '/js/libs/jquery/smartresize.jquery.min.js',
                     '/js/libs/jquery/detectmobilebrowser.min.js',
                     '/js/startup.js'
                   ]
                   //complete:function () {
                   //  Modernizr.load([
                   //                   '/js/libs/foundation/foundation.min.js',
                   //                   '/libs/jqueryui/jquery-ui.min.js',
                   //                   '/js/libs/jquery/smartresize.jquery.min.js',
                   //                   '/js/libs/jquery/detectmobilebrowser.min.js',
                   //                   '/js/startup.js'
                   //                 ]);
                   //}
                 //},
                 //{
                 //  test:window.FastClick,
                 //  nope:'/assets/foundation/js/vendor/fastclick.js'
                 //},
                 //{
                 //  test:window.Foundation,
                 //  nope:'/js/libs/foundation/js/foundation.min.js',
                 //  both:'/js/startup.js'
                   //},
                   //{
                   //  test:Modernizr.input.placeholder,
                   //  nope:'/assets/foundation/js/vendor/placeholder.js'
                 }
               ]);
