/**
 * Created by Joe Theuerkauf on 2014-12-14.
 */

/**
 * Round to specified precision
 * @param N Number to round
 * @param p precision
 * @returns {number}
 */
Math.roundBetter = function (N, p) {
    var _a = Math.pow(10, p || 0);
    return Math.round(N * _a) / _a;
};

/**
 * Created by joe on 8/31/2014.
 */

$.fn.stripTelLinks = function () {
    'use strict';
    if ($.browser.mobile === false) {
        this.each(
            function () {
                $(this).replaceWith(this.childNodes);
            }
        );
    }
};

window.Thook = {
    // Set any properties here to be used as "constants" in all subsequent plugins
};

window.Thook.CheckReveal = {
    init: function(inputs) {
        inputs = $(inputs).each(this.enableRevealed);
        this.bindUIActions(inputs);
    },

    bindUIActions: function(inputs) {
        inputs.on('change', this.enableRevealed);
    },

    enableRevealed: function() {
        var $this = $(this),
            isChecked = $this.prop('checked'),
            $value = $('#' + $this.data('requires-value'))[isChecked ? 'removeClass' : 'addClass']('hide');
        $this.closest('div')[isChecked && $value.prop('required') ? 'addClass' : 'removeClass']('required outside-left')
            .find(':input').not(this).prop('disabled', !isChecked);
        isChecked && ($value.length && $value[0].focus());
    }
};
