/**
 * Created by joe on 8/31/2014.
 */

$.fn.stripTelLinks = function(){
	'use strict';
	if ($.browser.mobile === false) {
		this.each(function () {
			$(this).replaceWith(this.childNodes);
		});
	}
};
