/**
 * Created by joe on 7/27/2014.
 */
'use strict';

window.ParsleyExtend = {
	asyncValidators: {
		captcha_json: {
			fn: function (xhr) {
				var newCaptcha, remoteOptions;
				if (!xhr.responseJSON.valid) {
					newCaptcha = xhr.responseJSON.captcha;
					(remoteOptions = this.$element.val('').data('parsley-remote-options')).data.id = newCaptcha.id;
					this.$element.attr('data-parsley-remote-options', JSON.stringify(remoteOptions));
					$('label[for=' + this.$element.attr('id') + ']').html($.parseHTML(newCaptcha.question));
				}
				return xhr.responseJSON.valid;
			},
			url : '/captchas/check.json'
		}
	}
};
