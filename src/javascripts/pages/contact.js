/**
 * Created by joe on 7/6/2014.
 */

'use strict';
(function ($, F, W, D, undefined) {
	var $form = $('form');

	$form.on('submit', function(event){ event.preventDefault(); });

	$.listen('parsley:form:validated', function (prslyForm) {
        var $form = prslyForm.$element;
        if (prslyForm.isValid()) {
            $form.find('#captcha').closest('div.input').replaceWith('<h4 class="brit text-center">Thank you, human.</h4>');
            $form.find('[type=submit]').prop('disabled', true);
            $[$form.attr('method')](
                $form.attr('action'), $form.serialize(), function (data) {
                    //var $content = $(data.content);
                    //$content.appendTo('body').closest(D).foundation().end()
                    $(data.content).stripTelLinks().appendTo('body').closest(D).foundation().end()
                        // Context is now $content, to open this specific one.
                        .foundation('reveal', 'open');
                    //if (data.pass) {
                    //	// Set up the fail reveal.
                    //}
                    //else {
                    //	// Set up the fail reveal.
                    //}
                }, 'json'
            );
        }
        return false;
    });
}(window.jQuery, window.Foundation, window, document));
