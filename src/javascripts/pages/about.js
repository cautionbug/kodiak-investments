/**
 * Created by joe on 7/9/2014.
 */

'use strict';
(function ($, W, D) {
    var _gmapId = 'gmap-js', _gmapJs;
    $(D).on('replace.fndtn.interchange', '.has-maps[data-interchange]',
            function () {
                if (!D.getElementById(_gmapId)) {
                    _gmapJs = D.createElement('script');
                    _gmapJs.id = _gmapId;
                    _gmapJs.src = '//maps.googleapis.com/maps/api/js?v=3.exp' +
                                  '&libraries=places&sensor=false' +
                                  '&key=AIzaSyBRzdfkR7UYMg05AXE81F_o6X9C-4s9OyI' +
                                  '&callback=Kodiak.Maps.init';
                    D.body.appendChild(_gmapJs);
                }
            });
}(window.jQuery, window, document));
