/*
Foundation Calendar Date Picker - Jocko MacGregor

Source available at: https://github.com/jockmac22/foundation_calendar_date_time_picker

Original Source Credit: Robert J. Patrick (rpatrick@mit.edu)
*/

$.fn.fcdp = function(opts, p1, p2) {
	var type = $.type(opts);
	if (type == 'object') {
		$(this).each(function() {
			$.fcdp.buildUI($(this), opts);
		});
	} else if (type == 'string') {
		$(this).each(function() {
			$.fcdp.execute($(this), opts, p1, p2);
		});
	}
};

$.fcdp = {
	init: function() {
		$('input[data-date-time]').add($('input[data-date]')).add($('input[data-time]')).each(function() {
			$.fcdp.buildUI($(this));
		});

		// Wireup the body to hide display elements when clicked.
		$(document).click(function(evt) {
			$('.calendar').not('.fixed').find('.date-picker').hide()
				.closest('.calendar').find('.time-picker').hide();
		});
	},

	/***
		Date Manipulation Helper Methods
	 ***/
	getDateFromString: function(str, nullable) {
		nullable = nullable || true;
		return str
			? new Date.parse(str)
			: (nullable ? null : new Date());
	},

	moveMonth: function(date, months) {
		return date.add({ months: months });
	},

	daysInMonth: function(date) {
		lastDay = new Date(date.getFullYear(), date.getMonth()+1, 0);
		return lastDay.getDate();
	},

	dateParts: function(date) {
		var parts = {
			year: date.getFullYear(),
			month: date.getMonth(),
			day: date.getDate(),
			hour: date.getHours(),
			minute: date.getMinutes(),
			second: date.getSeconds()
		};

		//parts.year += parts.year < 2000 ? 1900 : 0;
		return parts;
	},

	/***
		UI Date Management Methods
	 ***/
	getFieldDate: function(opts) {
		var val = opts.input.val();
		date = val
			? this.getDateFromString(val)
			: (opts.nullable ? null : new Date());
		return date;
	},

	// Sets the field date, date option must always be in UTC.
	setFieldDate: function(opts, date) {
		var inputVal = date ? date.format(opts.formats['value']) : '';

		opts.input.val(inputVal);

		date = date ? date.add(opts.utcOffset).hours() : date;
		this.setWorkingDate(opts, date);
	},

	getWorkingDate: function(opts) {
		var date_attr = opts.input.data('working-date'), date;
		if (!date_attr || ('' + date_attr).length === 0) {
			date = new Date();
			this.setWorkingDate(opts, date, true);
		} else {
			date = this.getDateFromString(date_attr);
		}
		return date;
	},

	setWorkingDate: function(opts, date, skipDisplay) {
		var dateVal, timeVal = dateVal = '--';

		opts.input.data('working-date', date ? date.format('%Y-%m-%d %H:%M:%S') : '');

		if (date && !skipDisplay) {
			dateVal = date.format(opts.formats['date']);
			timeVal = date.format(opts.formats['time']);
		}

		if (opts.dateSelector) { opts.dateSelector.find('.value').html(dateVal); }
		if (opts.timeSelector) { opts.timeSelector.find('.value').html(timeVal); }
	},


	/***
		UI Construction and Event Handling
	 ***/
	buildUI: function(input, opts) {
		var cal, sel, utcOffset, hasTimePicker, hasDatePicker, c_opts,
		    clear, ds, dp, ts, tp, val;
		// Wrap the input, and hide it from display.
		input
			.wrap('<div class="calendar"><div class="hidden"></div></div>')
			.addClass('value');

		// Generate the date/time selector container and add it to the calendar
		sel = $('<div class="selector"></div>');

		// Generate the calendar container
		cal = input.closest('.calendar').append(sel);

		// Set UTC offset from the unobtrusive javascript
		utcOffset = input.is('[data-utc-offset]') ? (parseInt(input.data('utc-offset'), 10) || 0) : 0;
		//utcOffset = isNaN(utcOffset) ? 0 : utcOffset;

		// Determine if the time and datepicker states
		hasTimePicker = (input.is('[data-time]') || input.is('[data-date-time]')) ? true : false;
		hasDatePicker = (input.is('[data-date]') || input.is('[data-date-time]')) ? true : !hasTimePicker;

		// Establish a default set of options for the calendar based on unobtrusive tags in the
		// input field.
		c_opts = {
			input: input,
			calendar: cal,
			formats: {
				date: input.is('[data-date-format]') ? input.data('date-format') : '%A: %B %-d, %Y',
				time: input.is('[data-time-format]') ? input.data('time-format') : '%-I:%M %p',
				value: input.is('[data-value-format]') ? input.data('value-format') : '%Y-%m-%d %H:%M:%S'
			},
			hasTimePicker: hasTimePicker,
			hasDatePicker: hasDatePicker,
			nullable: input.is('[data-nullable]'),
			utcOffset: utcOffset,
			minDate: input.is('[data-min-date]') ? this.getDateFromString(input.data('min-date')) : null,
			maxDate: input.is('[data-max-date]') ? this.getDateFromString(input.data('max-date')) : null,
			fixed: input.is('[data-fixed]') ? true : false
		};

		// Incorporate the options that were passed in with the build call if they
		// are present.
		opts = opts
			? $.extend({}, c_opts, opts)
			: c_opts;

		if (opts.fixed) {
			cal.addClass('fixed');
		}
		// If there's a date picker, generate its display.
		if (opts.hasDatePicker) {
			if (!opts.fixed) {
				ds = $('<a class="date-selector"></a>')
					.append('<i class="fi-calendar"></i><span class="value"></span>');
				sel.append(ds).addClass('date');
			}

			dp = $('<div class="date-picker"></div>');
			cal.append(dp);

			// Prevent click events on the date-picker from bubbling
			// up to the body.
			dp.click(function(evt) {
				evt.stopPropagation();
			});
		}

		// If there's a time picker, generate its display.
		if (opts.hasTimePicker && !opts.fixed) {
			ts = $('<a class="time-selector"></a>')
				.append('<i class="fi-clock"></i><span class="value"></span>');
			sel.append(ts).addClass('time');

			tp = $('<div class="time-picker"></div>');
			cal.append(tp);

			// Prevent click events on the time-picker from bubbling
			// up to the body.
			tp.click(function(evt) {
				evt.stopPropagation();
			});
		}

		if (opts.nullable) {
			clear = $('<a class="clear"><i class="fi-x"></i></a>');
			sel.append(clear);
		}

		// Prevent click events on the selectors from bubbling
		// up to the body.
		sel.click(function(evt) {
			evt.stopPropagation();
		});

		// Add DOM element references to the options to reduce DOM queries in future calls.
		opts = $.extend(opts, {
			dateSelector: hasDatePicker ? cal.find('.date-selector') : null,
			datePicker: hasDatePicker ? cal.find('.date-picker') : null,
			timeSelector: hasTimePicker ? sel.find('.time-selector') : null,
			timePicker: hasTimePicker ? cal.find('.time-picker') : null,
			clearButton: opts.nullable ? cal.find('a.clear') : null,
		});

		cal.data('opts', opts);
		input.data('opts', opts);

		if (opts.dateSelector) {
			opts.dateSelector.click(function(evt) {
				var cal = $(this).closest('.calendar'),
				    tp = cal.find('.time-picker'),
				    dp = cal.find('.date-picker'),
				    ds = cal.find('.date-selector');
				evt.preventDefault();
				dp.css({ top: ds.position().top + ds.outerHeight(), left: ds.position().left });
				tp.hide();
				dp.toggle();
			});
		}

		if (opts.timeSelector) {
			cal.find('a.time-selector').click(function(evt) {
				var cal = $(this).closest('.calendar'),
				    dp = cal.find('.date-picker'),
				    tp = cal.find('.time-picker'),
				    ts = cal.find('.time-selector');
				evt.preventDefault();
				tp.css({ top: ts.position().top + ts.outerHeight(), right: ts.position().right });
				dp.hide();
				tp.toggle();
			});
		}

		if (opts.clearButton) {
			opts.clearButton.click(function(evt) {
				var opts = $(this).closest('.calendar').data('opts');
				evt.preventDefault();
				opts.datePicker.add(opts.timePicker).hide();
				$.fcdp.setFieldDate(opts, null);
			});
		}

		this.setFieldDate(opts, this.getFieldDate(opts), !!opts.input.val() || opts.nullable);
		this.buildCalendar(opts);
		this.buildTime(opts);
		this.updateTimePicker(opts);
	},

	buildTime: function(opts) {
		var tp, header, time_label, time;
		if ((tp = opts.timePicker)) {
			time_label = $('<div class="time">Time</div>');
			header = $('<div class="header"></div>')
				.append(time_label);

			tp.append(header);

			time = $('<div class="time"></div>')
				.append('<div class="value-control hour"><label>Hr</label><a class="value-change up"><span></span></a><input type="text" class="display" value="12" /><a class="value-change down"><span></span></a></div>')
				.append('<div class="value-control minute"><label>Min</label><a class="value-change up"><span></span></a><input type="text" class="display" value="00" /><a class="value-change down"><span></span></a></div>')
				.append('<div class="value-control second"><label>Sec</label><a class="value-change up"><span></span></a><input type="text" class="display" value="00" /><a class="value-change down"><span></span></a></div>')
				.append('<div class="value-control ampm"><label>A/P</label><a class="value-change up"><span></span></a><input type="text" class="display" value="AM" /><a class="value-change down"><span></span></a></div>');

			tp.append(time);

			this.wireupTime(opts);
		}
	},

	wireupTime: function(opts) {
		var vc;
		if (opts.timePicker) {
			vc = opts.timePicker.find('.value-control');
			this.wireupTimeValueControl(vc.filter('.hour'), 1, 12, 1);
			this.wireupTimeValueControl(vc.filter('.minute'), 0, 59, 2);
			this.wireupTimeValueControl(vc.filter('.second'), 0, 59, 2);
			this.wireupTimeAmPmControl(vc.filter('.ampm'));
		}
	},

	wireupTimeAmPmControl: function(ampm) {
		ampm
			.on('click', '.value-change',
		        function (evt) {
			        "use strict";
			        var $this = $(this),
			            tvc = $this.closest('.value-control'),
			            val = tvc.find('input.display').val().toLowerCase();

			        evt.preventDefault();

			        val = val === 'am' ? 'PM' : 'AM';

			        tvc.find('input.display').val(val);
			        $.fcdp.updateTime($this.closest('.calendar').data('opts'));
		        })
			.on('click', 'input.display',
		        function () {
			        "use strict";
			        var $this = $(this),
			            tvc = $this.closest('.value-control'),
			            val = tvc.find('input.display').val().toLowerCase()[0];

			        val = val == 'p' ? 'PM' : 'AM';

			        tvc.find('input.display').val(val);
			        $.fcdp.updateTime($this.closest('.calendar').data('opts'));

		        }
		);

		//ampm.find('.value-change').click(function(evt) {
		//	var $this = $(this),
		//	    tvc = $this.closest('.value-control'),
		//	    val = tvc.find('input.display').val().toLowerCase();
        //
		//	evt.preventDefault();
        //
		//	val = val === 'am' ? 'PM' : 'AM';
        //
		//	tvc.find('input.display').val(val);
		//	$.fcdp.updateTime($this.closest('.calendar').data('opts'));
		//});
        //
		//ampm.find('input.display').change(function(evt) {
		//	var $this = $(this);
		//	var tvc = $this.closest('.value-control');
		//	var val = tvc.find('input.display').val().toLowerCase()[0];
        //
		//	val = val == 'p' ? 'PM' : 'AM';
        //
		//	tvc.find('input.display').val(val);
		//	$.fcdp.updateTime($this.closest('.calendar').data('opts'));
		//});
	},

	wireupTimeValueControl: function(tvc, min, max, pad) {
		tvc.data('opts', {
			max: max,
			min: min,
			pad: pad
		})
			.on('click', '.value-change.up',
		        function (evt) {
			        var $this = $(this),
			            tvc = $this.closest('.value-control'),
			            opts = tvc.data('opts'),
			            val = parseInt(tvc.find('input.display').val(), 10),
			            calOpts = $this.closest('.calendar').data('opts');

			        evt.preventDefault();
			        val += 1;
			        val = val > opts.max ? opts.min : val;
			        tvc.find('input.display').val(('' + val).lpad(pad));
			        $.fcdp.updateTime(calOpts);
		        })
			.on('click', '.value-change.down',
		        function (evt) {
			        var $this = $(this),
			            tvc = $this.closest('.value-control'),
			            opts = tvc.data('opts'),
			            val = parseInt(tvc.find('input.display').val(), 10),
			            calOpts = $this.closest('.calendar').data('opts');

			        evt.preventDefault();
			        val -= 1;
			        val = val < opts.min ? opts.max : val;
			        tvc.find('input.display').val(('' + val).lpad(pad));
			        $.fcdp.updateTime(calOpts);
		        })
			.on('change', 'input.display',
		        function () {
			        var $this = $(this),
			            tvc = $this.closest('.value-control'),
			            opts = tvc.data('opts'),
			            val = parseInt(tvc.find('input.display').val(), 10),
			            calOpts = $this.closest('.calendar').data('opts');

			        if (isNaN(val)) {
				        val = opts.min;
			        }
			        else {
				        val = val > opts.max ? opts.max : (val < opts.min ? opts.min : val);
			        }


			        tvc.find('input.display').val(('' + val).lpad(pad));
			        $.fcdp.updateTime(calOpts);
		        });
		//tvc.find('.value-change.up').click(function(evt) {
		//	var $this = $(this),
		//	    tvc = $this.closest('.value-control'),
		//	    opts = tvc.data('opts'),
		//	    val = parseInt(tvc.find('input.display').val()),
		//	    calOpts = $this.closest('.calendar').data('opts');
        //
		//	evt.preventDefault();
		//	val += 1;
        //
		//	val = val > opts.max ? opts.min : val;
        //
		//	tvc.find('input.display').val((''+val).lpad(pad));
		//	$.fcdp.updateTime(calOpts);
		//});
        //
		//tvc.find('.value-change.down').click(function(evt) {
		//	var $this = $(this);
		//	var tvc = $this.closest('.value-control');
		//	var opts = tvc.data('opts');
		//	var val = parseInt(tvc.find('input.display').val());
		//	evt.preventDefault();
        //
		//	val -= 1;
		//	val = val < opts.min ? opts.max : val;
        //
		//	tvc.find('input.display').val((''+val).lpad(pad));
        //
		//	var calOpts = $this.closest('.calendar').data('opts');
		//	$.fcdp.updateTime(calOpts);
		//});
        //
		//tvc.find('input.display').change(function(evt) {
		//	var $this = $(this);
		//	var tvc = $this.closest('.value-control');
		//	var opts = tvc.data('opts');
		//	var val = parseInt(tvc.find('input.display').val());
        //
		//	if (isNaN(val)) {
		//		val = opts.min;
		//	} else {
		//		val = val > opts.max ? opts.max : (val < opts.min ? opts.min : val);
		//	}
        //
		//	tvc.find('input.display').val((''+val).lpad(pad));
        //
		//	var calOpts = $this.closest('.calendar').data('opts');
		//	$.fcdp.updateTime(calOpts);
		//});
	},

	updateTimePicker: function(opts) {
		var vc, fieldDate;
		if (opts.timePicker) {
			fieldDate = this.getWorkingDate(opts);
			vc = opts.timePicker.find('.value-control');
			if (fieldDate) {
				vc.filter('.hour').find('input.display').val(fieldDate.format('%-I'));
				vc.filter('.minute').find('input.display').val(fieldDate.format('%M'));
				vc.filter('.second').find('input.display').val(fieldDate.format('%S'));
				vc.filter('.ampm').find('input.display').val(fieldDate.format('%p'));
			}
		}
	},

	updateTime: function(opts) {
		var vc, wDate, newDate, hour, minute, second, ampm;
		if (opts.timePicker) {
			vc = opts.find('.value-control');
			hour = parseInt(vc.filter('.hour').find('input.display').val(), 10) || 0;
			minute = parseInt(vc.filter('.minute').find('input.display').val(), 10) || 0;
			second = parseInt(vc.filter('.second').find('input.display').val(), 10) || 0;
			ampm = vc.filter('.ampm').find('input.display').val();

			hour = hour == 12 ? 0 : hour;
			if (ampm.toLowerCase() === 'pm') {
				hour += 12;
			}

			hour %= 24;

			wDate = this.getWorkingDate(opts);
			newDate = new Date(wDate.getFullYear(), wDate.getMonth(), wDate.getDate(), hour, minute, second);
			newDate = newDate ? newDate.add(-opts.utcOffset).hours() : newDate;
			this.setFieldDate(opts, newDate);

			opts.input.trigger('timeChange', [opts]);
		}
	},

	buildCalendar: function(opts) {
		var i = 0, ls = ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
		    dp, workingDate, fieldDate, parts,
		    startingPos, days, week, previousMonth, daysInPreviousMonth, header,
		    labels, weekday_num, day_opts;
		if ((dp = opts.datePicker)) {
			dp.empty();

			workingDate = this.getWorkingDate(opts);
			fieldDate = this.getFieldDate(opts) || new Date();

			parts = this.dateParts(workingDate);
			startingPos = new Date(parts.year, parts.month, 1).getDay();
			days = this.daysInMonth(workingDate) + startingPos;
			week = $('<div class="week"></div>');
			previousMonth = this.moveMonth(this.getWorkingDate(opts), -1);
			daysInPreviousMonth = this.daysInMonth(previousMonth);

			header = $('<div class="header"></div>')
				.append('<a href="#" class="month-nav prev"><span></span></a>')
				.append('<a href="#" class="month-nav next"><span></span></a>')
				.append('<div class="month">' + workingDate.format('%B %Y') + '</div>');

			dp.append(header);

			labels = $('<div class="week labels"></div>');
			for (; i < 7 ; ++i) {
				labels.append('<div class="day">' + ls[i] + '</div>');
			}
			dp.append(labels);

			// Iterate the maximum 6 weeks of days (6 is the maximum number of weeks
			// on a calendar for any given month).
			for (i = 0; i < 42; ++i) {
				weekday_num = i % 7;

				day_opts = {
					date: undefined,
					weekday_num: weekday_num,
					is_weekend: (weekday_num === 0) || (weekday_num === 6),
					is_current: false,
					day_number: 0
				};

				// If i is outside of the starting pos and the days in the
				// month, then we are in another month, so generate a non-link
				// display
				if (i < startingPos || i >= days) {
					if (i < startingPos) {
						day_opts.day_number = ((daysInPreviousMonth - (startingPos - 1)) + i);
					} else if (i >= days) {
						day_opts.day_number = (i - days + 1);
					}

					week.append(this.buildOtherMonthDayUI(opts, day_opts));
				// Otherwise we are in the month, so generate a numbered current month display.
				} else {
					day_opts.day_number = i - startingPos + 1;
					day_opts.date = new Date(parts.year, parts.month, day_opts.day_number, parts.hour, parts.minute, parts.second);
					day_opts.is_current = (fieldDate.format('%Y%m%d') == day_opts.date.format('%Y%m%d')),
					week.append(this.buildDayUI(opts, day_opts));
				}

				// If we're at the end of the week, append the week to the display, and
				// generate a new week
				if (weekday_num === 6) {
					dp.append(week);
					week = $('<div class="week"></div>');
				}
			}

			this.wireupCalendar(opts);
		}
	},

	buildOtherMonthDayUI: function(opts, day_opts) {
		var response = this.executeBehavior('buildOtherMonthDayUI', opts, day_opts),
			clazz = 'day other-month' + (day_opts.is_weekend ? ' weekend' : '');

		// If no response from the custom bound behaviors, generate the default response.
		if (!response) {
			response = '<div class="' + clazz + '">' + day_opts.day_number + '</div>';
		}

		return response;
	},

	buildDayUI: function(opts, day_opts) {
		var day_num = day_opts.date.getDate(),
		    response = this.executeBehavior('buildDayUI', opts, day_opts),
		    clazz = 'day' + (day_opts.is_weekend ? ' weekend' : '') + (day_opts.is_current ? ' current' : '');
		day_opts.is_clickable = this.dateIsClickable(opts, day_opts);

		// If no response from the custom method, generate the default response.
		if (!response) {
			response = (day_opts.is_clickable)
				? '<a href="#' + day_num + '" class="' + clazz + '" data-date="' + day_opts.date.format() + '">' + day_num + '</a>'
				: '<span class="' + clazz + '" data-date="' + day_opts.date.format() + '">' + day_num + '</span>';
		}

		return response;
	},

	dateIsClickable: function(opts, day_opts) {
		var response = this.executeBehavior('dateIsClickable', opts, day_opts);

		response = ((opts.minDate && day_opts.date < opts.minDate) || (opts.maxDate && day_opts.date > opts.maxDate))
			? false
			: (response === null ? true : response);

		return response;
	},

	wireupCalendar: function(opts) {
		var dp; //, opts, prevMonth, nextMonth;
		if ((dp = opts.datePicker)) {
			dp
				.on('click', 'a.month-nav.prev',
			        function (evt) {
				        var opts = $(this).closest('.calendar').data('opts'),
				            prevMonth = $.fcdp.moveMonth($.fcdp.getWorkingDate(opts), -1);
				        evt.preventDefault();
				        $.fcdp.setWorkingDate(opts, prevMonth);
				        $.fcdp.buildCalendar(opts);

				        opts.input.trigger('monthChange', [opts]);
				        opts.input.trigger('monthPrev', [opts]);
			        })
				.on('click', 'a.month-nav.next',
			        function (evt) {
				        var opts = $(this).closest('.calendar').data('opts'),
				            nextMonth = $.fcdp.moveMonth($.fcdp.getWorkingDate(opts), 1);

				        evt.preventDefault();
				        $.fcdp.setWorkingDate(opts, nextMonth);
				        $.fcdp.buildCalendar(opts);

				        opts.input.trigger('monthChange', [opts]);
				        opts.input.trigger('monthNext', [opts]);
			        })
				.on('click', 'a.day',
			        function (evt) {
				        var $this = $(this),
				            opts = $this.closest('.calendar').data('opts'),
				            dp = opts.datePicker,
				            dayDate = $.fcdp.getDateFromString($this.attr('data-date')),
				            fieldDate = $.fcdp.getFieldDate(opts) || $.fcdp.getWorkingDate(opts),
				            newDate = new Date(dayDate.getFullYear(), dayDate.getMonth(), dayDate.getDate(),
				                               fieldDate.getHours(), fieldDate.getMinutes(), fieldDate.getSeconds());
				        evt.preventDefault();
				        dp.find('a.current').removeClass('current');

				        $this.addClass('current');
				        $.fcdp.setFieldDate(opts, newDate);

				        opts.input.trigger('dateChange', [opts]);
				        dp.hide();
			        });
			//dp.find('a.month-nav.prev').click(function(evt) {
			//	evt.preventDefault();
            //
			//	var opts = $(this).closest('.calendar').data('opts');
			//	var prevMonth = $.fcdp.moveMonth($.fcdp.getWorkingDate(opts), -1);
			//	$.fcdp.setWorkingDate(opts, prevMonth);
			//	$.fcdp.buildCalendar(opts);
            //
			//	opts.input.trigger('monthChange', [opts]);
			//	opts.input.trigger('monthPrev', [opts]);
			//});
            //
			//dp.find('a.month-nav.next').click(function(evt) {
			//	evt.preventDefault();
            //
			//	var opts = $(this).closest('.calendar').data('opts');
			//	var nextMonth = $.fcdp.moveMonth($.fcdp.getWorkingDate(opts), 1);
			//	$.fcdp.setWorkingDate(opts, nextMonth);
			//	$.fcdp.buildCalendar(opts);
            //
			//	opts.input.trigger('monthChange', [opts]);
			//	opts.input.trigger('monthNext', [opts]);
			//});
            //
			//dp.find('a.day').click(function(evt) {
			//	var $this = $(this);
			//	var opts = $this.closest('.calendar').data('opts');
			//	var dp = opts.datePicker;
            //
			//	dp.find('a.current').removeClass('current');
			//	$this.addClass('current');
            //
			//	var dayDate = $.fcdp.getDateFromString($this.attr('data-date'));
			//	var fieldDate = $.fcdp.getFieldDate(opts) || $.fcdp.getWorkingDate(opts);
            //
			//	var newDate = new Date(dayDate.getFullYear(), dayDate.getMonth(), dayDate.getDate(), fieldDate.getHours(), fieldDate.getMinutes(), fieldDate.getSeconds());
			//	$.fcdp.setFieldDate(opts, newDate);
            //
			//	opts.input.trigger('dateChange', [opts]);
			//	dp.hide();
			//});
		}
	},

	execute: function(input, cmd, p1, p2) {
		switch(cmd) {
			case 'bindBehavior':
				this.bindBehavior(input, p1, p2);
				break;
			default:
				break;
		}
	},

	bindBehavior: function(input, behavior, func) {
		var behaviors = input.data('behaviors') || {};
		if ($.isFunction(func)) {
			behaviors[behavior] = behaviors[behavior] || [];
			behaviors[behavior].push(func);
			input.data('behaviors', behaviors);
		}
	},

	executeBehavior: function(behavior, opts, addl_opts) {
		var response = null,
		    behaviors = opts.input.data('behaviors');

		if (behaviors && behaviors[behavior]) {
			$.each(behaviors[behavior], function() {
				if ($.isFunction(this)) {
					response = this(opts, addl_opts, response);
				}
			});
		}
		return response;
	}
};

$(document).ready(function() { $.fcdp.init(); });
