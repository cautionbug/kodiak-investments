/**
 * Created by joe on 7/5/2014.
 */

'use strict';
if (!window.Kodiak) {
    window.Kodiak = {};
}

(function mapScript($, M, W, D, U) {
    var self,
        mapContainerSelector = '.map-container',
        hasMapsSelector = '.has-maps',
        hasMapsVisible = hasMapsSelector + ':visible',
        $body = $('body');
    W.Kodiak.Maps = {
        elements    : [],
        init        : function () {
            (self = this).createMaps(true);
        },
        createMaps  : function (all) {
            //$body.css('overflow', 'hidden');
            (all === true
                ? $(mapContainerSelector)
                : $(all.target).find(mapContainerSelector)).each(
                function () {
                    // These variables have to be initialized in EACH iteration
                    // to ensure they're local to the particular map being generated.
                    var //mapWidth, $hasMaps,
                        $mapContainer = $(this),
                        mapCenter = [],
                        $data = $mapContainer.data('map-options');
                    if (!$mapContainer.children().length) {
                        if (!($data.lat === U || $data.lng === U)) {
                            mapCenter.push($data.lat, $data.lng);
                        }
                        //$hasMaps = $mapContainer.closest(hasMapsVisible).first();
                        //mapWidth = $hasMaps.css('width');
                        //if (($hasMaps = $hasMaps.find(hasMapsVisible)).length) {
                        //  mapWidth = $hasMaps.css('width');
                        //}
                        //noinspection MagicNumberJS
                        $mapContainer
                            .gmap3(
                            {
                                map   : {
                                    address: $data.full_address,
                                    options: {
                                        center           : mapCenter, //[$data.lat || 0, $data.lng || 0],
                                        //mapTypeControl   :false,
                                        navigationControl: false,
                                        panControl       : false,
                                        streetViewControl: false,
                                        zoom             : 16,
                                        zoomControl      : false
                                    }
                                },
                                marker: {
                                    address: $data.full_address
                                }
                            }
                        )
                            .gmap3({trigger: 'resize'});
                    }
                }
            );
        },
        recreateMaps: function () {
            $(mapContainerSelector).empty().removeAttr('style');
            this.createMaps();
        },
        resizeMap   : function (mapContainer) {
            var $this;
            $(mapContainer).each(
                function () {
                    var map, mapCenter;
                    $this = $(this);
                    if ((map = $this.gmap3('get')) === U) {
                        Kodiak.Maps.init();
                        map = $this.gmap3('get');
                    }
                    mapCenter = map.getCenter();
                    $this.gmap3({trigger: 'resize'}).gmap3('get').setCenter(mapCenter);
                }
            );

        }
    };
}(window.jQuery, window.Modernizr, window, document));
