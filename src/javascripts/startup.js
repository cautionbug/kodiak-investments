/**
 * Created by Joe Theuerkauf on 6/24/2014.
 *
 * This file is for Always-Run code.
 * Every page uses this code consistently, or code here doesn't apply to the page and does nothing.
 */
'use strict';
/**
 * Round to specified precision
 * @param N Number to round
 * @param p precision
 * @returns {number}
 */
//Math.roundBetter = function (N, p) {
//	var _a = Math.pow(10, p || 0);
//	return Math.round(N * _a) / _a;
//};

//// jQuery Mini-plugins
//$.fn.stripTelLinks = function () {
//	if ($.browser.mobile === false) {
//		this.find('a[href^="tel:"]').each(function () {
//			$(this).replaceWith(this.childNodes);
//		});
//	}
//	return this;
//};

window.ParsleyConfig = {
    errorsWrapper: '<span class="parsley-errors-list"></span>', //'<div></div>',
    errorTemplate: '<span></span>'
};

(function ($, W, D, F, M, undefined) {
    var $this, $input, $doc = $(D),
        className, widget;
// Disable telephone links for non-mobile
//	if ($.browser.mobile === false) {
//		$('a[href^="tel:"]').each(function () {
//			$(this).replaceWith(this.childNodes);
//		});
//	}
    $doc.stripTelLinks();

    // Swap <label> text for placeholders (if set) when placeholder isn't supported
    if (!M.input.placeholder) {
        $('label').each(function () {
            $this = $(this);
            $input = $('#' + $this.attr('for'));
            // If the label isn't empty, skip..
            if (!this.childNodes.length) {
                $this.html($input.attr('placeholder') || $this.html()).removeClass('hidden-for-small');
            }
        });
    }

    // Blanket initialization for Foundation.
    // Page-specific overrides may set particular options, callbacks, etc.
    // Not sure how to get around the fact of duplicating init calls without
    // creating a page-JS file for EVERY page.
    $doc.foundation();

    //$('#search-box').on('closed.fndtn.dropdown', function () {
    //    $(this).closest('[data-had-tooltip]').removeAttr('data-had-tooltip').attr('data-tooltip', '');
    //});
    //$('#nav-search').on('click', function () {
    //    var $tipContainer = $(this).closest('[data-tooltip]').removeAttr('data-tooltip').attr('data-had-tooltip', '');
    //    $('[data-selector=' + $tipContainer.attr('id') + ']').hide();
    //    $tipContainer.find('input[type=search]').focus().select();
    //});
    $doc
	    .on('replace.fndtn.interchange', 'div', function interchangeReplace() {
            $doc.foundation();
        })
	    .on('click', '.click-toggle', function () {
			var showHide, $spans;
			$this = $(this);
			showHide = ($spans = $this.find('span')).filter('.toggle-hide-show').text();
			$spans.first()
				.removeClass('im-toggle-' + (showHide === 'Show' ? 'down' : 'up'))
				.addClass('im-toggle-' + (showHide === 'Show' ? 'up' : 'down'))
				.next().text(showHide === 'Show' ? 'Hide' : 'Show');
			$('#toggle-' + $this.data('toggle')).toggleClass('hide', showHide === 'Hide');
		});

    /**
     * Various Event Handler Hookups
     */
    $.smartresize && $(W).smartresize(
	    function () {
		    var maps = $('.map-container');
		    // Resize the map to fit the screen, or max 600px.
		    maps.width(Math.min(maps.parent().width(), 600) + 'px')
			    .each(function () {
					google.maps.event.trigger(this, 'resize');
				});
        }
    );

    $(function docReady() {
        $('nav').find('li.active').parents('li').addClass('active');
        //var jquiRegex = /(?:^|\s)jqui-(\w+)(?:\s|$)/g;
        /**
         * Provided two rules, jQueryUI widgets can build themselves:
         * 1. Elements that need a widget receive a class matching "jqui-[widgetName]"
         * 2. Any non-default options the widget should get are JSON-encoded into a "data-jqui-[widgetName]-opts" attribute.
         */
        //$('[class^=jqui-]').each(function () {
        //	$this = $(this);
        //	className = $this.attr('class');
        //	while (widget = jquiRegex.exec(className)) {
        //		$this[widget[1]]($this.data('jqui-' + widget[1] + '-opts'));
        //	}
        //});

        if (W.location.hash) {
            $('a[href=' + W.location.hash + ']').parent().not('.active').children('a').trigger('click');
        }
    });

}(window.jQuery, window, document, window.Foundation, window.Modernizr));
