# CakePHP Application Skeleton

A skeleton for creating applications with [CakePHP](http://cakephp.org) 3.0.

This is an unstable repository and should be treated as sample code only.

----
This was a contract project I began for one of my landlord's other businesses.
The intent was to build a small-scale application that would allow her to post
listings and details about other rental properties.

Rather than slap together a Wix site or something in WordPress, I took it as an
opportunity to start learning MVC principles in a PHP framework, as previous 
work history had not exposed me to any of it. This project began around the same
time I began work at PCR, Inc. and it quickly became too much for me to learn
Zend Framework 1 (yes, really) *and* CakePHP at the same time. Given how vastly
different the frameworks are, and the fact my employment now hinged on learning
ZF1, I discussed this project with my landlord and it was abandoned.

The code I authored here was just my first attempts at learning MVC, and some
will seem too clever for its own good. But I believe it demonstrates my 
recognition for abstraction patterns and basic comprehension that would continue
to evolve in my work at PCR.
