<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/28/2014
 * Time: 3:24 PM
 */

namespace App\Controller;

class DocsController extends AppController {
    public function index() {
        //$title = "Kodiak's Policies &amp; Other Documents";
        //
        //$this->set(compact('title'));

        // TODO: Build an actual menu page for Documents
        $this->redirect(['action' => 'website_policy']);
    }

    public function pets_policy() {
        $title    = 'Pet Policy';

        $this->set(compact('title'));
    }

    public function website_policy () {
        $title    = 'Website Use Policy';

        $this->set(compact('title'));
    }
}
