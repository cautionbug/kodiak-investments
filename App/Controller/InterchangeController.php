<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/29/2014
 * Time: 5:53 PM
 */

namespace App\Controller;

use Thook\Cakerov\Utility\Inflector;
//use Cake\Log\LogTrait;

class InterchangeController extends AppController
{
	public $layout = 'ajax';

	public function element($breakPoint, $page, $model, $id = NULL) {
		$Model = ucfirst($model);
		$this->loadModel($Model);

		$singleModel = Inflector::singularize($model);
		$data        = $id === NULL
				? $this->$Model->find('list')
				: $this->$Model->get($id);
		$partial     = $page . '-' . $singleModel;

		$this->set(compact('breakPoint', 'partial', 'data'));
	}

	public function cell() {
	}
}
