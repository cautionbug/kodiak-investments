<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\View\Exception\MissingViewException;
use Cake\Network\Exception\NotFoundException;
use Thook\Cakerov\Utility\Inflector;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{
    /**
     * /about Page
     *
     * It's really about the Parks, so load that Model & render the page.
     *
     * @todo Make the general "About Us" content data driven or something so i don't have to edit it all the time.
     */
    public function about()
    {
        $this->loadModel('Parks');
        $slickLib = Configure::read('CDN.slick');

        $this->_pageJS  = [
            'libs/jquery/gmap3/gmap3.min',
            $slickLib['js'],
            'kodiak.maps',
            'about',
        ];
        $this->_pageCSS = [
            $slickLib['css']
        ];

        $parks = $this->Parks
            ->find('all',
                   [
                       'conditions' => ['active' => TRUE]
                   ])
            ->order(['name' => 'ASC']);
        $title = 'About Kodiak Investments, LLC';

        $this->set(compact('parks', 'title'));
    }

    public function index()
    {

    }

    /**
     * Displays a view
     *
     * @param mixed : What page to display
     *
     * @return void|mixed
     *
     * @throws MissingViewException in debug mode.
     * @throws NotFoundException When the view file could not be found
     */
    public function display()
    {
        $this->layout = 'home';

        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = $title_for_layout = NULL;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        if (!empty($path[$count - 1])) {
            $title_for_layout = Inflector::humanize($path[$count - 1]);
        }
        $this->set(compact('page', 'subpage', 'title_for_layout'));

        try {
            return $this->render(implode('/', $path));
        }
        catch (MissingViewException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }
}
