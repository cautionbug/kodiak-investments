<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\Controller\Controller;
//use Thook\Cakerov\Controller\Controller;
use Cake\Log\Log;
//use Cake\Routing\Router;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    public $layout = 'common';
    /** @type array */
    public $components = [
        'Csrf' => [
            'cookieName' => 'kodiakCsrf'
        ],
        'Flash',
        'RequestHandler',
    ];
    /** @type array */
    public $helpers = [
        'Form' => ['className' => 'Thook/Cakerov.Form'],
        'Html' => ['className' => 'Thook/Cakerov.Html'],
        'Session',
        'Text',
    ];

    /** @type array */
    protected $_pageCSS = [];
    /** @type array */
    protected $_headJS = [];
    /** @type array */
    protected $_pageJS = [];
    /** @type array */
    protected $_requiredFields = [];
    /** @type string */
    protected $_routePrefix = '';

    /**
     * Can't be overridden - merged into protected version & tested if either is NOT empty.
     * No need to set properties declared here as required - they always exist!
     *
     * @type array
     */
    private $__requiredFields = [];

    public function beforeFilter(Event $event)
    {
        // Test for required Controller class fields (generally, for correct assembly of views)
        if (!(empty($this->_requiredFields) && empty($this->__requiredFields))) {
            foreach (array_merge($this->_requiredFields, $this->__requiredFields) as $required) {
                if (!(isset($this->$required) || $this->$required === NULL)) {
                    throw new Exception('Inheritance chain for class ' . get_class($this) . ' failed to define required field: ' . $required);
                }
            }
        }

        parent::beforeFilter($event);
    }

    public function beforeRender(Event $event)
    {
        $this->_setPageCssJs();
        $this->set('routeStr', $this->request->here);
        parent::beforeRender($event);
    }

    protected function _setPageCssJs()
    {
        $this->set('pageCSS', $this->_pageCSS);
        $this->set('pageJS', $this->_pageJS);
        $this->set('headJS', $this->_headJS);
    }
}
