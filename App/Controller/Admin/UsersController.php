<?php
namespace App\Controller\Admin;

use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Log\Log;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable               $Users
 * @property \App\Model\Entity\User                    $User
 * @property \Cake\Controller\Component\FlashComponent $Flash
 */
class UsersController extends AbstractAdminController
{
    protected $_virtualColumns = [
        'full_name' => [
            TRUE => 'first_name',
            'last_name'
        ]
    ];

    private $__omitIndexColumns = [
        'password',
        'modified',
        'first_name',
        'last_name',
    ];
    private $__title            = 'Manage Users';

    // REQUEST CALLBACKS

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);

		// Get the table's columns, to be manipulated based on the action.
		$this->_displayColumns = $this->Users->schema()->columns();
	}

	public function beforeRender(Event $event)
	{
		$this->set('title', $this->__title);

		// Don't forget the parent! (before or after local logic?)
		parent::beforeRender($event);
	}

    // AUTH
    // Users is only accessible to GOD_MODE, so there's no need for it here. The parent will handle it.

    // ACTIONS

	/**
     * Index method
     *
     * @return void
     */
    public function index()
    {
	    // We only need a few of the columns for the index.
	    $this->_diffDisplayColumns($this->__omitIndexColumns);
	    $this->_replaceVirtualColumns();
	    $this->set('columns', $this->_displayColumns);
	    $this->set('entities', $this->paginate($this->Users));
    }

    /**
     * Add method
     *
     * @return void
     */
    public function add()
    {
        $user = $this->Users->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->Users->save($user)) {
                $this->Flash->success('The user has been saved.');

                return $this->redirect(['action' => 'index']);
            }
            else {
                $this->Flash->error('The user could not be saved. Please, try again.');
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param null $id
     *
     * @return mixed
     */
    public function edit($id = NULL)
    {
        $user = $this->Users->get($id, ['contain' => []]);
        // Removes password from entity for Form data
        unset($user->password);
        if ($this->request->is(['patch', 'post', 'put'])) {
            // Skips updating password if a new one wasn't set
            if (empty($this->request->data['password'])) {
                unset($this->request->data['password']);
            }
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success('"' . h($user->full_name) . '" has been saved.', [
		                'key' => 'users',
                        'params' => [
	                        'class' => 'success',
                            'head' => 'Success'
                        ]
	                ]
                );

                return $this->redirect(['action' => 'index']);
            }
            else {
                $this->Flash->error('"' . h($user->full_name) . '" could not be saved. Please, try again.', [
		                'key' => 'users',
                        'params' => [
	                        'class' => 'alert',
	                        'head'  => 'Error'
                        ]
	                ]
                );
            }
        }
        $this->set(compact('user'));
    }

    public function login()
    {
        $this->__title = 'Login to Admin Site';
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if ($user['active']) {
                    $this->Auth->setUser($user);
                    $this->Users->save($this->Users->newEntity($user)->set('last_login', new Time()));

                    return $this->redirect($this->Auth->redirectUrl());
                }
                else {
                    Log::debug('Inactive user found:');
                    Log::debug($user);
                    $this->Flash->set(
                        __('You are not an active user. Sorry, permission denied.'),
                        [
                            'element' => 'general',
                            'params'  => [
                                'class' => 'alert',
                                'head'  => 'Error'
                            ]
                        ]
                    );
                }
            }
            else {
                Log::debug('No user found:');
                Log::debug($user);
                $this->Flash->set(
                    __('Invalid username or password. Try again.'),
                    [
                        'element' => 'general',
                        'params'  => [
                            'class' => 'alert',
                            'head'  => 'Error'
                        ]
                    ]
                );
            }
        }
        $this->set('hideMenu', TRUE);
    }

    public function logout()
    {
        $this->set('hideMenu', TRUE);

        return $this->redirect($this->Auth->logout());
    }

    public function active($id = NULL, $active = NULL)
	{
		if ($this->request->is('post')) {
			$this->_active($id, $active, $this->Users);
			$this->_diffDisplayColumns($this->__omitIndexColumns);
			$this->_replaceVirtualColumns();
			$this->set('columns', $this->_displayColumns);
		}
	}
}
