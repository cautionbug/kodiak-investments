<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/1/2014
 * Time: 10:19 PM
 */

namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;
use Cake\Event\Event;
//use Cake\Log\Log;

abstract class AbstractAdminController extends AppController
{
    const GOD_MODE = 'IDDQD';
    const ADMIN    = 'Admin';
    const EDITOR   = 'Editor';

    public static $ROLES = [
        self::GOD_MODE,
        self::ADMIN,
        self::EDITOR
    ];

    /** @type string */
    public $layout     = 'Admin/common';

    /** @type array */
    protected $_adminCSS       = [];
    /** @type array */
    protected $_adminJS        = [];
    /** @type array */
    protected $_displayColumns = [];
    /** @type array */
    protected $_messages       = [];
    /** @type array */
    protected $_virtualColumns = [
        'created_fmt' => 'created',
        'modified_fmt' => 'modified',
    ];
    /** @type string */
    protected $_routePrefix = '/admin';

    // Nothing gets to override these.
    /** @type array */
    private $__adminCSS       = ['admin'];
    /** @type array */
    private $__adminJS        = ['admin'];
    /** @type array */
    private $__pageCSS        = [];
    /** @type array */
    private $__pageJS         = [];
    /** @type array */
    private $__headJS         = [];
    /**
     * Top keys should belong to specific actions, value structure matches what the action needs
     *
     * @type array
     */
    private $__messages = [
        'active' => [
            'activate'   => [
                TRUE  => '',
                FALSE => 'Failed to activate record. Sorry about that.'
            ],
            'deactivate' => [
                TRUE  => '',
                FALSE => 'Failed to deactivate record. Sorry about that.'
            ]
        ]
    ];
    /**
     * Send these to _diffDisplayColumns to remove them from the $columns view array (typically used to loop a table)
     *
     * @type array
     */
    private $__omitIndexColumns = [
        'id',
    ];
    /**
     * This set of columns will always be swapped. No need to specify them in child classes.
     * @type array
     */
    private $__virtualColumns = [
        'status'       => 'active',
        //'created_fmt'  => 'created',
        //'modified_fmt' => 'modified',
    ];

    // Anything that needs to modify an inherited property should be done here instead of repeating the property definition.
    public function initialize()
    {
        parent::initialize();
        $this->components += [
            'Auth' => [
                'loginAction'    => ['_name' => 'admin-login'],
                'loginRedirect'  => ['_name' => 'admin-index'],
                'logoutRedirect' => ['_name' => 'admin-login'],
                'authorize'      => ['Controller']
            ]
        ];
    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        ConnectionManager::alias('admin', 'default');
        // Authorization checking
        // Deny access to ALL /admin activities without Authentication.
        $this->Auth->deny();

        // Then
        $this->_messages = array_replace_recursive($this->__messages, $this->_messages);
    }

    public function beforeRender(Event $event)
    {
        $this->set('navMenu', 'admin');
        /*
         * Because this method is called for ALL Admin views, none should set their own pageCSS or pageJS.
         * 1. It will lose the core Admin stuff unless going through the hassle of manually adding it, which is pointless
         * 2. If a particular action sets class-level pageCSS/JS they'll end up duplicated on the page because of these merges.
         */
        $this->_adminCSS = array_merge($this->__adminCSS, $this->_adminCSS);
        $this->_adminJS  = array_merge($this->__adminJS, $this->_adminJS);
        $this->_setAdminCssJs();
        $this->_pageCSS = array_merge($this->__pageCSS, $this->_pageCSS);
        $this->_pageJS  = array_merge($this->__pageJS, $this->_pageJS);
        $this->_headJS  = array_merge($this->__headJS, $this->_headJS);
        $this->_setPageCssJs();

        parent::beforeRender($event);
    }

    /**
     * The most basic authorization rule: IDDQD is TRUE, always.
     * Inheriting Controllers should call parent::isAuthorized FIRST to let this rule win:
     * if (!($authorized = parent::isAuthorized($user))) { ... }
     * No need to check the child authorization if user is IDDQD.
     *
     * @param $user
     *
     * @return bool
     */
    public function isAuthorized($user)
    {
        $authorized = false;
        if (isset($user['role'])) {
            $authorized = ($user['role'] === self::GOD_MODE);
        }

        return $authorized;
    }

    /**
     * Handles toggling records `active` field 1|0
     * Based on Controller(?), can get Table(?) and therefore the record ($id) to toggle.
     *
     * @todo This should be moved to an Entity Trait
     *
     * @param int                      $id
     * @param int|bool                 $active
     * @param \Thook\Cakerov\ORM\Table $table
     *
     * @return \Thook\Cakerov\ORM\Entity|false
     */
    protected function _active($id, $active, $table)
    {
        /** @type \Thook\Cakerov\ORM\Entity $entity */
        $entity = $table->get($id);
        $entityName = empty($entity->name) ? $entity->entityName() : ' "' . $entity->name . '"';
        $entity->active = $active;
        //$this->set('tableClass', get_class($table));
        //$this->set('entityClass', get_class($entity));

        $response = [
            'html' => [
                'id'      => ($active ? 'activate-' : 'deactivate-') . $id,
                'class'   => 'alert',
                'header'  => $entityName . ($active ? ' Activation' : ' Deactivation'),
                'message' => 'Nothing happened.'
            ],
            'json' => ['pass' => FALSE],
        ];

        if ($table->save($entity)) {
            $response['json']['pass']    = TRUE;
            $response['html']['class']   = 'success';
            $response['html']['message'] = $entityName . (
                $active
                    ? ' successfully activated. ' . $this->_messages['active']['activate'][TRUE]
                    : ' successfully deactivated. ' . $this->_messages['active']['deactivate'][TRUE]);
        }
        else {
            $response['html']['class']   = 'warning';
            $response['html']['message'] = $active
                ? $this->_messages['active']['activate'][FALSE]
                : $this->_messages['active']['deactivate'][FALSE];
        }
        $this->set(compact('entity', 'response'));

        //return false;
    }

    protected function _diffDisplayColumns(array $omits = [])
    {
        $this->_displayColumns = array_diff($this->_displayColumns, array_merge($omits, $this->__omitIndexColumns));
    }

    protected function _replaceVirtualColumns(array $virtualColumns = [])
    {
        $idx = FALSE;
        // Allow protected _virtualColumns to override private __virtualColumns
        foreach (array_merge($this->__virtualColumns, $this->_virtualColumns, $virtualColumns)
                 as $virtualCol => $dataCol
        ) {
            // An array allows to replace several display columns using 1 virtual column (e.g. full_name => [f_name, l_name])
            if (is_array($dataCol)) {
                // Where the key ($flag) === TRUE, that is the displayColumn to replace with the virtual.
                // Otherwise, we just remove the display column if it exists.
                foreach ($dataCol as $flag => $dCol) {
                    if ($flag === TRUE) {
                        $idx = array_search($dCol, $this->_displayColumns);
                    }
                    else if (($i = array_search($dCol, $this->_displayColumns)) !== FALSE) {
                        unset($this->_displayColumns[$i]);
                    }
                }
            }
            else {
                $idx = array_search($dataCol, $this->_displayColumns);
            }
            if ($idx !== FALSE) {
                $this->_displayColumns[$idx] = $virtualCol;
            }
        }
    }

    protected function _setAdminCssJs()
    {
        empty($this->_adminCSS) || $this->set('adminCSS', $this->_adminCSS);
        empty($this->_adminJS) || $this->set('adminJS', $this->_adminJS);
    }
}
