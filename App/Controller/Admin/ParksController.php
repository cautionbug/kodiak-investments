<?php
namespace App\Controller\Admin;

use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\Network\Exception\NotFoundException;
//use Cake\Log\Log;

/**
 * Parks Controller
 *
 * @property \App\Model\Table\ParksTable               $Parks
 * @property \App\Model\Entity\Park                    $Park
 * @property \Cake\Controller\Component\FlashComponent $Flash
 */
class ParksController extends AbstractAdminController
{
    protected $_messages = [
        'active' => [
            'activate' => [
                TRUE => 'It will now appear on the "About Us" page, and listings in the park will be searchable.'
            ],
            'deactivate' => [
                TRUE => 'It will <em>NOT</em> appear on the "About Us" page, and listings in the park will <em>NOT</em> be searchable.'
            ]
        ]
    ];
    protected $_adminJS = [
        'parks'
    ];

    /**
     * Send these to _diffDisplayColumns to remove them from the $columns view array (typically used to loop a table)
     *
     * @type array
     */
    private $__omitIndexColumns = [
        'county',
        'file_name_no_ext',
        'info_body',
        'lat',
        'lng',
        'street',
        'zip',
    ];
    private $__title = 'Manage Parks';

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Get the table's columns, to be manipulated based on the action.
        $this->_displayColumns = $this->Parks->schema()->columns();
    }

    public function beforeRender(Event $event)
    {
        $this->set('title', $this->__title);
        $this->set('useReveal', TRUE);

        // Don't forget the parent! (before or after local logic?)
        parent::beforeRender($event);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        // We only need a few of the columns for the index.
        $this->_diffDisplayColumns($this->__omitIndexColumns);
        $this->_replaceVirtualColumns();
        $this->set('columns', $this->_displayColumns);
        $this->set('entities', $this->paginate($this->Parks));
    }

    /**
     * @return \Cake\Network\Response|void
     */
    public function add()
    {
        $this->_pageJS[] = Configure::read('CDN.ckeditor');
        $park            = $this->Parks->newEntity($this->request->data);
        if ($this->request->is('post')) {
            try {
                $this->_handleUploads($park);
                if ($this->Parks->save($park)) {
                    $this->Flash->success(
                        'The park has been saved.',
                        [
                            'element' => 'general',
                            'params'  => [
                                'class' => 'success',
                                'head'  => 'Success'
                            ]
                        ]
                    );

                    return $this->redirect(['action' => 'index']);
                }
                else {
                    throw new Exception('The park could not be saved. Please, try again.');
                }
            }
            catch (Exception $ex) {
                $this->Flash->error(
                    $ex->getMessage(),
                    [
                        'element' => 'general',
                        'params'  => [
                            'class' => 'alert',
                            'head'  => 'Error'
                        ]
                    ]
                );
            }
        }
        //$photos = $this->Parks->Photos->find('list');
        //$traits = $this->Parks->ItemTraits->find('list');
        $this->set('entity', $park);
        //$this->set(compact('park', 'photos', 'traits'));
    }

    /***
     * @param int|null $id
     *
     * @return \Cake\Network\Response|void
     */
    public function edit($id = NULL)
    {
        $oldPark = $this->Parks->get($id);

        if ($this->request->is(['patch', 'post', 'put'])) {
            try {
                $newPark = clone $oldPark;
                $this->Parks->patchEntity($newPark, $this->request->data);

                // If the Park's name changed, the file_name probably also changed.
                // Rename the old files. If new ones were uploaded, they're handled below.
                if ($oldPark->file_name_no_ext !== $newPark->file_name_no_ext) {
                    $this->Parks->renameFiles($oldPark->file_name_no_ext, $newPark->file_name_no_ext);
                }

                // Handle new files. If the Park's name changed, the files were renamed above, so should match the new name.
                $this->_handleUploads($newPark);

                if ($this->Parks->save($newPark)) {
                    // TODO: Set up response the same as Contacts
                    $this->Flash->success(
                        '"' . h($oldPark->name) . '" has been saved.',
                        [
                            'element' => 'general',
                            'params'  => [
                                'class' => 'success',
                                'head'  => 'Success'
                            ]
                        ]
                    );

                    return $this->redirect(['action' => 'index']);
                }
                else {
                    throw new Exception('"' . h($oldPark->name) . '" data could not be saved. Please, try again.');
                }
            }
            catch (Exception $ex) {
                $this->Flash->error($ex->getMessage(),
                                    [
                                        'element' => 'general',
                                        'params'  => [
                                            'class' => 'alert',
                                            'head'  => 'Error'
                                        ]
                                    ]
                );
            }
        }
        else {
            $this->_pageJS[] = Configure::read('CDN.ckeditor');
        }
        //$photos = $this->Parks->Photos->find('list');
        //$traits = $this->Parks->ItemTraits->find('list');
        $this->set('entity', $oldPark);
        //$this->set(compact('park', 'photos', 'traits'));
    }

    /**
     * @param $id
     * @param $active
     */
    public function active($id = NULL, $active = NULL)
    {
        if ($this->request->is('post')) {
            $this->_active($id, $active, $this->Parks);
            $this->_diffDisplayColumns($this->__omitIndexColumns);
            $this->_replaceVirtualColumns();
            $this->set('columns', $this->_displayColumns);
        }
    }

    /**
     * @param $newPark
     */
    private function _handleUploads($newPark)
    {
        if (!empty($this->request->data['park_pdf']) && $this->request->data['park_pdf']['error'] === UPLOAD_ERR_OK) {
            if (TRUE === $this->Parks->deleteFiles($newPark->file_name_no_ext, 'files')) {
                if (FALSE === $this->Parks->saveFiles($newPark->file_name_no_ext, $this->request->data['park_pdf'])) {
                    throw new Exception('Uploaded PDF for "' . h($newPark->name) . '" was not saved. Please, try again.');
                }
            }
            else {
                throw new Exception('Original PDF for "' . h($newPark->name) . '" could not be deleted. Please, try again.');
            }
        }
        if (!empty($this->request->data['park_img']) && $this->request->data['park_img']['error'] === UPLOAD_ERR_OK) {
            if (TRUE === $this->Parks->deleteFiles($newPark->file_name_no_ext, 'img')) {
                if (FALSE === $this->Parks->saveFiles($newPark->file_name_no_ext, $this->request->data['park_img'], TRUE)) {
                    throw new Exception('Uploaded Image for "' . h($newPark->name) . '" was not saved. Please, try again.');
                }
            }
            else {
                throw new Exception('Original Image for "' . h($newPark->name) . '" could not be deleted. Please, try again.');
            }
        }
    }

    /**
     * View method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    //public function view($id = NULL)
    //{
    //    $park = $this->Parks->get(
    //        $id,
    //        [
    //            'contain' => ['Listings']
    //            //'contain' => ['Photos', 'ItemTraits', 'Listings']
    //        ]
    //    );
    //    $this->set('park', $park);
    //}

    /**
     * Delete method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    //public function delete($id = NULL)
    //{
    //    $park = $this->Parks->get($id);
    //    $this->request->allowMethod('post', 'delete');
    //    if ($this->Parks->delete($park)) {
    //        $this->Flash->success('The park has been deleted.');
    //    }
    //    else {
    //        $this->Flash->error('The park could not be deleted. Please, try again.');
    //    }
    //
    //    return $this->redirect(['action' => 'index']);
    //}
}
