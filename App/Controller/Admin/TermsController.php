<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;

/**
 * Terms Controller
 *
 * @property \App\Model\Table\TermsTable $Terms
 */
class TermsController extends AbstractAdminController
{
    /**
     * Send these to _diffDisplayColumns to remove them from the $columns view array (typically used to loop a table)
     *
     * @type array
     */
    private $__omitIndexColumns = [
        'fine_print',
    ];
    private $__title            = 'Terms List';

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Get the table's columns, to be manipulated based on the action.
        $this->_displayColumns = $this->Terms->schema()->columns();
    }

    public function beforeRender(Event $event)
    {
        $this->set('title', $this->__title);

        // Don't forget the parent! (before or after local logic?)
        parent::beforeRender($event);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->_diffDisplayColumns($this->__omitIndexColumns);
        $this->_replaceVirtualColumns();
        $this->set('columns', $this->_displayColumns);
        $this->set('entities', $this->paginate($this->Terms));
    }

    /**
     * Add method
     *
     * @return void
     */
    public function add()
    {
        $this->_pageJS[] = '//cdn.ckeditor.com/4.4.4/standard/ckeditor.js';
        $term = $this->Terms->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->Terms->save($term)) {
                $this->Flash->success('The item has been saved.',
                                      [
                                          'element' => 'general',
                                          'params'  => [
                                              'class' => 'success',
                                              'head'  => 'Success'
                                          ]
                                      ]
                );

                return $this->redirect(['action' => 'index']);
            }
            else {
                $this->Flash->error('The item could not be saved. Please, try again.',
                                    [
                                        'element' => 'general',
                                        'params'  => [
                                            'class' => 'alert',
                                            'head'  => 'Error'
                                        ]
                                    ]
                );
            }
        }
        $this->set('entity', $term);
    }

    /**
     * Edit method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    public function edit($id = NULL)
    {
        $term = $this->Terms->get($id, [
                'contain' => []
            ]
        );
        if ($this->request->is(['patch', 'post', 'put'])) {
            $term = $this->Terms->patchEntity($term, $this->request->data);
            if ($this->Terms->save($term)) {
                $this->Flash->success('The item has been saved.',
                                      [
                                          'element' => 'general',
                                          'params'  => [
                                              'class' => 'success',
                                              'head'  => 'Success'
                                          ]
                                      ]
                );

                return $this->redirect(['action' => 'index']);
            }
            else {
                $this->Flash->error('The item could not be saved. Please, try again.',
                                    [
                                        'element' => 'general',
                                        'params'  => [
                                            'class' => 'alert',
                                            'head'  => 'Error'
                                        ]
                                    ]
                );
            }
        }
        else {
            $this->_pageJS[] = '//cdn.ckeditor.com/4.4.4/standard/ckeditor.js';
        }
        $this->set('entity', $term);
    }

    /**
     * @param $id
     * @param $active
     */
    public function active($id = NULL, $active = NULL)
    {
        if ($this->request->is('post')) {
            $this->_active($id, $active, $this->Terms);
            $this->_diffDisplayColumns($this->__omitIndexColumns);
            $this->_replaceVirtualColumns();
            $this->set('columns', $this->_displayColumns);
        }
    }

    /**
     * View method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    //public function view($id = NULL)
    //{
    //    $term = $this->Terms->get(
    //        $id,
    //        [
    //            'contain' => ['Contacts', 'Listings']
    //        ]
    //    );
    //    $this->set('term', $term);
    //}

    /**
     * Delete method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    //public function delete($id = NULL)
    //{
    //    $term = $this->Terms->get($id);
    //    $this->request->allowMethod('post', 'delete');
    //    if ($this->Terms->delete($term)) {
    //        $this->Flash->success('The term has been deleted.');
    //    }
    //    else {
    //        $this->Flash->error('The term could not be deleted. Please, try again.');
    //    }
    //
    //    return $this->redirect(['action' => 'index']);
    //}
}
