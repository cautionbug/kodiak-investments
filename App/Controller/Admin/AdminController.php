<?php
namespace App\Controller\Admin;

use Cake\Event\Event;
//use Cake\Log\Log;

/**
 * Admin Controller
 */
class AdminController extends AbstractAdminController
{
	// REQUEST CALLBACKS

    public function beforeFilter(Event $event)
    {
	    parent::beforeFilter($event);
	    $this->Auth->deny('index');
    }

	public function beforeRender(Event $event)
	{
		parent::beforeRender($event);
	}

	// AUTH


	// ACTIONS

	public function index()
	{
		if (!$this->Auth->isAuthorized()) {
			$this->redirect(['_name' => 'admin-login']);
		}
	}
}
