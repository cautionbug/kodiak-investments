<?php
namespace App\Controller\Admin;

use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\Network\Exception\NotFoundException;

/**
 * Listings Controller
 *
 * @property \App\Model\Table\ListingsTable $Listings
 */
class ListingsController extends AbstractAdminController
{
    protected $_messages = [
        'active' => [
            'activate'   => [
                true => 'It is now available to the public to appear in the Browse section and in search results.'
            ],
            'deactivate' => [
                true => 'It will <em>NOT</em> appear in the Browse section, and will <em>NOT</em> be searchable.'
            ]
        ]
    ];

    /**
     * Send these to _diffDisplayColumns to remove them from the $columns view array (typically used to loop a table)
     *
     * @type array
     */
    private $__omitIndexColumns = [
        'street',
        'city',
        'county',
        'state',
        'zip',
        'lat',
        'lng',
        'area',
        'bedrooms',
        'bathrooms',
        'house_condition_id',
        'park_id',
        'term_id',
        'house_style_id',
        'info_body',
    ];
    private $__title            = 'Manage Listings';

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Get the table's columns, to be manipulated based on the action.
        $this->_displayColumns = $this->Listings->schema()->columns();
    }

    public function beforeRender(Event $event)
    {
        $this->set('title', $this->__title);
        $this->set('useReveal', true);

        // Don't forget the parent! (before or after local logic?)
        parent::beforeRender($event);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        // We only need a few of the columns for the index.
        $this->_diffDisplayColumns($this->__omitIndexColumns);
        $this->_replaceVirtualColumns();
        $this->set('columns', $this->_displayColumns);
        $this->paginate = [
            'contain' => ['HouseConditions', 'Parks', 'Terms', 'HouseStyles']
        ];
        $this->set('entities', $this->paginate($this->Listings));
        $this->set('_serialize', ['listings']);
    }

    /**
     * View method
     *
     * @param string|null $id Listing id.
     *
     * @return void
     *
     * @throws NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $listing = $this->Listings->get($id, [
            'contain' => ['HouseConditions', 'Parks', 'Terms', 'HouseStyles', 'Attributes', 'Photos']
        ]);
        $this->set('entity', $listing);
        $this->set('_serialize', ['listing']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     * @ return \Cake\Network\Response|void
     */
    public function add()
    {
        $this->_headJS[]  = Configure::read('CDN.ckeditor');
        $this->_headJS[]  = '../libs/jqui-notheme/jquery-ui.min';
        $this->_pageCSS[] = '../libs/jqui-notheme/jquery-ui.min';
        $this->_pageCSS[] = '../libs/jqui-notheme/jquery-ui.structure.min';
        $this->_pageCSS[] = 'datepicker';
        // This guy's libraries are externally dependent, and in dev mode the helpers are separate files.
        $this->_pageJS[] = 'libs/jockmac22-calendar/js/date.min';
        if (Configure::read('debug') === true) {
            $this->_pageJS[] = 'libs/jockmac22-calendar/helpers/date-helpers';
            $this->_pageJS[] = 'libs/jockmac22-calendar/helpers/string-helpers';
        }
        // Unfortunately, so much of this script looks like slop, i'm rewriting a bunch of it myself, so i won't use the libs version.
        $this->_pageJS[] = 'foundation_calendar';

        $listing = $this->Listings->newEntity();
        Log::debug($listing->jsonSerialize());
        Log::debug($listing->errors());
        if ($this->request->is('post')) {
            try {
                // TODO (JLT): Remove before production
                Log::debug($this->request->data);
                // This doesn't preserve the Attributes / ListingsAttributes POST data
                //$listing = $this->Listings->patchEntity($listing, $this->request->data);
                // This only patches the Attribute ID, losing the _joinData (value)
                // It also causes validation failures because the POST data doesn't have ALL the Attribute fields
                // It's like it expects to save a new record to Attributes, but i don't want to do that!
                $listing = $this->Listings->patchEntity($listing, $this->request->data, ['associated' => ['Attributes._joinData']]);
                //$listing = $this->Listings->patchEntity($listing, $this->request->data, ['associated' => ['ListingsAttributes']]);
                Log::debug($listing->jsonSerialize());
                Log::debug($listing->errors());

                if (($savedListing = $this->Listings->save($listing)) !== false) {
                    Log::debug('The listing has been saved.');
                    Log::debug($savedListing->jsonSerialize());
                    $this->Flash->success(
                        'The listing has been saved.',
                        [
                            'element' => 'general',
                            'params'  => [
                                'class' => 'success',
                                'head'  => 'Success'
                            ]
                        ]
                    );
                    $listing = $savedListing;
                    $this->redirect(['action' => 'edit', $listing->id]);
                } else {
                    Log::debug($listing->errors());
                    throw new Exception('The listing could not be saved. Please, try again.');
                }
            } catch (Exception $ex) {
                $this->Flash->error(
                    $ex->getMessage(),
                    [
                        'element' => 'general',
                        'params'  => [
                            'class' => 'alert',
                            'head'  => 'Error'
                        ]
                    ]
                );
                Log::debug($ex->getMessage());
            }
        }
        $houseConditions = $this->Listings->HouseConditions->findListByActive(true)->order('name');
        $parks           = $this->Listings->Parks->findListByActive(true)->order('name');
        $terms           = $this->Listings->Terms->findListByActive(true)->order('name');
        $houseStyles     = $this->Listings->HouseStyles->findListByActive(true)->order('name');
        $attributes      = $this->Listings->Attributes->findAllByActive(true)->order('name');
        //$photos = $this->Listings->Photos->findListByActive(true)->order('position');
        //Log::debug($attributes);
        $this->set('entity', $listing);
        $this->set(compact('houseConditions', 'parks', 'terms', 'houseStyles', 'attributes'));
        $this->set('_serialize', ['listing']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Listing id.
     *
     * @return void Redirects on successful edit, renders view otherwise.
     * @ return \Cake\Network\Response|void
     *
     * @throws NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $listing = $this->Listings->get($id, [
            'contain' => ['Attributes', 'Photos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $listing = $this->Listings->patchEntity($listing, $this->request->data);
            if ($this->Listings->save($listing)) {
                $this->Flash->success('The listing has been saved.');

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error('The listing could not be saved. Please, try again.');
            }
        }
        $houseConditions = $this->Listings->HouseConditions->findListByActive(true)->order('name');
        $parks           = $this->Listings->Parks->findListByActive(true)->order('name');
        $terms           = $this->Listings->Terms->findListByActive(true)->order('name');
        $houseStyles     = $this->Listings->HouseStyles->findListByActive(true)->order('name');
        $attributes      = $this->Listings->Attributes->findAllByActive(true);
        //$photos = $this->Listings->Photos->findListByActive(true)->order('position');

        //foreach ($attributes as &$attr) {
        //    $attr->current = in_array($attr->id, $listing->item_attrs);
        //}

        $this->set('entity', $listing);
        $this->set(compact('listing', 'houseConditions', 'parks', 'terms', 'houseStyles', 'attributes')); //, 'photos'));
        $this->set('_serialize', ['listing']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Listing id.
     *
     * @return void Redirects to index.
     * @throws NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $listing = $this->Listings->get($id);
        if ($this->Listings->delete($listing)) {
            $this->Flash->success('The listing has been deleted.');
        } else {
            $this->Flash->error('The listing could not be deleted. Please, try again.');
        }

        return $this->redirect(['action' => 'index']);
    }
}
