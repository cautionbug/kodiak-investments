<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;

/**
 * Photos Controller
 *
 * @property \App\Model\Table\PhotosTable $Photos
 */
class PhotosController extends AbstractAdminController
{
    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['MimeTypes']
        ];
        $this->set('photos', $this->paginate($this->Photos));
    }

    /**
     * View method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    public function view($id = NULL)
    {
        // TODO: Make sure MimeTypes loads from Thook\Websites
        $photo = $this->Photos->get(
            $id,
            [
                'contain' => ['MimeTypes', 'Listings', 'Parks']
            ]
        );
        $this->set('photo', $photo);
    }

    /**
     * Add method
     *
     * @return void
     */
    public function add()
    {
        $photo = $this->Photos->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->Photos->save($photo)) {
                $this->Flash->success('The photo has been saved.');

                return $this->redirect(['action' => 'index']);
            }
            else {
                $this->Flash->error('The photo could not be saved. Please, try again.');
            }
        }
        $mimeTypes = $this->Photos->MimeTypes->find('list');
        $listings  = $this->Photos->Listings->find('list');
        $parks     = $this->Photos->Parks->find('list');
        $this->set(compact('photo', 'mimeTypes', 'listings', 'parks'));
    }

    /**
     * Edit method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    public function edit($id = NULL)
    {
        $photo = $this->Photos->get(
            $id,
            [
                'contain' => ['Listings', 'Parks']
            ]
        );
        if ($this->request->is(['patch', 'post', 'put'])) {
            $photo = $this->Photos->patchEntity($photo, $this->request->data);
            if ($this->Photos->save($photo)) {
                $this->Flash->success('The photo has been saved.');

                return $this->redirect(['action' => 'index']);
            }
            else {
                $this->Flash->error('The photo could not be saved. Please, try again.');
            }
        }
        $mimeTypes = $this->Photos->MimeTypes->find('list');
        $listings  = $this->Photos->Listings->find('list');
        $parks     = $this->Photos->Parks->find('list');
        $this->set(compact('photo', 'mimeTypes', 'listings', 'parks'));
    }

    /**
     * Delete method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    public function delete($id = NULL)
    {
        $photo = $this->Photos->get($id);
        $this->request->allowMethod('post', 'delete');
        if ($this->Photos->delete($photo)) {
            $this->Flash->success('The photo has been deleted.');
        }
        else {
            $this->Flash->error('The photo could not be deleted. Please, try again.');
        }

        return $this->redirect(['action' => 'index']);
    }
}
