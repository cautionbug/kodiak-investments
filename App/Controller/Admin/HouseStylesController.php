<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;

/**
 * HouseStyles Controller
 *
 * @property \App\Model\Table\HouseStylesTable $HouseStyles
 */
class HouseStylesController extends AbstractAdminController
{
    /**
     * Send these to _diffDisplayColumns to remove them from the $columns view array (typically used to loop a table)
     *
     * @type array
     */
    //private $__omitIndexColumns = [];
    private $__title            = 'House Styles List';

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Get the table's columns, to be manipulated based on the action.
        $this->_displayColumns = $this->HouseStyles->schema()->columns();
    }

    public function beforeRender(Event $event)
    {
        $this->set('title', $this->__title);

        // Don't forget the parent! (before or after local logic?)
        parent::beforeRender($event);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->_diffDisplayColumns();
        $this->_replaceVirtualColumns();
        $this->set('columns', $this->_displayColumns);
        $this->set('entities', $this->paginate($this->HouseStyles));
    }

    /**
     * Add method
     *
     * @return void
     */
    public function add()
    {
        $houseStyle = $this->HouseStyles->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->HouseStyles->save($houseStyle)) {
                $this->Flash->success(
                    'The item has been saved.',
                    [
                        'element' => 'general',
                        'params'  => [
                            'class' => 'success',
                            'head'  => 'Success'
                        ]
                    ]
                );

                return $this->redirect(['action' => 'index']);
            }
            else {
                $this->Flash->error(
                    'The item could not be saved. Please, try again.',
                    [
                        'element' => 'general',
                        'params'  => [
                            'class' => 'alert',
                            'head'  => 'Error'
                        ]
                    ]
                );
            }
        }
        $this->set('entity', $houseStyle);
    }

    /**
     * Edit method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    public function edit($id = NULL)
    {
        $houseStyle = $this->HouseStyles->get($id, [
                'contain' => []
            ]
        );
        if ($this->request->is(['patch', 'post', 'put'])) {
            $houseStyle = $this->HouseStyles->patchEntity($houseStyle, $this->request->data);
            if ($this->HouseStyles->save($houseStyle)) {
                $this->Flash->success(
                    'The item has been saved.',
                    [
                        'element' => 'general',
                        'params'  => [
                            'class' => 'success',
                            'head'  => 'Success'
                        ]
                    ]
                );

                return $this->redirect(['action' => 'index']);
            }
            else {
                $this->Flash->error(
                    'The item could not be saved. Please, try again.',
                    [
                        'element' => 'general',
                        'params'  => [
                            'class' => 'alert',
                            'head'  => 'Error'
                        ]
                    ]
                );
            }
        }
        $this->set('entity', $houseStyle);
    }

    /**
     * @param $id
     * @param $active
     */
    public function active($id = NULL, $active = NULL)
    {
        if ($this->request->is('post')) {
            $this->_active($id, $active, $this->HouseStyles);
            $this->_diffDisplayColumns();
            $this->_replaceVirtualColumns();
            $this->set('columns', $this->_displayColumns);
        }
    }

    /**
     * View method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    //public function view($id = NULL)
    //{
    //    $houseStyle = $this->HouseStyles->get(
    //        $id,
    //        [
    //            'contain' => ['Listings']
    //        ]
    //    );
    //    $this->set('houseStyle', $houseStyle);
    //}

    /**
     * Delete method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    //public function delete($id = NULL)
    //{
    //    $houseStyle = $this->HouseStyles->get($id);
    //    $this->request->allowMethod('post', 'delete');
    //    if ($this->HouseStyles->delete($houseStyle)) {
    //        $this->Flash->success('The house style has been deleted.');
    //    }
    //    else {
    //        $this->Flash->error('The house style could not be deleted. Please, try again.');
    //    }
    //
    //    return $this->redirect(['action' => 'index']);
    //}
}
