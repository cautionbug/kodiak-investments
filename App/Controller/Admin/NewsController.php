<?php
namespace App\Controller\Admin;

use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * News Controller
 *
 * @property \App\Model\Table\NewsTable $News
 * @property \Cake\Controller\Component\FlashComponent $Flash
 */
class NewsController extends AbstractAdminController {
	private $__omitIndexColumns = [
		'content'
	];
	private $__title            = 'Manage News Updates';

	public function beforeFilter(Event $event)
	{
		parent::beforeFilter($event);

		// Get the table's columns, to be manipulated based on the action.
		$this->_displayColumns = $this->News->schema()->columns();
	}

	public function beforeRender(Event $event)
	{
		$this->set('title', $this->__title);

		// Don't forget the parent! (before or after local logic?)
		parent::beforeRender($event);
	}

	/**
 * Index method
 *
 * @return void
 */
	public function index() {
		// We only need a few of the columns for the index.
		$this->_diffDisplayColumns($this->__omitIndexColumns);
		$this->_replaceVirtualColumns();
		$this->set('columns', $this->_displayColumns);
		$this->set('entities', $this->paginate($this->News));
	}

	/**
	 * Add method
	 *
	 * @return void
	 */
	public function add() {
		$this->__addCssJs();

		$news = $this->News->newEntity($this->request->data);
		if ($this->request->is('post')) {
			if ($this->News->save($news)) {
				$this->Flash->success('The news update has been saved.',
				                      [
					                      'element' => 'general',
					                      'params'  => [
						                      'class' => 'success',
					                          'head'  => 'Success'
					                      ]
				                      ]
				);
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error('The news update could not be saved. Please, try again.',
				                    [
					                    'element' => 'general',
					                    'params'  => [
						                    'class' => 'alert',
					                        'head'  => 'Error'
					                    ]
				                    ]
				);
			}
		}
		$this->set('entity', $news);
	}

	/**
	 * Edit method
	 *
	 * @param string $id
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException
	 */
	public function edit($id = null) {
		$news = $this->News->get(
			$id, [
				'contain' => []
			]
		);
		$this->__addCssJs();
		if ($this->request->is(['patch', 'post', 'put'])) {
			$news = $this->News->patchEntity($news, $this->request->data);
			if ($this->News->save($news)) {
				$this->Flash->success('The news item has been saved.',
				                      [
					                      'element' => 'general',
					                      'params'  => [
						                      'class' => 'success',
					                          'head'  => 'Success'
					                      ]
				                      ]
				);
				return $this->redirect(['action' => 'index']);
			} else {
				$this->Flash->error('The news item could not be saved. Please, try again.',
				                    [
					                    'element' => 'general',
					                    'params'  => [
						                    'class' => 'alert',
					                        'head'  => 'Error'
					                    ]
				                    ]
				);
			}
		}
		$this->set('entity', $news);
	}

	/**
	 * @param $id
	 * @param $active
	 */
	public function active($id = NULL, $active = NULL)
	{
		if ($this->request->is('post')) {
			$this->_active($id, $active, $this->News);
			$this->_diffDisplayColumns($this->__omitIndexColumns);
			$this->_replaceVirtualColumns();
			$this->set('columns', $this->_displayColumns);
		}
	}

	private function __addCssJs()
	{
		$this->_pageCSS[] = 'datepicker';
		// This guy's libraries are externally dependent, and in dev mode the helpers are separate files.
		$this->_pageJS[] = 'libs/jockmac22-calendar/js/date.min';
		if (Configure::read('debug') === TRUE) {
			$this->_pageJS[] = 'libs/jockmac22-calendar/helpers/date-helpers';
			$this->_pageJS[] = 'libs/jockmac22-calendar/helpers/string-helpers';
		}
		// Unfortunately, so much of this script looks like slop, i'm rewriting a bunch of it myself, so i won't use the libs version.
		$this->_pageJS[] = 'foundation_calendar';
		$this->_pageJS[] = Configure::read('CDN.ckeditor');
	}

	/**
	 * View method
	 *
	 * @param string $id
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException
	 */
	//public function view($id = null) {
	//	$news = $this->News->get($id, [
	//			'contain' => []
	//		]);
	//	$this->set('news', $news);
	//}


	/**
	 * Delete method
	 *
	 * @param string $id
	 * @return void
	 * @throws \Cake\Network\Exception\NotFoundException
	 */
	//public function delete($id = null) {
	//	$news = $this->News->get($id);
	//	$this->request->allowMethod(['post', 'delete']);
	//	if ($this->News->delete($news)) {
	//		$this->Flash->success('The news has been deleted.');
	//	} else {
	//		$this->Flash->error('The news could not be deleted. Please, try again.');
	//	}
	//	return $this->redirect(['action' => 'index']);
	//}
}
