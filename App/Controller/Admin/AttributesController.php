<?php
namespace App\Controller\Admin;

use Cake\Network\Exception\NotFoundException;
use Cake\Event\Event;

/**
 * ItemAttrs Controller
 *
 * @property \App\Model\Table\AttributesTable $ItemAttrs
 */
class AttributesController extends AbstractAdminController
{
    /** @type array */
    protected $_virtualColumns = [
        //'icon' => 'icon_class'
    ];

    /**
     * Send these to _diffDisplayColumns to remove them from the $columns view array (typically used to loop a table)
     *
     * @type array
     */
    private $__omitIndexColumns = [
        'icon_class',
        'requires_value',
    ];
    private $__title = 'Manage Listing Attributes';

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Get the table's columns, to be manipulated based on the action.
        $this->_displayColumns = $this->ItemAttrs->schema()->columns();
    }

    public function beforeRender(Event $event)
    {
        $this->set('title', $this->__title);

        // Don't forget the parent! (before or after local logic?)
        parent::beforeRender($event);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->_diffDisplayColumns($this->__omitIndexColumns);
        $this->_replaceVirtualColumns();
        $this->set('columns', $this->_displayColumns);
        $this->set('entities', $this->paginate($this->ItemAttrs));
    }

    /**
     * @return \Cake\Network\Response|void
     */
    public function add()
    {
        $itemTrait = $this->ItemAttrs->newEntity($this->request->data);
        if ($this->request->is('post')) {
            if ($this->ItemAttrs->save($itemTrait)) {
                $this->Flash->success('The item has been saved.',
                                      [
                                          'element' => 'general',
                                          'params'  => [
                                              'class' => 'success',
                                              'head'  => 'Success'
                                          ]
                                      ]
                );

                return $this->redirect(['action' => 'index']);
            }
            else {
                $this->Flash->error('The item could not be saved. Please, try again.',
                                    [
                                        'element' => 'general',
                                        'params'  => [
                                            'class' => 'alert',
                                            'head'  => 'Error'
                                        ]
                                    ]
                );
            }
        }
        $this->set('entity', $itemTrait);
    }

    /**
     * @param int|null $id
     *
     * @return \Cake\Network\Response|void
     */
    public function edit($id = NULL)
    {
        $itemAttr = $this->ItemAttrs->get($id, [
                'contain' => []
            ]
        );
        if ($this->request->is(['patch', 'post', 'put'])) {
            $itemAttr = $this->ItemAttrs->patchEntity($itemAttr, $this->request->data);
            if ($this->ItemAttrs->save($itemAttr)) {
                $this->Flash->success('The item has been saved.',
                                      [
                                          'element' => 'general',
                                          'params'  => [
                                              'class' => 'success',
                                              'head'  => 'Success'
                                          ]
                                      ]
                );

                return $this->redirect(['action' => 'index']);
            }
            else {
                $this->Flash->error('The item could not be saved. Please, try again.',
                                    [
                                        'element' => 'general',
                                        'params'  => [
                                            'class' => 'alert',
                                            'head'  => 'Error'
                                        ]
                                    ]
                );
            }
        }
        $this->set('entity', $itemAttr);
    }

    /**
     * @param $id
     * @param $active
     */
    public function active($id = NULL, $active = NULL)
    {
        if ($this->request->is('post')) {
            $this->_active($id, $active, $this->ItemAttrs);
            $this->_diffDisplayColumns($this->__omitIndexColumns);
            $this->_replaceVirtualColumns();
            $this->set('columns', $this->_displayColumns);
        }
    }

    /**
     * View method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    //public function view($id = NULL)
    //{
    //    $itemTrait = $this->ItemTraits->get(
    //        $id,
    //        [
    //            'contain' => []
    //        ]
    //    );
    //    $this->set('entity', $itemTrait);
    //}

    /**
     * Delete method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    //public function delete($id = NULL)
    //{
    //    $itemTrait = $this->ItemTraits->get($id);
    //    $this->request->allowMethod('post', 'delete');
    //    if ($this->ItemTraits->delete($itemTrait)) {
    //        $this->Flash->success('The item trait has been deleted.');
    //    }
    //    else {
    //        $this->Flash->error('The item trait could not be deleted. Please, try again.');
    //    }
    //
    //    return $this->redirect(['action' => 'index']);
    //}
}
