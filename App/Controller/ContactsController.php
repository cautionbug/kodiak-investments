<?php
namespace App\Controller;

// Models
use App\Model\Entity\Contact;
use App\Model\Table\ContactsTable;
use App\Model\Table\TermsTable;
use Cake\Core\Configure;
use Cake\Core\Exception\Exception;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\Network\Email\Email;
use Cake\Network\Exception\NotFoundException;
use Cake\Routing\Router;
use Thook\Websites\Model\Table\CaptchasTable;
//use Cake\Log\Log;

/**
 * Contacts Controller
 *
 * @property CaptchasTable $Captchas
 * @property ContactsTable $Contacts
 * @property TermsTable    $Terms
 * @property Contact       $Contact
 */
class ContactsController extends AppController
{
    private $__directContact = <<<'DIRECT'
<address class="column text-center">Kodiak Investments / Universal Group
<br>1515 Michigan St.
<br>Grand Rapids, MI 49503
<br><a href="tel:+16164584949">616.458.4949</a>
</address>
DIRECT;

    public function beforeFilter(Event $event)
    {
        //array_push($this->_pageCSS,
        //           '../libs/jqueryui/jquery-ui.min'
        //);
        //array_push($this->_pageJS,
        //           'parsley-validators',
        //           'libs/parsley/parsley.remote (2)',
        //           'libs/parsley/parsley (2)'
        //           //'../libs/jqueryui/jquery-ui.min',
        //);
    }

    public function beforeRender(Event $event)
    {
        // Make sure this is the LAST JS added to the array (after any actions add theirs).
        array_push(
            $this->_pageJS,
            'parsley-validators',
            'libs/parsley/parsley.remote',
            'libs/parsley/parsley',
            'contact'
        );
        //$this->_pageJS[] = 'contact';
        parent::beforeRender($event);
    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $title = 'Contact Us';
        $this->loadModel('Thook/Websites.Captchas');

        $terms   = $this->Contacts->Terms->find('list');
        $captcha = $this->Captchas->find('random')->first();
        $contact = $this->Contacts->newEntity($this->request->data);

        $this->_pageCSS[] = 'datepicker';
        // This guy's libraries are externally dependent, and in dev mode the helpers are separate files.
        $this->_pageJS[] = 'libs/jockmac22-calendar/js/date.min';
        if (Configure::read('debug') === TRUE) {
            $this->_pageJS[] = 'libs/jockmac22-calendar/helpers/date-helpers';
            $this->_pageJS[] = 'libs/jockmac22-calendar/helpers/string-helpers';
        }
        // Unfortunately, so much of this script looks like slop, i'm rewriting a bunch of it myself, so i won't use the libs version.
        $this->_pageJS[] = 'foundation_calendar';

        $this->set(compact('captcha', 'contact', 'terms', 'title'));
    }

    /**
     * Remove method
     * Contacts receive a link in their email that allows them to remove themselves from the Table.
     *
     * @param $id
     * @param $hash
     */
    public function remove($id, $hash)
    {
        //list($id, $hash) = explode('/', $id_hash);
        $class = $content = $header = '';

        try {
            $contact = $this->Contacts->get(
                //$id,
                $id,
                [
                    'conditions' => [
                        'remove_hash =' => $hash
                    ]
                ]
            );

            if ($contact) {
                $this->Contacts->delete($contact);
                // Output confirmation message.
                $header  = 'Thank you';
                $class   = 'success';
                $content = [
                    'Your contact info has been removed from our system.',
                    'We strive to respect your privacy, and never release contact info to third parties. ' .
                    'If you received an unsolicited email from us, we apologize for any inconvenience.'
                ];
            }
            else {
                // Output not-found.
                $header  = 'We didn\'t find your address';
                $class   = 'warning';
                $content =
                    'If you\'re certain an unsolicited email came to you from us, please contact our office so we can address the issue immediately: ' .
                    $this->__directContact;
            }
        }
        catch (Exception $ex) {
            $header  = 'We didn\'t find your address';
            $class   = 'alert';
            $content =
                'If you\'re certain an unsolicited email came to you from us, please contact our office so we can address the issue immediately: ' .
                $this->__directContact;
        }
        finally {
            $this->set(compact('class', 'content', 'header'));
        }
    }

    /**
     * Submit method
     * Form submission handler
     */
    public function submit()
    {
        if ($this->request->is('post')) {
            unset ($this->request->data['captcha']);
            $contact = $this->Contacts->newEntity($this->request->data);
            $contact->set('remove_hash');
            $contact->set('ip_address', $this->request->clientIp());
            // If save fails, this will be the error output.
            // TODO: Build up internal error handling so i'm aware of a problem.
            $json    = ['pass' => FALSE];
            $class   = 'alert';
            $header  = 'Oops! This is embarrassing...';
            $message = 'There was a problem processing your request. ' .
                       ($directContact =
                           'We are working on it, but in the meantime we recommend you try contacting us directly: ' . $this->__directContact);

            $success = $this->Contacts->save($contact);
            if ($success) {
                $json['pass'] = TRUE;
                $header       = 'Thank You!';
                try {
                    $emailKodiak  = new Email('test'); // Replace profile with 'to_kodiak'
                    $emailContact = new Email('noreply');
                    // TODO: Set up email templates using Zurb Ink
                    $kodiakMessage  = $this->_buildEmailBody($contact, TRUE);
                    $contactMessage = $this->_buildEmailBody($contact);
//Log::debug($kodiakMessage);
//Log::debug($contactMessage);

                    $emailKodiak
                        ->transport('default')
                        ->sender($contact->email, $contact->name)
                        ->from($contact->email, $contact->name)
                        ->replyTo($contact->email, $contact->name)
                        ->subject($emailKodiak->subject() . 'New Website Contact information');
                    $emailContact
                        ->transport('default')
                        ->to($contact->email, $contact->name)
                        ->subject('Thank you for contacting Kodiak Investments ' . $emailContact->subject());
                    // Send Mail to Kodiak
                    if ($emailKodiak->send($kodiakMessage)) {
                        // Send Mail to Contact
                        if ($emailContact->send($contactMessage)) {
                            // $json['pass'] is already TRUE
                            $class   = 'success';
                            $message = 'We have received your inquiry and will respond as soon as we can. ' .
                                       'For your reference, we\'ve sent an email with the info you submitted to: &lt;' . h($contact->email) . '&gt;';
                        }
                        else {
                            throw new Exception('Failed trying to send email to Contact.');
                        }
                    }
                    else {
                        throw new Exception('Failed trying to send email to Kodiak.');
                    }
                }
                catch (Exception $ex) {
                    $class  = 'warning';
                    $header = 'Almost made it!';
                    $message = 'We were able to save your submission, but a problem occurred when sending an email notification.<br>' .
                               $ex->getMessage() .
                               $directContact; //.
                }
            }
            $response = [
                // This tells Common/responder.ctp what response to render & what method (element, cell, etc.)
                //	'render' => [
                //		// Default is (currently) reveal.
                //			'element' => 'reveal',
                //	],
                'html' => [
                    'id'      => 'contact-submit',
                    'class'   => $class,
                    'header'  => $header,
                    'message' => __($message),
                ],
                'json' => $json
            ];

            $this->set(compact('response', 'email'));
        }
    }

    /**
     * @todo: Replace this with a view using Zurb Ink
     *
     * @param \Cake\ORM\Entity $contact
     * @param bool    $internal
     *
     * @return string
     */
    private function _buildEmailBody($contact, $internal = FALSE)
    {
        $body = [
            'This is the information that was submitted to kodiakinvestmentsllc.com:',
            '',
            'Name: ' . $contact->name,
            'Email: ' . $contact->email,
            'Phone: ' . (empty($contact->phone_fmt) ? '--' : $contact->phone_fmt),
            'How ' . ($internal ? 'they' : 'you') . ' heard about Kodiak: ' . (empty($contact->source) ? '--' : $contact->source),
            'Planned deadline to move: ' . (empty($contact->move_deadline_fmt) ? '--' : $contact->move_deadline_fmt),
            'Housing budget: ' . (empty($contact->price_range_fmt) ? '--' : $contact->price_range_fmt),
            'Residency Terms: ' . (empty($contact->term_name) ? '--' : $contact->term_name),
            'Home size: ' . (empty($contact->home_size) ? '--' : $contact->home_size),
            'Family size: ' . (empty($contact->family_size) ? '--' : $contact->family_size),
            'Home traits: ' . (empty($contact->home_traits) ? '--' : $contact->home_traits),
            'Other comments: ' . (empty($contact->comments) ? '--' : $contact->comments),
        ];

        if (!$internal) {
            $url = $this->request->scheme() . '://' . $this->request->host() .
                   Router::url(
                       [
                           '_name' => 'contact-remove',
                           'id'    => $contact->id,
                           'hash'  => $contact->remove_hash
                       ]
                   );
            $body[] = '';
            $body[] = 'If you wish to be removed the above information from our system, <a href="' . $url . '">click here</a>';
        }

        return implode('<br>', $body);
    }

    /**
     * View method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    //private function _view($id = null) {
    //	$contact = $this->Contacts->get($id, [
    //		'contain' => ['Terms']
    //	]);
    //	$this->set('contact', $contact);
    //}

    /**
     * Add method
     *
     * @return void
     */
    //private function _add() {
    //	$contact = $this->Contacts->newEntity($this->request->data);
    //	if ($this->request->is('post')) {
    //		if ($this->Contacts->save($contact)) {
    //			$this->Flash->success('The contact has been saved.');
    //			return $this->redirect(['action' => 'index']);
    //		} else {
    //			$this->Flash->error('The contact could not be saved. Please, try again.');
    //		}
    //	}
    //	$terms = $this->Contacts->Terms->find('list');
    //	$this->set(compact('contact', 'terms'));
    //}

    /**
     * Edit method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    //private function _edit($id = null) {
    //	$contact = $this->Contacts->get($id, [
    //		'contain' => []
    //	]);
    //	if ($this->request->is(['post', 'put'])) {
    //		$contact = $this->Contacts->patchEntity($contact, $this->request->data);
    //		if ($this->Contacts->save($contact)) {
    //			$this->Flash->success('The contact has been saved.');
    //			return $this->redirect(['action' => 'index']);
    //		} else {
    //			$this->Flash->error('The contact could not be saved. Please, try again.');
    //		}
    //	}
    //	$terms = $this->Contacts->Terms->find('list');
    //	$this->set(compact('contact', 'terms'));
    //}

    /**
     * Delete method
     *
     * @param string $id
     *
     * @return void
     * @throws NotFoundException
     */
    //private function _delete($id = null) {
    //	$contact = $this->Contacts->get($id);
    //	$this->request->allowMethod('post', 'delete');
    //	if ($this->Contacts->delete($contact)) {
    //		$this->Flash->success('The contact has been deleted.');
    //	} else {
    //		$this->Flash->error('The contact could not be deleted. Please, try again.');
    //	}
    //	return $this->redirect(['action' => 'index']);
    //}
}
