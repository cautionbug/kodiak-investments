<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/21/2014
 * Time: 3:17 PM
 *
 * @type array $columns
 * @type \Thook\Cakerov\ORM\Entity $entity
 */
?>
<tr>
    <?php
    foreach ($columns as $column):
    ?>
        <td><?= $entity->$column; ?></td>
    <?php
    endforeach;
    ?>
    <td><?=
        $this->Html->link(
            'Edit',
            [
                'action' => 'edit',
                $entity->id
            ]
        ); ?></td>
    <?php
    if (isset($entity->active)):
    ?>
    <td class="pulse-result"><?=
        $this->Html->link(
            $entity->active ? 'Deactivate' : 'Activate',
            //$entity->active ? 'Deactivate' : 'Activate',
            [
                'action' => 'active',
                $entity->id,
                (int)!$entity->active
            ],
            [
                'data-toggle-active' => ''
            ]); ?></td>
    <?php
    else:
    ?>
    <td></td>
    <?php
    endif;
    ?>
</tr>
