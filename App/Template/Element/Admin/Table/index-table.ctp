<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/27/2014
 * Time: 3:16 AM
 *
 * Variables:
 *
 * @type Thook\Cakerov\ORM\Entity $entities
 * @type array                    $columns
 */

use Thook\Cakerov\Utility\Inflector;
//use Cake\Core\Configure;
//use Cake\Routing\Router;

$colCount = count($columns);
$rowCount = count($entities);
?>
<table role="grid">
    <caption><?=
        $this->Html->link(
            'Add New',
            [
                'action' => 'add'
            ]
        );
        ?></caption>
    <thead class="<?= $rowCount > 0 ? '' : 'hide'; ?>">
    <tr>
        <?php
        foreach (Inflector::humanizeArray($columns) as $column):
            ?>
            <th scope="col"><?= str_replace(' Fmt', '', $column); ?></th>
        <?php
        endforeach;
        ?>
        <th scope="colgroup" colspan="2">Actions</th>
    </tr>
    </thead>
    <tbody class="<?= $rowCount > 0 ? '' : 'hide'; ?>">
    <?php
    foreach ($entities as $entity):
        echo $this->element('Admin/Table/index-tr', compact('columns', 'entity'));
    endforeach;
    ?>
    </tbody>
    <tfoot>
    <tr>
        <td colspan="<?= $colCount + 2; ?>" class="text-center">
            <?= $rowCount > 0 ? '' : 'No records to display. Click to ' .
                                     $this->Html->link('add new', [
                                         'action' => 'add'
                                     ]) . ' records.'; ?>
        </td>
    </tr>
    </tfoot>
</table>
