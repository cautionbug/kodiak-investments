<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 11/24/2014
 * Time: 11:22 PM
 *
 * @type \App\Model\Entity\Attribute         $attributes
 * @type \App\Model\Entity\ListingsAttribute $listAttr
 */

$html      = '';
$attrsHtml = [];
$count     = 0; // First pre-increment starts indexes at 0.

foreach ($attributes as $attr) {
    $attrIdName    = "attributes.{$count}.id";
    $attrValueName = "attributes.{$count}._joinData.value";
    //$attrIdName    = "listings_attributes.{$count}.attribute_id";
    //$attrValueName = "listings_attributes.{$count}.value";
    $attrsHtml[]   = $this->Html->div(
        'columns small-16 medium-8' . ($attr->active ? '' : ' inactive'),
        $this->Form->hidden(
            "attributes.{$count}.name",
            [
                'value' => $attr->name,
                'disabled' => true,
            ]
        ) .
        $this->Form->hidden(
            "attributes.{$count}.abbrev",
            [
                'value' => $attr->abbrev,
                'disabled' => true,
            ]
        ) .
        $this->Form->hidden(
            "attributes.{$count}.icon_class",
            [
                'value' => $attr->icon_class,
                'disabled' => true,
            ]
        ) .
        $this->Form->hidden(
            "attributes.{$count}.requires_value",
            [
                'value' => $attr->requires_value,
                'disabled' => true,
            ]
        ) .
        $this->Form->hidden(
            "attributes.{$count}.active",
            [
                'value' => $attr->active,
                'disabled' => true,
            ]
        ) .
        $this->Form->label(
            $attrIdName,
            $this->Form->checkbox(
                $attrIdName,
                [
                    'id'                  => $this->Form->domId($attrIdName),
                    'hiddenField'         => false,
                    'value'               => $attr->id,
                    //'checked'             => $attr->current, //rand(0, 10) % 2, //
                    'data-requires-value' => ($attrValueId = $this->Form->domId($attrValueName)),
                ]
            ) . ' ' . $attr->name .
            ($attr->icon_class ? ' (&nbsp;<span class="' . $attr->icon_class . '"></span>&nbsp;)' : ''),
            ['escape' => false]
        ) . ($attr->requires_value
            ? $this->Form->text(
                $attrValueName,
                [
                    'label'    => false,
                    'id'       => $attrValueId,
                    'class'    => 'hide',
                    'disabled' => true,
                    'required' => true,
                    //'value'    => ??,
                ]
            )
            : '')
    );
    $count++;
}
foreach (array_chunk($attrsHtml, 2) as $attrHtml) {
    $html .= $this->Html->div('row', implode('', $attrHtml));
}
echo $this->Html->tag('fieldset',
                      $this->Html->tag('legend', 'Attributes') . $this->Html->div('column', $html),
                      ['class' => 'row']);
