<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/28/2014
 * Time: 3:20 PM
 *
 *
 */

use Cake\Routing\Router;

echo
$this->Html->div(
    'row button-row',
    $this->Html->div(
        'column',
        $this->Html->link(
            'Cancel',
            Router::url(['action' => 'index']),
            [
                'class' => 'button secondary radius left'
            ]
        ) .
        $this->Form->submit(
            'Save',
            [
                'class' => 'button radius right'
            ]
        )
    )
);
