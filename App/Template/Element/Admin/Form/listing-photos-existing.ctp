<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 12/6/2014
 * Time: 10:36 PM
 *
 * @type \App\Model\Entity\Photo $photos
 */

// EXISTING Photos
foreach ($photos as $photo) {
    echo
    $this->Html->div(
        'row',
        $this->Html->div(
            'columns small-10',
            $this->Html->image($photo->file_path, ['alt' => 'Listing Thumbnail'])
        ) .
        $this->Html->div(
            'columns small-6',
            $this->Html->tag(
                'ul',
                $this->Html->tag(
                    'li',
                    $this->Html->link(
                        '', '#', [
                              'class' => 'button im-chevron-circle-up',
                              'title' => 'Move picture UP the presentation order'
                          ]
                    )
                ) .
                $this->Html->tag(
                    'li',
                    $this->Html->link(
                        '', '#', [
                              'class' => 'button im-chevron-circle-down',
                              'title' => 'Move picture DOWN the presentation order'
                          ]
                    )
                ) .
                $this->Html->tag(
                    'li',
                    $this->Html->link(
                        '', '#', [
                              'class'   => 'button im-cancel-circle',
                              'title'   => 'Remove the picture from this Listing',
                              'confirm' => 'Are you sure you want to remove this Listing picture?'
                          ]
                    )
                ),
                [
                    'class' => 'button-group'
                ]
            )
        ),
        [
            'id' => 'listings-photos-' . $photo->id
        ]
    );
};

echo
$this->Html->div('columns small-16 medium-10',
    $this->Html->div()
);
