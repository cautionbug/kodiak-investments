<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/28/2014
 * Time: 3:25 PM
 *
 * @type Thook\Cakerov\ORM\Entity $entity
 * @type string                   $gridClass
 */

use Cake\Core\Configure;

$status = Configure::read('DataTypes.Bool.Status');

if (!isset($gridClass) || empty($gridClass)) {
    $gridClass = '';
}

echo
$this->Form->input(
    'active',
    [ //Configure::read('DataTypes.Status'), [
      'label'     => [
          'text'  => 'Status',
          'class' => 'required'
      ],
      'type'      => 'select',
      //'checked' => !!$park->active,
      'required'  => TRUE,
      'empty'     => FALSE,
      'options'   => $status,
      'value'     => array_search($entity->status, $status),
      'templates' => [
          'inputContainer'      => '<div class="' . $gridClass . ' {{type}}{{required}}">{{content}}</div>',
          'inputContainerError' => '<div class="' . $gridClass . ' {{type}}{{required}}">{{content}}{{error}}</div>',
      ]
    ]
);
