<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 7/27/2014
 * Time: 8:15 PM
 *
 * @type string $key
 * @type string $message
 * @type array  $params
 *  Expected $params keys:
 *      class
 *      head
 */

// Specific Flash elements are called in Controllers, which will make adjustments to variables and call this Element.
// All Flash elements should set keys used here. Testing presence of every damn thing is irritating.
?>
<div id="flash-<?= $key; ?>" class="alert-box <?= $params['class']; ?>" data-alert>
    <strong><?= $params['head']; ?>:</strong> <?= $message; ?>
    <a class="close" href="#"><span class="fi-x-circle"></span></a>
</div>
