<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/24/2014
 * Time: 11:07 PM
 *
 * @type string $key
 * @type string $message
 * @type array  $params
 */

// Set defaults for required $params keys
empty($params['class']) && ($params['class'] = 'error');
empty($params['head']) && ($params['head'] = 'Error');
// Set default $key if missing
isset($key) || ($key = 'flash');
