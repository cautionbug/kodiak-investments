<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/24/2014
 * Time: 11:04 PM
 *
 * @type string $key
 * @type string $message
 * @type array  $params
 */

// Set defaults for required $params keys
empty($params['class']) && ($params['class'] = 'success');
empty($params['head']) && ($params['head'] = 'Success');
// Set default $key if missing
isset($key) || ($key = 'flash');
