<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/15/2014
 * Time: 1:43 AM
 */
?>
<script>
    $(document).ajaxSend(
        function(e, xhr, settings){
            xhr.setRequestHeader('X-CSRF-Token', '<?= $this->request->params['_csrfToken']; ?>');
        }
    );
</script>
