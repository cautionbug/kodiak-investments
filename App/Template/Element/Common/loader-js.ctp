<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/24/2014
 * Time: 10:06 PM
 * http://www.html5rocks.com/en/tutorials/speed/script-loading/
 */
?>
<script>
	'use strict';

	!function tinyLoader(e,t,r){function n(){for(;d[0]&&'loaded'==d[0][f];)c=d.shift(),c[o]=!i.parentNode.insertBefore(c,i)}for(var s,a,c,d=[],i=e.scripts[0],o='onreadystatechange',f='readyState';s=r.shift();)a=e.createElement(t),'async'in i?(a.async=!1,e.head.appendChild(a)):i[f]?(d.push(a),a[o]=n):e.write('<'+t+' src="'+s+'" defer></'+t+'>'),a.src=s}(document,'script',['<?= implode("','", $loaderJS); ?>']);
</script>
