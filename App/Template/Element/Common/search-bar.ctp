<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 8/19/2014
 * Time: 12:26 AM
 */
echo $this->Html->div('f-dropdown medium content',
		// TODO: Add <H> tag to title the search form
		                  $this->Html->div('row',
		                                   $this->Html->tag('h4', 'Find A Home With Us', [
					                  			       'class' => 'brit'
		                                   ]) .
		                                   $this->Form->create(null, [
				                                                       'class' => 'column'
		                                                       ]) .
		                                   $this->Html->div('columns small-11',
		                                                    $this->Form->input('search', [
				                                                    'type'        => 'search',
				                                                    'label'       => [
						                                                    'for'  => 'property-search',
						                                                    'text' => ''
				                                                    ],
				                                                    'class'       => 'right hidden-for-screen',
				                                                    // TODO : Add tooltip title & class to describe search terms
				                                                    'id'          => 'property-search',
				                                                    'placeholder' => 'Enter Search Terms',
				                                                    'style'       => 'width:100%',
				                                                    'templates' => [
						                                                    'inputContainer' => '<div class="row"><div class="column {{type}}{{required}}">'.
						                                                                       '{{content}}</div></div>',
				                                                    ]
		                                                    ])
		                                   ) .
		                                   //$this->Html->div('columns small-9',
		                                   //                 $this->Html->tag('input', null, [
		                                   //                    'placeholder' => 'Find a home with us...',
		                                   //                    'style'       => 'width:100%',
		                                   //                    'type'        => 'search',
		                                   //                 ])
		                                   //) .
		                                   $this->Html->div('columns small-5',
		                                                    $this->Html->tag('button', 'Search <span class="fi-magnifying-glass"></span>',
		                                                                     [
				                                                                     'class' => 'tiny round button',
				                                                                     'type'  => 'submit',
				                                                                     'value' => 'Search'
		                                                                     ])
		                                   ) .
		                                   $this->Form->end()
		                                   //. $this->element('/Common/search-bar-tips')
		                  ),
		                  [
				                  'id'                    => 'search-box',
				                  'data-dropdown-content' => ''
		                  ]
);
