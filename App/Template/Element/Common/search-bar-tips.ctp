<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 8/24/2014
 * Time: 12:15 PM
 */
?>
<div class="column">
	<div class="row">
		<div class="column nowrap">
			<a class="click-toggle" data-toggle="search-tips">
				<span class="im-toggle-down"></span>
				<span class="toggle-hide-show">Show</span> Tips</a>
		</div>

		<div id="toggle-search-tips" class="column panel-tan radius hide">
			<p class="">Try things like:</p>
			<ul>
				<li>3bed, 2bath</li>
				<li>3000-4000 sqft, &lt; 700/mo</li>
				<li>off-street parking, w/d, for sale</li>
			</ul>
			<p><small>Separate search terms with commas.</small></p>
		</div>
	</div>
</div>
