<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 8/11/2014
 * Time: 8:54 PM
 *
 * @type array $response
 *
 * $response contents should be appropriate to the $renderMethod/$renderPath being used.
 */

$renderMethod = 'element';
$renderPath   = '/Common/Response/';

if (empty($response['render'])) {
	$renderPath .= 'reveal';
}
else {
	$renderMethod = key($response['render']);
	$renderPath .= $response['render'][$renderMethod];
}

echo $this->$renderMethod($renderPath, $response);
