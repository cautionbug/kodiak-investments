<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 7/31/2014
 * Time: 12:31 AM
 *
 * @type array $response
 * Expected components of $response:
 *
 * ['html'] => [
 *  'id'      => HTML ID for the Reveal container
 *  'class'   => Styling CSS Classes
 *  'header'  => Headline for the Reveal
 *  'message' => Content of the response
 * ]
 */
?>
<div id="flash-<?= $response['html']['id']; ?>" class="reveal-modal <?= $response['html']['class']; ?>" data-reveal>
	<h3><?= $response['html']['header']; ?></h3>

	<?= $this->Html->para(null, $response['html']['message']); ?>
	<a class="close-reveal-modal">&#215;</a>
</div>
