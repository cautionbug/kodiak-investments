<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/26/2014
 * Time: 2:19 AM
 *
 * @type string $idPrefix
 * @type array  $menu
 * @type string $menuType
 * @type string $navMenu
 * @type string $routeStr
 */

/*
 * Unfortunately CakePHP makes it very difficult to use - in URLs, insisting instead on _./
 * To make sure the menu items can match URL path segments, they have to be _ separated.
 * The idPrefix can still be dashed, as it's not used in comparison. It'll just look silly in HTML.
 * More annoying, using _ also requires Controller actions to be _ separated, NOT camel.
 * Unless the URL is camel, which is confusing and ugly in a URL.
 */

/** @noinspection PhpIncludeInspection */ // Because PhpStorm is still stupid about include/require paths.
require APP . 'Config/menu_' . (empty($navMenu) ? 'general' : $navMenu) . '.php';

// If not otherwise set, give $idPrefix a default.
empty($idPrefix) && ($idPrefix = 'nav-top');
// If not otherwise set, give $menuType a default.
empty($menuType) && ($menuType = 'top-bar');

// For menu in another position, pass a unique prefix to keep ID attributes unique.
echo $this->Html->parseMenu($menu, $idPrefix, $routeStr, $menuType, $this->Session->read('Auth.User.role') ?: '');
