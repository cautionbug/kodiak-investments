<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/29/2014
 * Time: 5:31 PM
 *
 * @var Cake\ORM\Entity $data
 */

// Medium+ @media breakpoint: Tabs

$widget  = 'tab';
$compact = compact('data', 'widget');
$useMap  = !empty(glob(WWW_ROOT . 'img/parks/' . $data['file_name_no_ext'] . '.*'));
?>
<dl id="about-park-<?= $data['id']; ?>" class="tabs" data-tab>
		<dd id="park-info-<?= $data['id'];?>"
        class="round-top active"><a href="#<?= $widget . '-' . $data['id']; ?>-info"
                          class="round-top"><span class="im-info"></span> Park Information</a></dd>
		<dd id="park-location-<?= $data['id']; ?>"
        class="round-top"><a href="#<?= $widget . '-' . $data['id']; ?>-location"
                              class="round-top"><span class="fi-map"></span> Location</a></dd>
<?php if ($useMap): ?>
		<dd id="park-map-<?= $data['id']; ?>"
        class="round-top"><a href="#<?= $widget . '-' . $data['id']; ?>-parkmap"
                              class="round-top"><span class="fi-photo"></span> Park Map</a></dd>
<?php endif; ?>
</dl>
<div class="tabs-content">
		<?=
		$this->element('/Interchange/content/about/park-info', $compact),
		$this->element('/Interchange/content/about/park-location', $compact),
		($useMap ? $this->element('/Interchange/content/about/park-map', $compact) : '');
		?>
</div>
<script>
  $('#about-park-<?= $data['id']; ?>').on('toggled.fndtn.tab', function(ev, tab){
    var mapContainer = $(tab).find('.map-container');
    if (mapContainer.length) {
      Kodiak.Maps.resizeMap(mapContainer);
    }
  })
</script>
