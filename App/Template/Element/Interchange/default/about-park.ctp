<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 7/2/2014
 * Time: 11:15 PM
 *
 * @var Cake\ORM\Entity $data
 */

// Default @media breakpoint: Accordion

$widget  = 'accordion';
$compact = compact('data', 'widget');
$useMap  = !empty(glob(WWW_ROOT . 'img/parks/' . $data['file_name_no_ext'] . '.*'));
?>
<dl id="about-park-<?= $data['id']; ?>" class="accordion" data-accordion>
		<dd class="accordion-navigation short-pad active">
				<a href="#<?= $widget . '-' . $data['id']; ?>-info"><span class="im-info"></span> Park Information</a>
				<?= $this->element('/Interchange/content/about/park-info', $compact); ?>
		</dd>
		<dd class="accordion-navigation short-pad">
				<a href="#<?= $widget . '-' . $data['id']; ?>-location"><span class="fi-map"></span> Location</a>
				<?= $this->element('/Interchange/content/about/park-location', $compact); ?>
		</dd>
<?php if ($useMap): ?>
		<dd class="accordion-navigation short-pad">
				<a href="#<?= $widget . '-' . $data['id']; ?>-parkmap"><span class="fi-photo"></span> Park Map</a>
				<?= $this->element('/Interchange/content/about/park-map', $compact); ?>
		</dd>
<?php endif; ?>
</dl>
<script>
  $('#about-park-<?= $data['id']; ?>').on('toggled.fndtn.accordion', function (ev, acc) {
    var mapContainer = $(acc).find('.map-container');
    if (mapContainer.length) {
      Kodiak.Maps.resizeMap(mapContainer);
    }
  })
</script>
