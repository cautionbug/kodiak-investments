<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 7/2/2014
 * Time: 11:37 PM
 *
 * This corresponds to an Accordion or Tab content panel.
 * The panel structure is the same, so based on CSS breakpoint, the page will call for Accordion or Tab
 * and include this panel.
 *
 * @var Cake\ORM\Entity $data
 * @var string          $widget
 */

echo
$this->Html->div('content',
                 //$this->Html->image('parks/' . $data['file_name_no_ext'] . '.png',
                 $this->Html->image('parks/' . $data['file_name_no_ext'] . '.png',
                                    [
		                                    'alt' => $data['name'] . ' Lot Map',
                                    ]),
                 [
		                 'id' => $widget . '-' . $data['id'] . '-parkmap',
                 ]
);
