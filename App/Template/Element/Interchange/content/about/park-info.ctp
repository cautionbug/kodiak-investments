<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 7/2/2014
 * Time: 11:26 PM
 *
 * This corresponds to an Accordion or Tab content panel.
 * The panel structure is the same, so based on CSS breakpoint, the page will call for Accordion or Tab
 * and include this panel.
 *
 * @var Cake\ORM\Entity $data
 * @var string          $widget
 */

echo
$this->Html->div('content active',
                 $this->Html->div('column',
                                  $data['info_body']),
                 [
                     'id' => $widget . '-' . $data['id'] . '-info',
                 ]);
