<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 7/2/2014
 * Time: 11:35 PM
 *
 * This corresponds to an Accordion or Tab content panel.
 * The panel structure is the same, so based on CSS breakpoint, the page will call for Accordion or Tab
 * and include this panel.
 *
 * @var Cake\Orm\Entity $data
 * @var string          $widget
 */

echo
$this->Html->div(
		'content',
		$this->Html->tag('address',
		                 $data['street'] . '<br>' .
		                 $data['city'] . ', ' . $data['state'] . ' ' . $data['zip'],
		                 [
				                 'class' => 'column',
		                 ]) .
		$this->Html->div(
				'map-container column', '',
				[
						'id'               => 'map-container-' . $data['id'],
						'data-map-options' => json_encode(
								[
										'lat'          => $data['lat'],
										'lng'          => $data['lng'],
										'full_address' => $data['street'] . ' ' . $data['city'] . ', ' .
										                  $data['state'] . ' ' . $data['zip'],
								])
				]),
		[
				'id' => $widget . '-' . $data['id'] . '-location',
		]
);
