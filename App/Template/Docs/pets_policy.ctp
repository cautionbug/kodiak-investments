<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/28/2014
 * Time: 2:48 PM
 */

$this->extend('/Common/container');

echo
$this->Html->nestedList([
    'Each pet needs a completed Pet Request Form on file.',
    'A photo of the animal must be included with the Request Form.',
    'There is an additional $10.00 charged in rent for each dog.',
    'For insurance and legal purposes, certain "aggressive" breeds are restricted from the community.',
    'You must clean up after your pet.',
    'Within the community, all dogs must be on a leash when outside the home.',
    'Make sure to thoroughly review the Pet Section of Rules & Regulations.',
    'Vet records are required.',
    'Current county/state licenses are required.',
                  ])
;

//? >
