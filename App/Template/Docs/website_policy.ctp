<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/28/2014
 * Time: 2:48 PM
 */

$this->extend('/Common/container');
?>
<p>Below are the standing policies regarding use of the Kodiak Investments, LLC website.</p>

<dl class="accordion" data-accordion>
    <dd class="accordion-navigation active">
        <a href="#privacy-policy" class="h3">Privacy Policy</a>

        <div id="privacy-policy" class="content active">
            <p>On this website, you may provide personal information about yourself, such as your name, contact email,
                and other details. This privacy policy describes how we collect and use that information.</p>

            <h4>How and where we collect your information</h4>
            <ul>
                <li>Contact Form</li>
            </ul>

            <h4>How we use your personal information</h4>

            <p>We will never rent, sell, share or otherwise disclose your personal information to third parties. We may
                contact you from time to time regarding new home listings or other business related to this website.</p>

            <p>We may also send occasional promotional material about new home listings or other updates to our website
                that you might find useful.</p>

            <p>You may opt out of receiving information from us at any time, and ask us to remove your contact
                information from our database.</p>
        </div>
    </dd>
    <dd class="accordion-navigation">
        <a href="#disclaimers" class="h3">Website Disclaimers</a>

        <div id="disclaimers" class="content">
            <p>All the information on this website is published in good faith and for general information only. We do
                not make any warranties about the completeness, reliability, or accuracy of this information. Any action
                you take based on the information on this website is strictly at your own risk, and we will not be
                liable for any losses or damages in connection with the use of our website.</p>

            <p>Our website may contain links to other websites using hyperlinks to those sites. While we strive to
                provide links to useful and ethical websites, we have no control over the content and nature of those
                sites. Links to other websites do not imply a recommendation of the information found there, nor
                indicate any implicit partnership between the representative entities and Kodiak Investments, LLC.</p>

            <p>Please be aware that when you leave our website, other sites may have different privacy policies and
                usage terms than those found here, and are beyond our control.</p>
        </div>
    </dd>
</dl>
