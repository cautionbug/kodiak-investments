<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/5/2014
 * Time: 5:02 PM
 *
 * Undefined Variables:
 *
 * @type array $pageCSS : Page-specific CSS links
 * @type array $pageJS  : Page-specific <script> tags
 */

$this->extend('/Common/container');

empty($pageJS) || $this->Html->script($pageJS, ['block' => 'page-js']);
empty($pageCSS) || $this->Html->css($pageCSS, ['block' => 'page-css']);

$this->start('flash-block');
// This assumes IF a Flash has been set, it has all the variables & keys expected by general_response.
echo $this->Flash->render('flash');
$this->end();

echo $this->element('/Admin/Table/index-table', compact('entities', 'columns'));
