<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/15/2014
 * Time: 7:15 PM
 *
 * @type App\Model\Entity\Park $entity
 */

use Cake\Core\Configure;
//use Cake\Routing\Router;

$this->extend('/Common/container');

empty($pageJS) || $this->Html->script($pageJS, ['block' => 'page-js']);
empty($pageCSS) || $this->Html->css($pageCSS, ['block' => 'page-css']);

echo
$this->Form->create($entity, [
        'novalidate' => TRUE,
        'type'       => 'file'
    ]
),
$this->Form->input('id', [
        'type' => 'hidden'
    ]
),
$this->Html->div('row',
    $this->Form->input('name', [
            'label'     => [
                'class' => 'required'
            ],
            'required'  => TRUE,
            'autofocus' => TRUE,
            'maxlength' => 100,
            'templates' => [
                'inputContainer'      => '<div class="columns medium-8 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ) .
    $this->Form->input('street', [
            'label'     => [
                'class' => 'required'
            ],
            'required'  => TRUE,
            'maxlength' => 50,
            'templates' => [
                'inputContainer'      => '<div class="columns medium-8 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    )
),
$this->Html->div('row',
    $this->Form->input('city', [
            'label'     => [
                'class' => 'required'
            ],
            'required'  => TRUE,
            'maxlength' => 50,
            'templates' => [
                'inputContainer'      => '<div class="columns medium-7 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns medium-7 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ) .
    $this->Form->input('county', [
            'maxlength' => 25,
            'templates' => [
                'inputContainer'      => '<div class="columns medium-4 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns medium-4 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ) .
    $this->Form->input('state', [
            'label'     => [
                'class' => 'required'
            ],
            'required'  => TRUE,
            'empty'     => TRUE,
            'options'   => Configure::read('DataLists.US_States'),
            'templates' => [
                'inputContainer'      => '<div class="columns small-6 medium-2 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-6 medium-2 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ) .
    $this->Form->input('zip', [
            'label'     => [
                'class' => 'required'
            ],
            'maxlength' => 10,
            'required'  => TRUE,
            'pattern'   => Configure::read('DataExpressions.Zip'),
            'templates' => [
                'inputContainer'      => '<div class="columns small-10 medium-3 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-10 medium-3 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    )
),
// TODO: Provide a LOOKUP mechanism to auto-retrieve Lat/Lon from GMaps.
$this->Html->div('row',
    $this->Form->input('lat', [
            'label'     => [
                'text' => 'Latitude'
            ],
            'maxlength' => 11,
            'max'       => 90,
            'min'       => -90,
            'step'      => 0.0000001,
            'pattern'   => Configure::read('DataExpressions.LatLng'),
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-3 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-3 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ) .
    $this->Form->input('lng', [
            'label'     => [
                'text' => 'Longitude'
            ],
            'maxlength' => 11,
            'max'       => 360,
            'min'       => 0,
            'step'      => 0.0000001,
            'pattern'   => Configure::read('DataExpressions.LatLng'),
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-3 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-3 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ) .
    $this->element('Admin/Form/field-status', [
            'entity'    => $entity,
            'gridClass' => 'columns small-6 medium-2'
        ]
    ).
    $this->Form->input('file_name_no_ext', [
            'label'       => [
                'text' => 'Park Map File Name (ext removed)'
            ],
            'readonly'    => TRUE,
            'maxlength'   => 100,
            'placeholder' => 'Value is set from Park Name',
            'templates'   => [
                'inputContainer'      => '<div class="columns small-10 medium-8 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-10 medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    )
),
$this->Html->div('row',
    $this->Html->div('columns medium-centered medium-15 p',
         $this->Form->input('info_body', [
                 'type'     => 'textarea',
                 'label'    => [
                     'class' => 'required',
                     'text'  => 'Park Information'
                 ],
                 'class'    => 'ckeditor',
                 'escape'   => FALSE,
                 'required' => TRUE,
                 'rows'     => 10
             ]
         )
    )
),
$this->Html->div('row',
    $this->Html->para('columns small-16 medium-12 medium-centered text-center',
                      'The file uploads below are optional. However, it helps a visitor get a visual idea of what a park looks like, ' .
                      'so providing a layout map of parks is highly recommended.')
),
$this->Html->div('row',
    $this->Form->input('park_img', [
            'label'     => [
                'text' => 'Park Map Image (JPG, PNG, GIF)'
            ],
            'type'      => 'file',
            'accept'    => 'image/gif,image/jpeg,image/png',
            'templates' => [
                'inputContainer'      => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ) .
    $this->Form->input(
        'park_pdf', [
            'label'     => [
                'text' => 'Park Map PDF'
            ],
            'type'      => 'file',
            'accept'    => 'application/pdf',
            'templates' => [
                'inputContainer'      => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    )
),
$this->element('Admin/Form/button-row'),
$this->Form->end()
;
