<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/21/2014
 * Time: 1:07 PM
 *
 * @type array                     $columns
 * @type array                     $response
 * @type \Thook\Cakerov\ORM\Entity $entity
 * @type bool                      $useReveal
 */

// Most of the time, the reveal is unnecessary. So if it's not set, set it FALSE.
isset($useReveal) || ($useReveal = FALSE);

echo json_encode(
    array_merge(
        $response['json'],
        [
            'content' => $this->element('/Admin/Table/index-tr', [
                    'columns' => $columns,
                    'entity' => $entity
                ]
            ),
            'html'    => $response['html'],
            'reveal'  => $useReveal ? $this->element('/Common/ajax-responder', $response) : NULL,
        ]
    )
);
