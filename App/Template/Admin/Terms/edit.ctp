<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/6/2014
 * Time: 10:51 PM
 *
 * @type App\Model\Entity\HouseCondition $entity
 */
use Cake\Core\Configure;

//use Cake\Routing\Router;

$this->extend('/Common/container');

empty($pageJS) || $this->Html->script($pageJS, ['block' => 'page-js']);
empty($pageCSS) || $this->Html->css($pageCSS, ['block' => 'page-css']);

echo
$this->Form->create($entity, [
        //'data-parsley-validate' => '',
        'novalidate' => TRUE,
    ]
),
$this->Form->input('id', [
        'type' => 'hidden'
    ]
),
$this->Html->div('row',
    $this->Form->input('name', [
            'label'     => [
                'class' => 'required'
            ],
            'required'  => TRUE,
            'autofocus' => TRUE,
            'maxlength' => 100,
            'templates' => [
                'inputContainer'      => '<div class="columns small-6 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-6 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ) .
    $this->element('Admin/Form/field-status', [
            'entity'    => $entity,
            'gridClass' => 'columns small-6 medium-2 end'
        ]
    )
),
$this->Html->div('row',
    $this->Html->div('columns medium-centered medium-15 p',
        $this->Form->input('fine_print', [
                'label'  => [
                    'text'   => 'Fine Print '. $this->Html->tag('span', '', [
                                'data-tooltip' => '',
                                'class'        => 'icon im-question has-tip tip-right',
                                'title'        => 'This is optional. Add any disclaimers or disclosures you may need to provide. ' .
                                                  'The info (if provided) will be provided with each listing of this Terms type.'
                            ]
                        ),
                    'escape' => FALSE,
                ],
                'type'   => 'textarea',
                'class'  => 'ckeditor',
                'escape' => FALSE,
                'rows'   => 10
            ]
        )
    )
),
$this->element('Admin/Form/button-row'),
$this->Form->end();
