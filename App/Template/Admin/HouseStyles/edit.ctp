<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/05/2014
 * Time: 7:36 PM
 *
 * @type App\Model\Entity\HouseStyle $entity
 */
use Cake\Core\Configure;

//use Cake\Routing\Router;

$this->extend('/Common/container');

empty($pageJS) || $this->Html->script($pageJS, ['block' => 'page-js']);
empty($pageCSS) || $this->Html->css($pageCSS, ['block' => 'page-css']);

echo
$this->Form->create($entity, [
        //'data-parsley-validate' => '',
        'novalidate' => TRUE,
    ]
),
$this->Form->input('id', [
        'type' => 'hidden'
    ]
),
$this->Html->div('row',
    $this->Form->input('name', [
            'label'     => [
                'class' => 'required'
            ],
            'required'  => TRUE,
            'autofocus' => TRUE,
            'maxlength' => 50,
            'templates' => [
                'inputContainer'      => '<div class="columns small-6 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-6 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ) .
    $this->element('Admin/Form/field-status', [
            'entity'    => $entity,
            'gridClass' => 'columns small-6 medium-2 end'
        ]
    )
),
$this->element('Admin/Form/button-row'),
$this->Form->end();
