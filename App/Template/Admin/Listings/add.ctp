<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 11/10/2014
 * Time: 8:09 PM
 *
 * @type \Thook\Cakerov\ORM\Entity $entity
 */

// Defaults for new Entity - maybe not the best place to set these?
$entity->set('active', $entity->active ?: false);
$this->extend('edit');
