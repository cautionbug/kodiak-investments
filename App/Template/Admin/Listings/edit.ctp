<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 11/10/2014
 * Time: 8:10 PM
 *
 * @type App\Model\Entity\Park $entity
 */

use Cake\Core\Configure;

$this->extend('/Common/container');

empty($pageJS) || $this->Html->script($pageJS, ['block' => 'page-js']);
empty($pageCSS) || $this->Html->css($pageCSS, ['block' => 'page-css']);

echo
$this->Form->create($entity, [
        //'novalidate' => TRUE,
        'type'       => 'file'
    ]
),
$this->Html->div('row',
    $this->Form->input('title', [
            'label' => [
                'text' => 'Listing Title ' . $this->Html->tag('span', '', [
                            'data-tooltip' => '',
                            'class'        => 'icon im-info has-tip tip-bottom',
                            'title'        => 'Give this listing an eye-catching headline. Or, just use the street address.'
                        ]
                    ),
                'class' => 'required',
                'escape' => FALSE,
            ],
            'required' => TRUE,
            'autofocus' => TRUE,
            'maxlength' => 200,
            'templates' => [
                'inputContainer'      => '<div class="column {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="column {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    )
),
$this->Html->div('row',
    $this->Form->input('street', [
            'label' => [
                'class' => 'required'
            ],
            'required' => TRUE,
            'maxlength' => 50,
            'templates' => [
                'inputContainer'      => '<div class="columns small-12 medium-9 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-12 medium-9 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    ) .
    $this->Form->input('lot_no', [
            'label' => 'Lot #',
            'maxlength' => 50,
            'templates' => [
                'inputContainer'      => '<div class="columns small-4 medium-3 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-4 medium-3 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    ) .
    $this->Form->input('lat', [
            'label'     => [
                'text' => 'Latitude'
            ],
            'type'      => 'number',
            'class'     => 'text-right',
            'maxlength' => 11,
            'max'       => 90,
            'min'       => -90,
            'step'      => 0.0000001,
            'pattern'   => Configure::read('DataExpressions.LatLng'),
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-2 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-2 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ) .
    $this->Form->input('lng', [
            'label'     => [
                'text' => 'Longitude'
            ],
            'type'      => 'number',
            'class'     => 'text-right',
            'maxlength' => 11,
            'max'       => 360,
            'min'       => 0,
            'step'      => 0.0000001,
            'pattern'   => Configure::read('DataExpressions.LatLng'),
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-2 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-2 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    )
),
$this->Html->div('row',
     $this->Form->input('city', [
             'label'     => [
                 'class' => 'required'
             ],
             'maxlength' => 50,
             'required'  => TRUE,
             'templates' => [
                 'inputContainer'      => '<div class="columns medium-7 {{type}}{{required}}">{{content}}</div>',
                 'inputContainerError' => '<div class="columns medium-7 {{type}}{{required}}">{{content}}{{error}}</div>',
             ],
         ]
     ) .
     $this->Form->input('county', [
             'maxlength' => 25,
             'templates' => [
                 'inputContainer'      => '<div class="columns medium-3 {{type}}{{required}}">{{content}}</div>',
                 'inputContainerError' => '<div class="columns medium-3 {{type}}{{required}}">{{content}}{{error}}</div>',
             ],
         ]
     ) .
     $this->Form->input('state', [
             'label'     => [
                 'class' => 'required'
             ],
             'required'  => TRUE,
             'empty'     => TRUE,
             'options'   => Configure::read('DataLists.US_States'),
             'templates' => [
                 'inputContainer'      => '<div class="columns small-6 medium-3 {{type}}{{required}}">{{content}}</div>',
                 'inputContainerError' => '<div class="columns small-6 medium-3 {{type}}{{required}}">{{content}}{{error}}</div>',
             ],
         ]
     ) .
     $this->Form->input('zip', [
             'label'     => [
                 'class' => 'required'
             ],
             'maxlength' => 10,
             'required'  => TRUE,
             'pattern'   => Configure::read('DataExpressions.Zip'),
             'templates' => [
                 'inputContainer'      => '<div class="columns small-10 medium-3 {{type}}{{required}}">{{content}}</div>',
                 'inputContainerError' => '<div class="columns small-10 medium-3 {{type}}{{required}}">{{content}}{{error}}</div>',
             ],
         ]
     )
),
$this->Html->div('row',
    $this->Form->input('area', [
            'type'      => 'number',
            'label'     => 'Area (Sq Ft)',
            'class'     => 'text-right',
            'min'       => 0,
            'step'      => 10,
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-3 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-3 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    ) .
    $this->Form->input('bedrooms', [
            'type'      => 'number',
            'label'     => [
                'text'  => 'Beds',
                'class' => 'required',
            ],
            'class'     => 'text-right',
            'min'       => 0,
            'required'  => TRUE,
            'step'      => 1,
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-3 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-3 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    ) .
    $this->Form->input('bathrooms', [
            'type'      => 'number',
            'label'     => [
                'text'  => 'Baths',
                'class' => 'required',
            ],
            'class'     => 'text-right',
            'min'       => 0,
            'required'  => TRUE,
            'step'      => 1,
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-3 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-3 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    ) .
    $this->Form->input('price', [
            'type'      => 'number',
            'label'     => [
                'text' => 'Price ' . $this->Html->tag('span', '', [
                            'data-tooltip' => '',
                            'class'        => 'icon im-info has-tip tip-bottom right',
                            'title'        => 'TIP: If Terms will be Sale Only, set the full sale price here. ' .
                                              'Otherwise, set a monthly amount. You can always clarify the numbers in the description.'
                        ]
                    ),
                'class'  => 'required',
                'escape' => FALSE,
            ],
            'class'     => 'text-right',
            'min'       => 0,
            'pattern'   => Configure::read('DataExpressions.Money'),
            'required'  => TRUE,
            'step'      => 1,
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-3 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-3 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    ) .
    $this->Form->input('ready_date', [
            'type'             => 'text',
            'data-date'        => '',
            'data-date-format' => '%m/%d/%Y',
            'data-nullable'    => '',
            //'value'            => $entity->ready_date ?: date('m/d/Y'),
            'templates'        => [
                'inputContainer'      => '<div class="columns medium-4 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns medium-4 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    )
),
$this->Html->div('row',
    $this->Html->div('columns small-16 medium-8',
        $this->element('Admin/Form/listing-attributes'),
        [
            'id' => 'listings-attributes'
        ]
    ) .
    $this->Html->div('columns small-16 medium-8',
        $this->Html->tag('fieldset',
            $this->Html->tag('legend', 'Other') .
            $this->Form->input('park_id',
                [
                    'empty'     => TRUE,
                    'templates' => [
                        'inputContainer'      => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}</div>',
                        'inputContainerError' => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
                    ]
                ]
            ) .
            $this->Form->input('house_condition_id',
                [
                    'label'     => 'Condition',
                    'empty'     => TRUE,
                    'templates' => [
                        'inputContainer'      => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}</div>',
                        'inputContainerError' => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
                    ]
                ]
            ) .
            $this->Form->input('house_style_id',
                [
                    'label'     => 'Style',
                    'empty'     => TRUE,
                    'templates' => [
                        'inputContainer'      => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}</div>',
                        'inputContainerError' => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
                    ]
                ]
            ) .
            $this->Form->input('term_id',
                [
                    'label'     => 'Terms',
                    'empty'     => TRUE,
                    'templates' => [
                        'inputContainer'      => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}</div>',
                        'inputContainerError' => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
                    ]
                ]
            ),
            [
                'class' => 'row'
            ]
        )
    )
) .
$this->element('Admin/Form/photos', ['page' => 'listing']) .
$this->Html->div('row',
    $this->Html->div('column',
        // RICH-TEXT EDITOR
        $this->Form->input('info_body',
            [
                'type'     => 'textarea',
                'label'    => [
                    'class' => 'required',
                    'text'  => 'Listing Information'
                ],
                'class'    => '',
                'escape'   => FALSE,
                'required' => TRUE,
                'rows'     => 10
            ]
        )
    ) .
    $this->Html->scriptBlock(<<<'JS'
'use strict';
CKEDITOR.replace('info_body', {removeButtons: 'Source,About'});
JS
    )
),
$this->Html->div('row',
    $this->Html->div('column',
        $this->Html->tag('fieldset',
            $this->Html->tag('legend', 'Ready to publish?') .
            $this->Html->para('',
                              __('Activate this button when the listing is ready to publish. ' .
                                 'You may preview changes before activating the Listing by clicking below.')
            ) .
            $this->Form->checkbox('active',
                [
                    'id'      => 'active',
                    //'checked' => $entity->active
                ]
            ) .
            $this->Form->label('active'),
            [
                'class'  => 'switch radius',
            ]
        )
    )
),
$this->element('Admin/Form/button-row'),
$this->Form->end()
;
