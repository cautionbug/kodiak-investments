<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/14/2014
 * Time: 10:35 PM
 */

$this->extend('/Common/container');

empty($pageJS) || $this->Html->script($pageJS, ['block' => 'page-js']);
empty($pageCSS) || $this->Html->css($pageCSS, ['block' => 'page-css']);

echo
$this->Form->create()
?>
<fieldset>
    <legend><?= __('Please enter your username and password.'); ?></legend>
    <?= $this->Form->input('username'), $this->Form->input('password'); ?>
</fieldset>
<?=
$this->Form->button(__('Login')),
$this->Form->end();
