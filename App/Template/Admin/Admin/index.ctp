<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/27/2014
 * Time: 2:14 AM
 */

$this->extend('/Common/container');
?>

<h1 class="brit">Admin Mode</h1>
<p>You're logged in to the Admin section of the website. Be careful about what you do here.</p>
<h2 class="brit">Basic Warnings</h2>
<ul>
    <li>Data you change is LIVE the moment you save it.</li>
    <li>Almost nothing can be deleted from the database. Instead you can Deactivate things you want to remove from view on the site.</li>
    <li>Some forms let you create fancy text &amp; content. Don't get too crazy, the site will handle most of the styles and layout itself.</li>
</ul>

