<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/5/2014
 * Time: 10:18 PM
 *
 * @type \Thook\Cakerov\ORM\Entity $entity
 */

// Defaults for new Entity - maybe not the best place to set these?
$entity->active = TRUE;
$entity->requires_value = FALSE;
$this->extend('edit');
