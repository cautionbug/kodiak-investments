<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/5/2014
 * Time: 10:18 PM
 *
 * @type App\Model\Entity\HouseCondition $entity
 */
use Cake\Core\Configure;

//use Cake\Routing\Router;

$this->extend('/Common/container');

empty($pageJS) || $this->Html->script($pageJS, ['block' => 'page-js']);
empty($pageCSS) || $this->Html->css($pageCSS, ['block' => 'page-css']);

echo
$this->Form->create($entity, [
        //'data-parsley-validate' => '',
        'novalidate' => TRUE,
    ]
),
$this->Form->input('id', [
        'type' => 'hidden'
    ]
),
$this->Html->div('row',
    $this->Form->input('name', [
            'label'     => [
                'class' => 'required'
            ],
            'required'  => TRUE,
            'autofocus' => TRUE,
            'maxlength' => 50,
            'templates' => [
                'inputContainer'      => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ) .
    $this->Form->input('abbrev', [
            'label'     => 'Abbreviated',
            'maxlength' => 10,
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-4 end {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-4 end {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    //) .
    //$this->Form->input('icon_class', [
    //        'label'     => [
    //            // Placing the icon before the label text because the tooltip won't position correctly with the classes i want.
    //            'text'   => $this->Html->tag('span', '', [
    //                        'data-tooltip' => '',
    //                        'class'        => 'icon im-question has-tip tip-bottom',
    //                        'title'        => 'This will eventually be a way to assign a visual icon to various listing traits. ' .
    //                                          'For now, ignore it.'
    //                    ]
    //                ) . ' Icon',
    //            'escape' => FALSE
    //        ],
    //        'class'     => 'radius',
    //        'maxlength' => 10,
    //        'templates' => [
    //            'inputContainer'      => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}</div>',
    //            'inputContainerError' => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}{{error}}</div>',
    //        ]
    //    ]
    )
),
$this->Html->div('row',
    $this->Form->input('requires_value', [
            'label'     => [
                'text'   => 'Requires Value?' . $this->Html->tag('span', '', [
                            'data-tooltip' => '',
                            'class'        => 'icon im-question has-tip tip-bottom right',
                            'title'        => 'When assigning this trait to a Listing, will it require a value specific to that listing?<br>' .
                                              'For example: "Square Feet" should have the square feet for the specific listing, ' .
                                              'but "Laundry Hookups" doesn\'t.'
                        ]
                    ),
                'escape' => FALSE,
            ],
            'options'   => Configure::read('DataTypes.Bool.YesNo'),
            'value'     => (int)$entity->requires_value,
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}{{error}}</div>'
            ]
        ]
    ) .
    $this->element(
        'Admin/Form/field-status', [
            'entity'    => $entity,
            'gridClass' => 'columns small-8 medium-2 end'
        ]
    )
),
$this->element('Admin/Form/button-row'),
$this->Form->end();
