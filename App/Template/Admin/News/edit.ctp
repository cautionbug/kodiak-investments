<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/6/2014
 * Time: 10:51 PM
 *
 * @type App\Model\Entity\HouseCondition $entity
 */

use Cake\Core\Configure;
//use Cake\Routing\Router;

$this->extend('/Common/container');

empty($pageJS) || $this->Html->script($pageJS, ['block' => 'page-js']);
empty($pageCSS) || $this->Html->css($pageCSS, ['block' => 'page-css']);

echo
$this->Form->create($entity, [
        //'data-parsley-validate' => '',
        'novalidate' => TRUE,
    ]
),
$this->Form->input('id', [
        'type' => 'hidden'
    ]
),
$this->Html->div('row',
    $this->Form->input('headline', [
            'label'     => [
                'class' => 'required'
            ],
            'required'  => TRUE,
            'autofocus' => TRUE,
            'maxlength' => 100,
            'templates' => [
                'inputContainer'      => '<div class="columns small-16 medium-10 medium-centered '.
                                         '{{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-16 medium-10 medium-centered '.
                                         '{{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    )
),
$this->Html->div('row',
    $this->Form->input('expires', [
            'type'             => 'text',
            'label'            => [
                'text' => 'Stop Displaying After',
            ],
            'data-date'        => '',
            'data-date-format' => '%m/%d/%Y',
            'data-nullable'    => '',
            'value'            => $entity->expires,
            'templates'        => [
                'inputContainer'      => '<div class="columns small-16 medium-6 medium-offset-3 ' .
                                         '{{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-16 medium-6 medium-offset-3 ' .
                                         '{{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    ) .
    $this->element(
        'Admin/Form/field-status', [
            'entity'    => $entity,
            'gridClass' => 'columns small-16 medium-4 end'
        ]
    )
),
$this->Html->div('row',
    $this->Html->div('columns medium-centered medium-15 p',
        $this->Form->input('content', [
                'type'     => 'textarea',
                'label'    => [
                    'class' => 'required',
                    'text'  => 'News Update'
                ],
                'class'    => 'ckeditor',
                'escape'   => FALSE,
                'required' => TRUE,
                'rows'     => 10
            ]
        )
    )
),
$this->element('Admin/Form/button-row'),
$this->Form->end();
