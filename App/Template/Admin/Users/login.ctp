<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/14/2014
 * Time: 10:35 PM
 */

$this->extend('/Common/container');

empty($pageJS) || $this->Html->script($pageJS, ['block' => 'page-js']);
empty($pageCSS) || $this->Html->css($pageCSS, ['block' => 'page-css']);

echo
$this->Html->div('row',
           $this->Form->create(NULL, [
                   'id'    => 'login-form',
                   'class' => 'columns small-16 medium-10 medium-centered'
               ]
           ) .
           $this->Flash->render('flash') .
           $this->Form->inputs(['username', 'password'],
                               ['legend' => __('Please enter your username and password.')]) .
           $this->Form->button(__('Login')) .
           $this->Form->end()
);
