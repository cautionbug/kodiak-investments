<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/15/2014
 * Time: 7:15 PM
 *
 *
 * @type App\Model\Entity\Park $user
 */

use Cake\Core\Configure;
use App\Controller\Admin\AbstractAdminController as Admin;
//use Cake\Routing\Router;

$this->extend('/Common/container');

empty($pageJS) || $this->Html->script($pageJS, ['block' => 'page-js']);
empty($pageCSS) || $this->Html->css($pageCSS, ['block' => 'page-css']);
$roles = Admin::$ROLES;
$status = Configure::read('DataTypes.Bool.Status');

echo
$this->Form->create($user, [
        //'data-parsley-validate' => '',
        'novalidate'            => TRUE,
    ]
),
$this->Form->input('id', [
        'type' => 'hidden'
    ]
),
$this->Html->div('row',
    $this->Form->input('first_name', [
            'required'  => TRUE,
            'autofocus' => TRUE,
            'maxlength' => 50,
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}{{error}}</div>'
            ]
        ]
    ) .
    $this->Form->input('last_name', [
            'required'  => TRUE,
            'autofocus' => TRUE,
            'maxlength' => 50,
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}{{error}}</div>'
            ]
        ]
    ) .
    $this->Form->input('username', [
            'label'     => 'User ID',
            'required'  => TRUE,
            'autofocus' => TRUE,
            'maxlength' => 50,
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}{{error}}</div>'
            ]
        ]
    ) .
    $this->Form->input('password', [
            'label'     => [
                'text'         => __('Password '.
                                  '<span data-tooltip class="has-tip tip-bottom im-question" title="'.
                                  'Hover or click into the password field to display it so you can write it down. ' .
                                  'Once it is stored, the password is encrypted and cannot be recovered.<br><br>' .
                                  'If you forget it, you\'ll have to be issued a new password."></span>'),
                'escape' => false,
            ],
            'type'      => 'text',
            'required'  => empty($user->id), // Don't require a password when editing a user.
            'autofocus' => TRUE,
            'maxlength' => 50,
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}{{error}}</div>'
            ]
        ]
    )
),
$this->Html->div('row',
    $this->Form->input('email', [
            'maxlength' => 100,
            'templates' => [
                'inputContainer'      => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-16 medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ).
    $this->Form->input('role', [
            'empty'     => '(none)',
            'options'   => array_combine($roles, $roles),
            'value'     => $user->role,
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ).
    $this->element('Admin/Form/field-status', [
            'entity'    => $user,
            'gridClass' => 'columns small-8 medium-4'
        ]
    )
),
$this->element('Admin/Form/button-row'),
$this->Form->end();
