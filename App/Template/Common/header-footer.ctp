<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/15/2014
 * Time: 5:58 PM
 *
 * Undefined variables:
 *
 * @type string $topNav
 * @type bool   $hideMenu
 */
isset($hideMenu) || ($hideMenu = FALSE);

$this->extend('/Common/body');

$this->start('header');
?>
	<div class="contain-to-grid sticky">
		<nav id="top-nav" class="top-bar" data-topbar>
			<ul class="title-area">
				<li class="name">
					<h1>
						<?=
						$this->Html->link(
								// TODO: SOME DAY... <picture>
								//$this->tag('picture',
								//           $this->Html->tag('source', '', [
								//
								//           ]) .
							$this->Html->image(
								'openclipart/462/kodiak-default.png',
								[
									'id'               => 'kodiak-nav-logo',
									'alt'              => 'Kodiak Investments LLC Logo',
									'data-interchange' => '[/img/openclipart/462/kodiak-default.png, (default)],' .
									                      '[/img/openclipart/462/kodiak-logo-side-britannic-banner-default.png, (large)]',
									                      //'data-interchange'     => '[/img/openclipart/462/kodiak.min.svg, (default)],' .
									                      //                          '[/img/openclipart/462/kodiak-logo-side-britannic.min.svg, (large)]',
									                      //'data-png-interchange' => '[/img/openclipart/462/kodiak-default.png, (default)],' .
									                      //                          '[/img/openclipart/462/kodiak-logo-side-britannic-banner-default.png, (large)]',
						        ]
							)
							//)
							,'/',
							['escape' => false]
						); ?>
					</h1>
				</li>
				<li class="toggle-topbar menu-icon">
					<a href="#">Menu</a>
				</li>
			</ul>
			<section class="top-bar-section clearfix">
				<ul class="right">
            <?= $hideMenu ? '' : ($this->element(empty($topNav) ? 'Common/menu' : $topNav)); ?>
				</ul>
			</section>
		</nav>
	</div>
<?php
echo $this->element('/Common/search-bar');
$this->end();

$this->start('footer');
$this->end();
