<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/15/2014
 * Time: 11:06 PM
 *
 * @type array $adminCSS
 * @type array $adminJS
 */
use Cake\Core\Configure;

$adminCssPathPrefix = Configure::read('App.adminBaseUrl') . Configure::read('App.cssBaseUrl');
$adminJsPathPrefix  = Configure::read('App.adminBaseUrl') . Configure::read('App.jsBaseUrl');

$this->start('meta');
// Not a fan of Cake's handling of META/LINK tags.
echo
$this->Html->charset('utf-8'), PHP_EOL,
$this->Html->tag('meta', NULL, ['http-equiv' => 'X-UA-Compatible', 'content' => 'IE=edge']), PHP_EOL,
$this->Html->meta('viewport', 'width=device-width, initial-scale=1.0, user-scalable=no'), PHP_EOL,
$this->Html->meta('description', ''), PHP_EOL,
$this->Html->meta('icon'), PHP_EOL,
$this->Html->tag('link', NULL, ['name' => 'author', 'content' => 'Joe Theuerkauf']), PHP_EOL,
$this->Html->tag('link', NULL, ['rel' => 'author', 'href' => 'https://plus.google.com/+JosephTheuerkauf/about']);
$this->end();

$this->Html->css(
    [
        //'libs/normalize.min',
        'core'
    ], [
        'block' => TRUE
    ]
);

// Don't bother building the admin-css block if there's nothing in it.
empty($adminCSS) ||
$this->Html->css($adminCSS, [
        'block'      => 'admin-css',
        'pathPrefix' => $adminCssPathPrefix
    ]
);

/*
 * The JS loaded here should be limited to those NECESSARY for the page to operate
 * to reduce render-blocking. Others should be added to the 'body-js' block below..
 */
$this->Html->script(
    (array)Configure::read('CDN.basics')['js'],
    ['block' => TRUE]
);
empty($headJS) ||
$this->Html->script(
    $headJS,
    ['block' => TRUE]
);

$this->start('body-js');
echo
$this->fetch('page-js'),
$this->Html->script([
                        'libs/jquery/smartresize.jquery.min',
                        'libs/jquery/detectmobilebrowser.min',
                        'jquery.thook',
                        'startup',
                    ]);
if (isset($this->request->params['_csrfToken'])) {
    echo $this->element('/Common/js/x-csrf-token');
}
$this->end();

// Don't bother building the admin-js block if there's nothing in it.
empty($adminJS) ||
$this->Html->script($adminJS, [
        'block'      => 'admin-js',
        'pathPrefix' => $adminJsPathPrefix
    ]
);
