<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/14/2014
 * Time: 10:40 PM
 */

$this->extend('/Common/head');

$this->start('body');
?>
  <div id="body-wrap">
    <header class="<?= $this->fetch('header-class'); ?>">
      <?= $this->fetch('header'); ?>
    </header>
    <div id="main-body" class="row">
      <div class="column">
        <?= $this->fetch('container'); ?>
      </div>
    </div>
    <footer class="hide-for-small">
      <div class="row">
        <div class="column">
          <?= $this->fetch('footer'); ?>
        </div>
      </div>
    </footer>
  </div>
<?php
$this->end();
