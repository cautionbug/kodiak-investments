<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/15/2014
 * Time: 10:32 PM
 */

$this->extend('/Common/header-footer');

$this->start('container');
$this->Flash->render();
?>
	<div class="row">
		<?php if (!empty($title)): ?>
		<h1 class="column"><?= $title; ?></h1>
		<?php endif; ?>
	</div>
<?php
if (($flashBlock = $this->fetch('flash-block'))):
?>
	<div id="flash-block" class="row">
		<div class="column">
			<?= $flashBlock; ?>
		</div>
	</div>
<?php
endif;
?>
	<div class="row" id="content">
		<div class="column">
			<?= $this->fetch('content'); ?>
		</div>
	</div>
<?php
$this->end();
