<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/29/2014
 * Time: 3:45 PM
 *
 * @type Cake\ORM\Entity $parks : Database records
 * @type array           $pageCSS : Page-specific CSS links
 * @type array           $pageJS : Page-specific <script> tags
 */
use Cake\Routing\Router;

$this->extend('/Common/container');

empty($pageJS) || $this->Html->script($pageJS, ['block' => 'page-js']);
empty($pageCSS) || $this->Html->css($pageCSS, ['block' => 'page-css']);

?>
<div class="row">
    <p class="column">Kodiak Investments, LLC is a Michigan-based, family owned and operated company in
        business since 2005.</p>

    <p class="column">Our corporate offices are located in Grand Rapids Michigan, but our inventory of manufactured homes are currently split between
        two parks: Base Line Estates in southwest Michigan, and Crystal Valley Estates in northern Indiana.</p>

    <p class="column">We currently have homes that have been professionally remodeled in the Allegan Michigan area, and in Middlebury Indiana (near
        Shipshewana).</p>

    <p class="column">Our inventory ranges from two-bedroom one-bath homes to four-bedroom two-bath homes, depending on the time of the year.</p>

    <p class="column">Our mission is to provide good quality homes, whether you are leasing or buying at affordable prices.</p>
</div>
<div class="row">
    <div class="panel radius column small-16 medium-8 medium-centered">
        <p>You may contact our corporate office at this address / phone number, or use our <?= $this->Html->link(
                'Contact Form',
                Router::url('contact')
            ); ?></p>
        <address class="text-center">Universal Group<br>1515 Michigan St.<br>Grand Rapids, MI 49503<br><a
                href="tel:+16164584949">616.458.4949</a></address>

    </div>
</div>
<dl class="accordion has-maps" data-accordion>
    <?php
    $active = ' active';
    foreach ($parks as $park):
    ?>
        <dd class="accordion-navigation<?= $active; ?>">
            <a class="h3" href="#accordion-<?= $park['id']; ?>"><?= $park['name'] . ' (' .
                                                                    $park['city'] . ',&nbsp;' .
                                                                    $park['state'] . ')'; ?></a>

            <div id="accordion-<?= $park['id']; ?>" class="content<?= $active; ?>">
                <div id="park-container-<?= $park['id']; ?>" class="has-maps park-container"
                     data-interchange="
						[/interchange/element/default/about/parks/<?= $park['id']; ?>, (default)],
						[/interchange/element/medium/about/parks/<?= $park['id']; ?>, (medium)]"></div>
            </div>
        </dd>
    <?php
//$active only has a value for the first loop.
        $active = '';
    endforeach;
    ?>
</dl>
