<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/22/2014
 * Time: 10:01 PM
 */

//use Cake\Routing\Router;

$this->extend('/Common/container');

empty($pageJS) || $this->Html->script($pageJS, ['block' => 'page-js']);
empty($pageCSS) || $this->Html->css($pageCSS, ['block' => 'page-css']);

