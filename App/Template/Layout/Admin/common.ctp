<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/17/2014
 * Time: 10:29 PM
 */

?>
<!DOCTYPE html>
<?php //@formatter:off ?>
<!--[if lt IE 9]><html class="ie8" lang="en-US"><![endif]-->
<!--[if gte IE 9]><!--><html class="no-js" lang="en-US"><!--<![endif]-->
<head>
	<?=
	$this->fetch('meta'),
	'<title>' . (!empty($title) ? $title . ' - ' : '') . 'Kodiak Investments, LLC</title>',
	$this->fetch('css'),
	$this->fetch('page-css'),
	$this->fetch('admin-css'),
	$this->fetch('script');

	//@formatter:on
    ?>
</head>
<body>
<!--[if lt IE 9]>
<p class="browsehappy text-center">We're sorry &ndash; If you're seeing this message, you're using an outdated browser
	not supported by this website.<br>Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your
	experience.</p>
<![endif]-->
<?=
$this->fetch('body'),
$this->fetch('body-js'),
$this->fetch('admin-js');
?>

</body>
</html>
