<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

?>
<!DOCTYPE html>
<html>
<head>
	<?php // Need a better <meta> generator for this. ?>
	<?= $this->fetch('meta'); ?>
	<title>
		<?= $this->fetch('title'); ?>
	</title>
	<?=
	$this->fetch('css'),
	$this->fetch('script');
	?>
</head>
<body>
<div id="container">
	<div id="content">
		<?= $this->Session->flash(); ?>

		<?= $this->fetch('content'); ?>
	</div>
	<div id="footer">
		<?php echo $this->Html->link(
				$this->Html->image('cake.power.gif', ['alt' => '', 'border' => '0']),
				'http://www.cakephp.org/',
				['target' => '_blank', 'escape' => false]
		);
		?>
	</div>
</div>
<?= $this->element('sql_dump'); ?>
</body>
</html>
