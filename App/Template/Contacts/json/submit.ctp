<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 7/5/2014
 * Time: 8:29 PM
 *
 * @type array $response
 */

$response['html']['message'] .= empty($email) ? '' : '&lt;' . h($email) . '&gt;';

// Get the rendered response element, then on up to the Layout (ajax).
echo json_encode(array_merge(
		                 $response['json'], [
				                 'content' => $this->element('/Common/ajax-responder', $response)
		                 ]
                 )
);
