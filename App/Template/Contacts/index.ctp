<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/22/2014
 * Time: 4:38 PM
 *
 * Variables:
 *
 * @type Thook\Cakerov\ORM\Entity $terms   : List of Terms sent by ContactController
 * @type Thook\Cakerov\ORM\Entity $captcha : Captcha Entity
 * @type Thook\Cakerov\ORM\Entity $contact : empty(?) Contact Entity
 *
 * @type array                    $pageCSS : Page-specific CSS links
 * @type array                    $pageJS  : Page-specific <script> tags
 */

use Cake\Core\Configure;
use Cake\Routing\Router;

//TODO : Look into using SQL to query for column comments and apply them as the label text.

$this->extend('/Common/container');

$this->Html->script($pageJS, ['block' => 'page-js']);
$this->Html->css($pageCSS, ['block' => 'page-css']);

$privacyLink = $this->Html->link(
    'privacy policy',
    [
        'controller' => 'docs',
        'action'     => 'website_policy'
    ]
);

$captchaURL = Router::url(['action' => 'captcha', 'id' => $captcha->id]);
?>
    <p>Interested in a Kodiak home, but don't see the right match on our site yet? We'll work hard to find what you're looking for. We enjoy direct
        contact with you, as it helps us understand what kind of home will best suit your needs.</p>
    <p>Please fill in the information below to give us the clearest idea what you need. We'll contact you to discuss options, and keep you updated as
        new listings are available.</p>
    <div>Please note the following:
        <ol>
            <li>This is <em>not</em> an application of any kind. We will not use this information for any other purpose than to establish contact with
                you
                for the purposes of finding a suitable Kodiak home based on the criteria you provide.
            </li>
            <li>If you provide your phone number, we may call from a private number. If you prefer not to receive such calls, please tell us in the
                comments
                space.
            </li>
        </ol>
    </div>
    <p>
        <small class="required-sample">These fields are required.</small>
        <small>If you do not wish to answer optional questions, simply leave them blank.</small>
    </p>
<?=
$this->Form->create($contact, [
        'action'                => 'submit',
        'data-parsley-validate' => '',
        'novalidate'            => TRUE,
    ]
),
// Name
$this->Html->div('row',
    $this->Form->input('name', [
            'label'     => [
                'class' => 'required',
            ],
            'class'     => 'radius',
            'required'  => TRUE,
            'autofocus' => TRUE,
            'maxlength' => 50,
            'templates' => [
                'inputContainer'      => '<div class="columns medium-centered medium-8 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    )
),
$this->Html->div('row',
    // Email
    $this->Form->input('email', [
            'type'      => 'email',
            'label'     => [
                'class' => 'required',
            ],
            'required'  => TRUE,
            'maxlength' => 75,
            'templates' => [
                'inputContainer'      => '<div class="columns medium-5 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns medium-5 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    ) .
    // Phone
    $this->Form->input('phone', [
            'type'      => 'tel',
            'maxlength' => 14,
            'templates' => [
                'inputContainer'      => '<div class="columns medium-5 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns medium-5 {{type}}{{required}}">{{content}}{{error}}</div>'
            ]
        ]
    ) .
    // Move Date
    $this->Form->input('move_deadline', [
            'type'             => 'text',
            'label'            => [
                'text' => 'When are you planning to move?',
            ],
            'data-date'        => '',
            'data-date-format' => '%m/%d/%Y',
            'data-nullable'    => '',
            'value'            => '',
            'templates'        => [
                'inputContainer'      => '<div class="columns medium-6 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns medium-6 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    )
),
$this->Html->div('row',
    // Price Range
    $this->Form->input('price_range', [
            'type'      => 'number',
            'label'     => [
                'title' => "What are you budgeting for a home?",
            ],
            'class'     => 'text-right',
            'min'       => 0,
            'step'      => 100,
            'pattern'   => Configure::read('DataExpressions.Money'),
            'value'     => 0,
            'title'     => "How much are you budgeting for a home?",
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    ) .
    // Listing Type
    $this->Html->div('columns small-8 medium-4',
        $this->Form->input('term_id', [
                'label'   => [
                    'text' => 'Listing Type?',
                ],
                'options' => $terms,
                'empty'   => TRUE,
            ]
        )
    ) .
    // Home Size
    $this->Form->input('home_size', [
            'label'     => [
                'title' => 'Square-footage, bed/bath count, etc.',
            ],
            'maxlength' => 100,
            'title'     => 'Sqare-footage, bed/bath count, etc.',
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    ) .
    // Family Size
    $this->Form->input('family_size', [
            'type'      => 'number',
            'class'     => 'text-right',
            'min'       => 1,
            'step'      => 1,
            'templates' => [
                'inputContainer'      => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns small-8 medium-4 {{type}}{{required}}">{{content}}{{error}}</div>',
            ]
        ]
    )
),
// Home Traits
$this->Html->div('row',
    $this->Form->input('home_traits', [
            'label '    => [
                'title' => "Tell us a little more about what you're looking for",
            ],
            'maxlength' => 200,
            'title'     => "Tell us a little more about what you're looking for",
            'templates' => [
                'inputContainer'      => '<div class="columns medium-8 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    ) .
    // Source
    $this->Form->input('source', [
            'label'     => [
                'text' => 'How did you hear about us?',
            ],
            'maxlength' => 200,
            'templates' => [
                'inputContainer'      => '<div class="columns medium-8 {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="columns medium-8 {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    )
),
    // Comments
$this->Html->div('row',
    $this->Form->input('comments', [
            'rows'      => 4,
            'templates' => [
                'inputContainer'      => '<div class="column {{type}}{{required}}">{{content}}</div>',
                'inputContainerError' => '<div class="column {{type}}{{required}}">{{content}}{{error}}</div>',
            ],
        ]
    )
),
$this->Html->div('panel radius',
    $this->Html->div('row',
        $this->Html->para('columns small-16 medium-14 medium-centered text-center',
                          "Please convince us you're not a sneaky no-good robot:"
        )
    ) .
    $this->Html->div('row button-row',
        $this->Html->div('columns small-16 medium-10 medium-centered',
            $this->Form->input('captcha', [
                    'label'                         => [
                        'class' => 'required',
                        'text'  => $captcha->question
                    ],
                    'required'                      => TRUE,
                    'class'                         => 'text-center text-caps',
                    'pattern'                       => '^[\d\w\s_-]+$',
                    'data-parsley-minlength'        => 1,
                    'data-parsley-remote'           => '',
                    'data-parsley-remote-options'   => json_encode(['data' => ['id' => $captcha->id]]),
                    'data-parsley-remote-validator' => 'captcha_json',
                    'data-parsley-remote-message'   => 'Incorrect. Please try again.',
                    'data-captcha-url'              => $captchaURL,
                ]
            )
        )
    )
),
$this->Html->div('row',
    $this->Html->para('column',
        <<<P
We take your privacy very seriously. We are asking for this information only to find a suitable home based on the
criteria you provide. Kodiak Investments will not release your information to any other parties. By submitting this
form you acknowledge you have read and agree to our {$privacyLink}.
P
    ) .
    $this->Html->div('column',
        $this->Html->div('row button-row',
            $this->Html->div('column',
                $this->Form->submit('Send', [
                        'class' => 'button radius right'
                    ]
                )
            )
        )
    )
),
$this->Form->end();

