<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 8/4/2014
 * Time: 8:42 PM
 *
 * @type array        $pageCSS : Page-specific CSS links
 * @type array        $pageJS  : Page-specific <script> tags
 * @type string       $header
 * @type string|array $content
 * @type string       $class
 * @type string       $iconClass
 */

$this->extend('/Common/container');

$this->Html->script($pageJS, ['block' => 'page-js']);
$this->Html->css($pageCSS, ['block' => 'page-css']);

// TODO: Remember what $iconClass was supposed to do & implement it.
?>
<div class="reveal-modal <?= $class; ?>" id="remove-reveal" data-reveal data-options="animation:none">
	<h3 class="text-center"><?= $header; ?></h3>
	<!--<h3 class="text-center --><?//= $iconClass; ?><!--">--><?//= $header; ?><!--</h3>-->
	<div class="p"><?= implode('</div><div class="p">', (array)$content); ?></div>
</div>
<div class="reveal-modal-bg hide"></div>

<?php //There's no auto-open, so a quick script will open it. ?>
<script>
	$(function(){
		$('#remove-reveal').foundation('reveal', 'open');
	});
</script>
