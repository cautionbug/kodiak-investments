<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 6/29/2014
 * Time: 7:12 PM
 *
 * @var string          $breakPoint : Defined by AJAX call param
 * @var string          $partial    : Defined by AJAX call param
 * @var Cake\ORM\Entity $data       : Typically 1 record to apply to the Element partials
 */

echo $this->element('/Interchange/' . $breakPoint . '/' . $partial, ['data' => $data]);
