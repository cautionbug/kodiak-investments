<?php
namespace App\Model\Entity;

use Thook\Cakerov\ORM\Entity;

/**
 * Term Entity.
 */
class Term extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'name'       => true,
        'fine_print' => true,
        'active'     => true,
        'contacts'   => true,
        'listings'   => true,
    ];
}
