<?php
namespace App\Model\Entity;

use Thook\Cakerov\ORM\Entity;
use Cake\I18n\Time;

/**
 * News Entity.
 */
class News extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'headline' => true,
        'content'  => true,
        'expires'  => true,
    ];

    protected function _setExpires($expires)
    {
        return (new Time(empty($expires) ? '+2 weeks' : $expires))->toUnixString();
    }

    protected function _getExpires(Time $expires = null)
    {
        return empty($expires) ? $expires : $expires->i18nFormat('MM/dd/YYYY');
    }
}
