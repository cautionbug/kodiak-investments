<?php
namespace App\Model\Entity;

use Thook\Cakerov\ORM\Entity;

/**
 * ListingsPhoto Entity.
 */
class ListingsPhoto extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'caption'  => true,
        'position' => true,
        'listing'  => true,
        'photo'    => true,
    ];

    protected function _getRecordId()
    {
        return $this->_properties['listing_id'] . '-' . $this->_properties['photo_id'];
    }
}
