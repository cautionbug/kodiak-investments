<?php
namespace App\Model\Entity;

use Cake\Auth\DefaultPasswordHasher;
use Thook\Cakerov\ORM\Entity;

/**
 * User Entity.
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
        //'username' => true,
        //'password' => true,
        //'first_name' => true,
        //'last_name' => true,
        //'email' => true,
        //'role' => true,
        //'last_login' => true,
        //'active' => true,
    ];

    protected function _getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }
}
