<?php
namespace App\Model\Entity;

use Thook\Cakerov\ORM\Entity;

/**
 * Photo Entity.
 */
class Photo extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'mime_type_id' => true,
        'file_path'    => true,
        'hash'         => true,
        'mime_type'    => true,
        'listings'     => true,
        'parks'        => true,
    ];
}
