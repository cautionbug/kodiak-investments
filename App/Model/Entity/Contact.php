<?php
namespace App\Model\Entity;

use Cake\I18n\Number;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\TableRegistry;
use Thook\Cakerov\ORM\Entity;

/**
 * Contact Entity
 */
class Contact extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'name'          => true,
        'email'         => true,
        'phone'         => true,
        'source'        => true,
        'move_deadline' => true,
        'price_range'   => true,
        'term_id'       => true,
        'home_size'     => true,
        'family_size'   => true,
        'home_traits'   => true,
        'ip_address'    => true,
        'comments'      => true,
        'remove_hash'   => true,
        'term'          => true,
    ];

    /**
     * Virtual Field
     *
     * @ param string $format
     *
     * @return null|string
     */
    protected function _getMoveDeadlineFmt()
    {
        return $this->move_deadline === null ? null : (new Time($this->move_deadline))->i18nFormat('MM/dd/YYYY');
    }

    /**
     * Virtual Field
     *
     * @return mixed
     */
    protected function _getPhoneFmt()
    {
        return preg_replace('/(\d{3})(\d{3})(\d{4})/', '(${1}) ${2}-${3}', $this->phone);
    }

    /**
     * Virtual Field
     *
     * @return string
     */
    protected function _getPriceRangeFmt()
    {
        return empty($this->price_range) ? null : Number::currency($this->price_range);
    }

    /**
     * @param $move_deadline
     *
     * @return null
     */
    protected function _setMoveDeadline($move_deadline)
    {
        return $move_deadline === null ? null : (new Time($move_deadline))->i18nFormat('YYYY-MM-dd');
    }

    /**
     * @param $phone
     *
     * @return mixed
     */
    protected function _setPhone($phone)
    {
        $this->_properties['phone'] = preg_replace('/\D/', '', $phone);

        return $phone;
    }

    protected function _setPriceRange($price_range)
    {
        Log::debug($price_range);

        return (int)$this->price_range === 0 ? null : $price_range;
    }

    /**
     * Produce a "random" string stripped of non-alphanumeric characters to act as an auto-removal key.
     * Url of this paired with correct record ID will delete that record.
     *
     * @param string|null $hash
     *
     * @return string
     */
    protected function _setRemoveHash($hash = null)
    {
        return $hash === null
            ? hash('sha1', uniqid('kodiak-', true) . uniqid('universal-', true))
            : preg_replace('/[^\dA-F]/i', '', $hash);
    }

    /**
     * Virtual Field
     *
     * @return null
     */
    protected function _getTermName()
    {
        return $this->term_id === null ? null : TableRegistry::get('Terms')->get($this->term_id)->name;
    }
}
