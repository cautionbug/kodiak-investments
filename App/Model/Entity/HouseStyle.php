<?php
namespace App\Model\Entity;

use Thook\Cakerov\ORM\Entity;

/**
 * HouseStyle Entity.
 */
class HouseStyle extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'name'     => true,
        'active'   => true,
        'listings' => true,
    ];
}
