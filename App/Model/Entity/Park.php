<?php
namespace App\Model\Entity;

use Thook\Cakerov\ORM\Entity;

/**
 * Park Entity.
 */
class Park extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        'name'             => true,
        'street'           => true,
        'city'             => true,
        'county'           => true,
        'state'            => true,
        'zip'              => true,
        'lat'              => true,
        'lng'              => true,
        'info_body'        => true,
        'file_name_no_ext' => true,
        'active'           => true,
        'listings'         => true,
        //'photos' => true,
        //'traits' => true,
    ];

    protected function _getFullAddress()
    {
        return "{$this->street} {$this->city} {$this->state} {$this->zip}";
    }
}
