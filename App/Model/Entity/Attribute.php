<?php
namespace App\Model\Entity;

//use Cake\Log\Log;
use Cake\Core\Configure;
use Thook\Cakerov\ORM\Entity;

/**
 * Attribute Entity.
 */
class Attribute extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @type array
     */
    protected $_accessible = [
        'id' => true,
        'name'           => true,
        'abbrev'         => true,
        'icon_class'     => true,
        'requires_value' => true,
        'active'         => true,
        'listings'       => true,
    ];

    protected function _getValueRequired()
    {
        return Configure::read('DataTypes.Bool.YesNo')[$this->requires_value];
    }

    protected function _getIcon()
    {
        return empty($this->icon_class)
            ? ''
            : '<span class="' . $this->icon_class . '"></span>';
    }

    protected function _getCurrent()
    {
        return isset($this->_properties['current']) ? $this->_properties['current'] : false;
    }

    protected function _setCurrent($current)
    {
        $this->_properties['current'] = $current;

        return $current;
    }
}
