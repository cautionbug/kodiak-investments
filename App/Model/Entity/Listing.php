<?php
namespace App\Model\Entity;

use Cake\Log\Log;
use Cake\I18n\Time;
use Thook\Cakerov\ORM\Entity;

/**
 * Listing Entity.
 */
class Listing extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @type array
     */
    protected $_accessible = [
        'title'              => true,
        'street'             => true,
        'city'               => true,
        'county'             => true,
        'state'              => true,
        'zip'                => true,
        'lat'                => true,
        'lng'                => true,
        'lot_no'             => true,
        'area'               => true,
        'bedrooms'           => true,
        'bathrooms'          => true,
        'price'              => true,
        'house_condition_id' => true,
        'park_id'            => true,
        'term_id'            => true,
        'house_style_id'     => true,
        'info_body'          => true,
        'ready_date'         => true,
        'active'             => true,
        'house_condition'    => true,
        'park'               => true,
        'term'               => true,
        'house_style'        => true,
        'attributes'         => true,
        'photos'             => true,
    ];

    protected function _getStreetLot($listing)
    {
        //Log::debug($listing);
        return $this->street . ' (Lot # ' . $this->lot_no . ')';
    }

    protected function _getFullAddress()
    {
        return "{$this->street} {$this->city} {$this->state} {$this->zip}";
    }

    protected function _getReadyDate($readyDate)
    {
        Log::debug(__FILE__ . '~' . __LINE__);
        Log::debug($readyDate);
        $readyDate = (new Time($readyDate))->i18nFormat([\IntlDateFormatter::SHORT, \IntlDateFormatter::NONE], null, 'en-US');
        Log::debug($readyDate);

        return $readyDate;
    }

    protected function _setReadyDate($readyDate)
    {
        Log::debug(__FILE__ . '~' . __LINE__);
        Log::debug($readyDate);
        $readyDate = (new Time($readyDate))->i18nFormat('YYYY-mm-dd');
        Log::debug($readyDate);

        return $readyDate;
    }
}
