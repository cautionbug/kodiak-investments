<?php
namespace App\Model\Entity;

//use Cake\Log\Log;
use Cake\ORM\Entity;

/**
 * ListingsAttribute Entity.
 */
class ListingsAttribute extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @type array
     */
    protected $_accessible = [
        'listing_id'   => true,
        'attribute_id' => true,
        'value'        => true,
        'listing'      => true,
        'attribute'    => true,
    ];
}
