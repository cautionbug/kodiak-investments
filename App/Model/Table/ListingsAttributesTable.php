<?php
namespace App\Model\Table;

//use Cake\Log\Log;
//use App\Model\Entity\ListingsAttribute;
//use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use Thook\Cakerov\ORM\Table;

/**
 * ListingsAttributes Model
 */
class ListingsAttributesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('listings_attributes');
        $this->displayField('value');
        $this->primaryKey('id');
        $this->belongsTo('Listings', [
            'foreignKey' => 'listing_id'
        ]);
        $this->belongsTo('Attributes', [
            'foreignKey' => 'attribute_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('listing_id', 'valid', ['rule' => 'numeric'])
            //->requirePresence('listing_id', 'create')
            ->notEmpty('listing_id')
            ->add('attribute_id', 'valid', ['rule' => 'numeric'])
            //->requirePresence('attribute_id', 'create')
            ->notEmpty('attribute_id')
            ->allowEmpty('value')
            //
            ->allowEmpty('listing_id', 'create')
            ->allowEmpty('attribute_id', 'create')
        ;

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['listing_id'], 'Listings'));
        $rules->add($rules->existsIn(['attribute_id'], 'Attributes'));
        return $rules;
    }
}
