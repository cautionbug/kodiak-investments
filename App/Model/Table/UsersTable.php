<?php
namespace App\Model\Table;

//use Cake\ORM\Query;
use Thook\Cakerov\ORM\Table;
use Cake\Validation\Validator;
use Cake\Core\Configure;
use App\Controller\Admin\AbstractAdminController as Admin;

/**
 * Users Model
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('users');
        $this->displayField('id');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            //->requirePresence('username', 'create')
            ->notEmpty('username')
            ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table'])
            //->requirePresence('password', 'create')
            ->notEmpty('password', null, 'create')
            //->requirePresence('first_name', 'create')
            ->notEmpty('first_name')
            //->requirePresence('last_name', 'create')
            ->notEmpty('last_name')
            ->add('email', 'valid', ['rule' => 'email'])
            //->requirePresence('email', 'create')
            ->notEmpty('email')
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table'])
            ->allowEmpty('role')
            ->add('last_login', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('last_login')
            ->add('active', 'valid', ['rule' => 'boolean'])
            //->requirePresence('active', 'create')
            ->notEmpty('active')
            // Additional Rules
            ->add('password', [
                                'length'   => [
                                    'rule'    => ['minLength', 8],
                                    'message' => 'Password must be at least 8 characters.'
                                ],
                                'strength' => [
                                    'rule'    => ['custom', Configure::read('DataExpressions.PasswordStrength')],
                                    'message' => (
                                    'Password must contain at least 3 of these character types: UPPERCASE & lowercase letters, numbers, punctuation'
                                    )
                                ],
                            ]
            )
            ->add('role', 'inList', [
                            'rule'    => ['inList', Admin::$ROLES],
                            'message' => 'Please enter a valid Role'
                        ]
            );

        return $validator;
    }
}
