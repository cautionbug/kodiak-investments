<?php
namespace App\Model\Table;

//use Cake\Log\Log;
//use App\Model\Entity\Attribute;
//use Cake\ORM\Query;
//use Cake\ORM\RulesChecker;
use Thook\Cakerov\ORM\Table;
use Cake\Validation\Validator;

/**
 * Attributes Model
 */
class AttributesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('attributes');
        $this->displayField('name');
        $this->primaryKey('id');
        $this->belongsToMany('Listings', [
            //'through' => 'ListingsAttributes'
            'foreignKey' => 'attribute_id',
            'targetForeignKey' => 'listing_id',
            'joinTable' => 'listings_attributes'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->allowEmpty('abbrev')
            ->allowEmpty('icon_class')
            ->add('requires_value', 'valid', ['rule' => 'boolean'])
            ->requirePresence('requires_value', 'create')
            ->notEmpty('requires_value')
            ->add('active', 'valid', ['rule' => 'boolean'])
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }
}
