<?php
namespace App\Model\Table;

//use Thook\Cakerov\ORM\Query;
use Thook\Cakerov\ORM\Table;
use Cake\Validation\Validator;

/**
 * Parks Model
 *
 * @method bool deleteFiles(string $fileName, array $dirs = [])
 * @method bool renameFiles(string $oldName, string $newName, mixed $dirs = [])
 * @method bool saveFiles  (string $fileName, mixed $dirs = [])
 */
class ParksTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        //$this->table('parks');
        //$this->displayField('name');
        //$this->primaryKey('id');

        $this->hasMany('Listings', [
            'foreignKey' => 'park_id',
        ]);

        $this->addBehavior('Map');
        $this->addBehavior('Timestamp');
        $this->addBehavior('Upload', [
                                       'objectDir' => $this->_table
                                   ]
        );
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->requirePresence('name', 'create')
            ->notEmpty('name')
            ->requirePresence('street', 'create')
            ->notEmpty('street')
            ->requirePresence('city', 'create')
            ->notEmpty('city')
            ->requirePresence('county', 'create')
            ->allowEmpty('county')
            ->requirePresence('state', 'create')
            ->notEmpty('state')
            ->requirePresence('zip', 'create')
            ->notEmpty('zip')
            ->add('lat', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('lat')
            ->add('lng', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('lng')
            ->allowEmpty('info_body')
            ->allowEmpty('file_name_no_ext')
            ->add('active', 'valid', ['rule' => 'boolean'])
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }
}
