<?php
namespace App\Model\Table;

//use Cake\ORM\Query;
use Thook\Cakerov\ORM\Table;
use Cake\Validation\Validator;

/**
 * News Model
 */
class NewsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        //$this->table('news');
        $this->displayField('headline');
        //$this->primaryKey('id');
        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            //->requirePresence('headline', 'create')
            ->notEmpty('headline')
            //->requirePresence('content', 'create')
            ->notEmpty('content')
            ->add('expires', 'valid', ['rule' => 'date'])
            ->allowEmpty('expires');

        return $validator;
    }
}
