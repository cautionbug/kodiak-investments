<?php
namespace App\Model\Table;

//use Cake\ORM\Query;
use Thook\Cakerov\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contacts Model
 */
class ContactsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        //$this->table('contacts');
        //$this->displayField('name');
        //$this->primaryKey('id');
        $this->addBehavior('Timestamp');

        $this->belongsTo('Terms', [
            'foreignKey' => 'term_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            //->requirePresence('name', 'create')
            ->notEmpty('name', 'create')
            //->requirePresence('email', 'create')
            ->notEmpty('email', 'create')
            ->add('email', 'valid', ['rule' => 'email'])
            // TODO: For now, allow email to be duplicated. Later, make it unique, with user-confirmation to overwrite existing.
            //->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table'])
            ->allowEmpty('price_range')
            ->add('price_range', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('term_id')
            ->add('term_id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('family_size')
            ->add('family_size', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('move_deadline')
            ->add('move_deadline', 'valid', ['rule' => 'date'])
            ->allowEmpty('phone')
            ->allowEmpty('source')
            ->allowEmpty('home_size')
            ->allowEmpty('home_traits')
            ->allowEmpty('ip_address')
            ->allowEmpty('comments')
            ->allowEmpty('remove_hash');

        return $validator;
    }
}
