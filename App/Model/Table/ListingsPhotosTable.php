<?php
namespace App\Model\Table;

//use Thook\Cakerov\ORM\Query;
use Thook\Cakerov\ORM\Table;
use Cake\Validation\Validator;

/**
 * ListingsPhotos Model
 */
class ListingsPhotosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        //$this->table('listings_photos');
        $this->displayField('caption');
        $this->belongsTo('Listings');
        $this->belongsTo('Photos');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('listing_id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('listing_id', 'create')
            ->add('photo_id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('photo_id', 'create')
            ->allowEmpty('position')
            ->allowEmpty('caption');

        return $validator;
    }
}
