<?php
namespace App\Model\Table;

//use Thook\Cakerov\ORM\Query;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Terms Model
 */
class TermsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        //$this->table('terms');
        //$this->displayField('name');
        //$this->primaryKey('id');

        $this->hasMany('Contacts');
        $this->hasMany('Listings');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            //->requirePresence('name', 'create')
            ->notEmpty('name')
            ->allowEmpty('fine_print')
            ->add('active', 'valid', ['rule' => 'boolean'])
            //->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }
}
