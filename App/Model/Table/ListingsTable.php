<?php
namespace App\Model\Table;

//use Cake\Log\Log;
//use App\Model\Entity\Listing;
//use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use Thook\Cakerov\ORM\Table;

/**
 * Listings Model
 */
class ListingsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        $this->table('listings');
        $this->displayField('title');
        $this->primaryKey('id');
        $this->addBehavior('Timestamp');
        $this->belongsTo('HouseConditions', [
            'foreignKey' => 'house_condition_id'
        ]);
        $this->belongsTo('Parks', [
            'foreignKey' => 'park_id'
        ]);
        $this->belongsTo('Terms', [
            'foreignKey' => 'term_id'
        ]);
        $this->belongsTo('HouseStyles', [
            'foreignKey' => 'house_style_id'
        ]);
        $this->belongsToMany('Attributes', [
            //'through' => 'ListingsAttributes',
            'foreignKey' => 'listing_id',
            'targetForeignKey' => 'attribute_id',
            'joinTable' => 'listings_attributes'
        ]);
        //$this->belongsToMany('Photos', [
        //    //'through' => 'ListingsPhotos',
        //    'foreignKey' => 'listing_id',
        //    'targetForeignKey' => 'photo_id',
        //    'joinTable' => 'listings_photos'
        //]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->requirePresence('title', 'create')
            ->notEmpty('title')
            ->requirePresence('street', 'create')
            ->notEmpty('street')
            ->requirePresence('city', 'create')
            ->notEmpty('city')
            ->allowEmpty('county')
            ->requirePresence('state', 'create')
            ->notEmpty('state')
            ->requirePresence('zip', 'create')
            ->notEmpty('zip')
            ->add('lat', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('lat')
            ->add('lng', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('lng')
            ->allowEmpty('lot_no')
            ->add('area', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('area')
            ->add('bedrooms', 'valid', ['rule' => 'numeric'])
            ->requirePresence('bedrooms', 'create')
            ->notEmpty('bedrooms')
            ->add('bathrooms', 'valid', ['rule' => 'numeric'])
            ->requirePresence('bathrooms', 'create')
            ->notEmpty('bathrooms')
            ->add('price', 'valid', ['rule' => 'decimal'])
            ->requirePresence('price', 'create')
            ->notEmpty('price')
            ->add('house_condition_id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('house_condition_id')
            ->add('park_id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('park_id')
            ->add('term_id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('term_id')
            ->add('house_style_id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('house_style_id')
            ->requirePresence('info_body', 'create')
            ->notEmpty('info_body')
            ->add('ready_date', 'valid', ['rule' => 'date', 'format' => 'mdy'])
            ->allowEmpty('ready_date')
            ->add('active', 'valid', ['rule' => 'boolean'])
            ->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['house_condition_id'], 'HouseConditions'));
        $rules->add($rules->existsIn(['park_id'], 'Parks'));
        $rules->add($rules->existsIn(['term_id'], 'Terms'));
        $rules->add($rules->existsIn(['house_style_id'], 'HouseStyles'));
        return $rules;
    }
}
