<?php
namespace App\Model\Table;

//use Thook\Cakerov\ORM\Query;
use Thook\Cakerov\ORM\Table;
use Cake\Validation\Validator;

/**
 * HouseStyles Model
 */
class HouseStylesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        //$this->table('house_styles');
        //$this->displayField('name');
        //$this->primaryKey('id');

        $this->hasMany('Listings', [
            'foreignKey' => 'house_style_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            //->requirePresence('name', 'create')
            ->notEmpty('name')
            ->add('active', 'valid', ['rule' => 'boolean'])
            //->requirePresence('active', 'create')
            ->notEmpty('active');

        return $validator;
    }
}
