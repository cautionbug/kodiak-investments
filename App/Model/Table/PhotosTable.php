<?php
namespace App\Model\Table;

//use Thook\Cakerov\ORM\Query;
use Thook\Cakerov\ORM\Table;
use Cake\Validation\Validator;

/**
 * Photos Model
 */
class PhotosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     *
     * @return void
     */
    public function initialize(array $config)
    {
        //$this->table('photos');
        $this->displayField('file_path');
        //$this->primaryKey('id');

        $this->belongsTo('MimeTypes', [
            'className'  => 'Thook\Websites\Model\Table\MimeTypesTable',
            'foreignKey' => 'mime_type_id',
        ]);
        $this->belongsToMany('Listings', [
            //'foreignKey'       => 'photo_id',
            //'targetForeignKey' => 'listing_id',
             'through'      => 'ListingsPhotos',
             'saveStrategy' => 'replace',
        ]);
        //$this->belongsToMany('Parks',
        //                     [
        //                         //'foreignKey'       => 'photo_id',
        //                         //'targetForeignKey' => 'park_id',
        //                         'through'          => 'ParksPhotos',
        //                         'saveStrategy'     => 'replace',
        //                     ]
        //);

        $this->addBehavior('Timestamp');
        $this->addBehavior('Upload', [
            'objectDir' => $this->_table
        ]);
        $this->addBehavior('Thumbnail');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator
     *
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create')
            ->add('mime_type_id', 'valid', ['rule' => 'numeric'])
            //->requirePresence('mime_type_id', 'create')
            ->notEmpty('mime_type_id')
            //->requirePresence('file_path', 'create')
            ->notEmpty('file_path')
            ->allowEmpty('hash');

        return $validator;
    }
}
