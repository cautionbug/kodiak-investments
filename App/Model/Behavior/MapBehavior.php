<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 11/9/2014
 * Time: 11:34 PM
 */

namespace App\Model\Behavior;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\Behavior;
//use Thook\Cakerov\ORM\Behavior;
use Thook\Cakerov\ORM\Entity;
use Thook\Cakerov\ORM\Table;
use Cake\Log\Log;

class MapBehavior extends Behavior
{
    protected $_defaultConfig = [];

    public function __construct(Table $table, array $config = [])
    {
        $this->config('gmapAPIKey', Configure::read('App.gmapAPIKey'));
    }

    /**
     * When an address is provided but Lat/Lng is unknown, this can retrieve them via Google Maps API - provided the address is valid.
     *
     * @param string $address
     * @param bool   $valuesOnly
     *
     * @return array
     */
    public function gmapLatLng($address, $valuesOnly = false)
    {
        $latLng  = [
            'lat' => null,
            'lng' => null
        ];
        $address = urlencode($address);
        $json    =
            file_get_contents(
                "https://maps.googleapis.com/maps/api/geocode/json?sensor=false&region=us&language=en-US&address={$address}&key=" .
                $this->_config['gmapAPIKey']
            );
        $json    = json_decode($json);
        if ($json->{'status'} === 'OK') {
            $latLng['lat'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'} ?: null;
            $latLng['lng'] = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'} ?: null;
        }

        return $valuesOnly ? array_values($latLng) : $latLng;
    }

    public function beforeSave(Event $event, Entity $entity)
    {
        if (empty($entity->lat) || empty($entity->lng)) {
            list($entity->lat, $entity->lng) = $this->gmapLatLng($entity->full_address, true);
        }
    }
}
