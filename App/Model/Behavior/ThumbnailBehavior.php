<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 11/16/2014
 * Time: 7:23 PM
 */

namespace App\Model\Behavior;

use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\Behavior;
//use Thook\Cakerov\ORM\Behavior;
use Thook\Cakerov\ORM\Entity;
use Thook\Cakerov\ORM\Table;
use Cake\Log\Log;

class ThumbnailBehavior extends Behavior
{
    protected $_defaultConfig = [];

    public function __construct(Table $table, array $config = [])
    {
    }

    public function thumbnail($imgPath, $thumbPath, $maxDim = 125)
    {
        $thumb = new \Imagick($imgPath);
        $thumb->thumbnailImage($maxDim, $maxDim, true);
        $thumb->writeImage($thumbPath);
    }

    public function beforeSave(Event $event, Entity $entity)
    {
        // TODO: Logic here to loop through NEW & CHANGED images to create thumbnails
    }
}
