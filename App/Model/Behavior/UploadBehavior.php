<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 11/3/2014
 * Time: 11:32 PM
 */

namespace App\Model\Behavior;

//use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\ORM\Behavior;

//use Thook\Cakerov\ORM\Behavior;
//use Thook\Cakerov\ORM\Entity;
use Thook\Cakerov\ORM\Table;

//use Cake\Log\Log;

class UploadBehavior extends Behavior
{
    const IMG_DIR   = 'img';
    const FILES_DIR = 'files';

    protected $_defaultConfig = [
        // This doesn't appear to work - Table class doesn't recognize aliases when called.
        //'implementedMethods' => [
        //    'delete' => 'deleteFiles',
        //    'rename' => 'renameFiles',
        //    'save'   => 'saveFiles'
        //],
        'directories' => [
            'object'        => '',
            self::FILES_DIR => self::FILES_DIR,
            self::IMG_DIR   => self::IMG_DIR,
        ],
        'mimes'       => [
            'application/pdf' => [
                'dir' => self::FILES_DIR,
                'ext' => 'pdf'
            ],
            'image/gif'       => [
                'dir' => self::IMG_DIR,
                'ext' => 'gif'
            ],
            'image/jpeg'      => [
                'dir' => self::IMG_DIR,
                'ext' => 'jpg'
            ],
            'image/png'       => [
                'dir' => self::IMG_DIR,
                'ext' => 'png'
            ],
        ]
    ];

    public function __construct(Table $table, array $config = [])
    {
        if (isset($config['directories']) && is_array($config['directories'])) {
            $this->config('directories', $config['directories']);
        } else {
            foreach ($config as $key => $setting) {
                if (substr($key, -3) === 'Dir') {
                    $this->config('directories.' . substr($key, 0, -3), $setting);
                }
            }
        }
        if (isset($config['mimes']) && is_array($config['mimes'])) {
            $this->config('mimes', $config['mimes'], false);
        }
    }

    /**
     * @param string       $fileName
     * @param array|string $dirs
     *
     * @return bool
     */
    //public function delete($fileName, $dirs = [self::IMG_DIR, self::FILES_DIR])
    public function deleteFiles($fileName, $dirs = [self::IMG_DIR, self::FILES_DIR])
    {
        $unlinked = true;
        if ($dirs === false) {
            $unlinked = unlink($fileName);
        } else {
            foreach ($this->_getGlob($dirs, $fileName) as $file) {
                if (false === ($unlinked = unlink($file))) {
                    break;
                }
            }
        }

        return $unlinked;
    }

    /**
     * @param string       $oldName
     * @param string       $newName
     * @param array|string $dirs
     *
     * @return bool
     */
    //public function rename($oldName, $newName, $dirs = [self::IMG_DIR, self::FILES_DIR])
    public function renameFiles($oldName, $newName, $dirs = [self::IMG_DIR, self::FILES_DIR])
    {
        $moved = true;
        foreach ($this->_getGlob($dirs, $oldName) as $file) {
            $ext = $this->_config['mimes'][(new File($file))->mime()]['ext'];
            if (false === ($moved = rename($file, rtrim(dirname($file), DS) . DS . $newName . '.' . $ext))) {
                break;
            }
        }

        return $moved;
    }

    /**
     * @param string $name
     * @param array  $fileData
     * @param bool   $convertToPng
     *
     * @return bool
     */
    //public function save($name, array $fileData, $convertToPng = FALSE)
    public function saveFiles($name, array $fileData, $convertToPng = false)
    {
        $mime = (new File($fileData['tmp_name']))->mime();
        if (null === ($parts = $this->config('mimes.' . $mime))) {
            return false;
        }

        $moveTo = WWW_ROOT . $parts['dir'] . DS . $this->_config['directories']['object'] . DS . $name . '.' . $parts['ext'];
        $moved  = is_uploaded_file($fileData['tmp_name']) &&
                  move_uploaded_file($fileData['tmp_name'], $moveTo);
        if ($convertToPng) {
            $converted = (false === strpos($mime, 'image') || $this->saveToPng($moveTo));
        } else {
            $converted = true;
        }

        return $moved && $converted;
    }

    /**
     * Quick & Dirty save to PNG to keep the View simple.
     *
     * @param $filePath
     *
     * @return bool
     */
    public function saveToPng($filePath)
    {
        $img = new \Imagick($filePath);
        $img->setFormat('png');
        $dotSplode = explode('.', $filePath);
        $pngPath   = preg_replace('/\.' . array_pop($dotSplode) . '$/', '.png', $filePath);

        return $this->deleteFiles($filePath, false) && $img->writeImage($pngPath);
    }

    private function _getGlob($dirs, $fileName)
    {
        // i WISH it could be this cool. But Cake's config doesn't accept an array as a getter (forces it into write mode),
        // so instead the directories need to be looped & retrieved one at a time.
        //// Generate comma-separated string of directory names from dot-noted config keys assembled from passed/default $dirs string/array.
        //$directories = implode(',', $this->config(explode('|', 'directories.' . implode('|directories.', (array)$dirs))));

        $configs = explode('|', 'directories.' . implode('|directories.', (array)$dirs));
        $paths   = [];
        for ($i = 0, $c = count($configs); $i < $c; ++$i) {
            $paths[] = $this->config($configs[$i]);
        }
        $globStr = WWW_ROOT . '{' . implode(',', $paths) . '}' . DS . $this->_config['directories']['object'] . DS . $fileName . '.*';
        $glob    = glob($globStr, GLOB_BRACE);

        return $glob;
    }
}
