<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/1/2014
 * Time: 10:13 PM
 */

use App\Controller\Admin\AbstractAdminController as Admin;

$menu = [
    // HOME
    [
        'a-attrs'  => [
            'id' => '',
        ],
        'li-attrs' => [
            'id'    => '',
            'class' => '',
        ],
        'text'     => $this->Html->tag('span', '', ['class' => 'im-home']) . ' Main Site',
        'url'      => ['_name' => 'home'],
    ],
    // LISTINGS
    [
        'a-attrs'  => [
            'id' => '',
        ],
        'li-attrs' => [
            'id'    => 'listings',
            'class' => '',
        ],
        'text'     => 'Listings',
        'url'      => [
            'prefix'     => 'admin',
            'controller' => 'Listings',
            'action'     => 'index'
        ],
    ],
    // PARKS
    [
        'a-attrs'  => [
            'id' => '',
        ],
        'li-attrs' => [
            'id'    => 'parks',
            'class' => '',
        ],
        'text'     => 'Parks',
        'url'      => [
            'prefix'     => 'admin',
            'controller' => 'Parks',
            'action'     => 'index'
        ],
    ],
    // NEWS
    [
        'a-attrs'  => [
            'id' => '',
        ],
        'li-attrs' => [
            'id'    => 'news',
            'class' => '',
        ],
        'text'     => 'News Updates',
        'url'      => [
            'prefix'     => 'admin',
            'controller' => 'News',
            'action'     => 'index'
        ],
    ],
    // OTHER
    [
        'a-attrs'  => [
            'id' => ''
        ],
        'li-attrs' => [
            'id'    => 'other',
            'class' => 'not-click',
        ],
        'text'     => 'Other&hellip;',
        'url'      => NULL, //'javascript:void(0);',
        // SUB-MENU
        'sub'      => [
            // HOUSE CONDITIONS
            [
                'a-attrs'  => [
                    'id' => '',
                ],
                'li-attrs' => [
                    'id'    => 'house_conditions',
                    'class' => '',
                ],
                'text'     => 'House Conditions',
                'url'      => [
                    'prefix'     => 'admin',
                    'controller' => 'HouseConditions',
                    'action'     => 'index'
                ],
            ],
            // HOUSE STYLES
            [
                'a-attrs'  => [
                    'id' => '',
                ],
                'li-attrs' => [
                    'id'    => 'house_styles',
                    'class' => '',
                ],
                'text'     => 'House Styles',
                'url'      => [
                    'prefix'     => 'admin',
                    'controller' => 'HouseStyles',
                    'action'     => 'index'
                ],
            ],
            // ITEM ATTRIBUTES
            [
                'a-attrs'  => [
                    'id' => '',
                ],
                'li-attrs' => [
                    'id'    => 'item_attrs',
                    'class' => '',
                ],
                'text'     => 'Item Attributes',
                'url'      => [
                    'prefix'     => 'admin',
                    'controller' => 'ItemAttrs',
                    'action'     => 'index'
                ],
            ],
            // TERMS
            [
                'a-attrs'  => [
                    'id' => '',
                ],
                'li-attrs' => [
                    'id'    => 'terms',
                    'class' => '',
                ],
                'text'     => 'Terms',
                'url'      => [
                    'prefix'     => 'admin',
                    'controller' => 'Terms',
                    'action'     => 'index'
                ],
            ],
            // PHOTOS
            //[
            //    'a-attrs'  => [
            //        'id' => '',
            //    ],
            //    'li-attrs' => [
            //        'id'    => 'photos',
            //        'class' => '',
            //    ],
            //    'text'     => 'Photos',
            //    'url'      => [
            //        'prefix'     => 'admin',
            //        'controller' => 'Photos',
            //    ],
            //],

            // USERS
            [
                'a-attrs'  => [
                    'id' => '',
                ],
                'li-attrs' => [
                    'id'    => 'users',
                    'class' => '',
                ],
                'text'     => 'Users',
                'url'      => [
                    'prefix'     => 'admin',
                    'controller' => 'Users',
                    'action'     => 'index'
                ],
                'roles' => [Admin::GOD_MODE]
            ],
        ],
    ],
    [
        'a-attrs'  => [
            'id' => '',
        ],
        'li-attrs' => [
            'id'    => 'logout',
            'class' => ''
        ],
        'text'     => $this->Html->tag('span', '', ['class' => 'im-exit']) . '&nbsp;Log Out',
        'url'      => ['_name' => 'admin-logout'],
    ],
];
