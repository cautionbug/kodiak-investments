<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Config;

use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Core\Plugin;
use Cake\Error\Debugger;
use Cake\Routing\Router;
use Cake\Routing\RouteBuilder;

/**
 * Uncomment the define below to use CakePHP prefix routes.
 *
 * The value of the define determines the names of the routes
 * and their associated controller actions:
 *
 * Set to an array of prefixes you want to use in your application. Use for
 * admin or other prefixed routes.
 *
 * Routing.prefixes = array('admin', 'manager');
 *
 * Enables:
 *  `App\Controller\Admin` and `/admin/controller/index`
 *  `App\Controller\Manager` and `/manager/controller/index`
 *
 */
//Configure::write('Routing.prefixes', ['admin']);


// Enable use of data views
Router::extensions(['json']);

// NORMAL ROUTES
Router::scope('/',
    function (RouteBuilder $routes) {
        $debug = Configure::read('debug');
        $routes->connect('/about',
                         [
                             'controller' => 'Pages',
                             'action'     => 'about'
                         ],
                         ['_name' => 'about']
        );

        $routes->connect('/browse/**',
                         [
                             'controller' => 'Listings',
                             'action' => 'index',
                         ],
                         ['_name' => 'browse']
        );

        $routes->connect('/captchas/:action/*',
                         [
                             'plugin'     => 'Thook/Websites',
                             'controller' => 'Captchas'
                         ]
        );

        // Singular URL, Plural Controller/Table
        $routes->connect('/contact',
                         ['controller' => 'Contacts'],
                         ['_name' => 'contact']
        );
        $routes->connect('/contact/remove/:id/:hash',
                         [
                             'controller' => 'Contacts',
                             'action'     => 'remove'
                         ],
                         [
                             '_name' => 'contact-remove',
                             'pass'  => ['id', 'hash'],
                             //'id' => '^\d+$',
                             //'hash' => '^[\da-zA-Z]+$'
                         ]
        );

        $routes->connect('/search/**',
                         [
                             'controller' => 'Listings',
                             'action'     => 'search',
                         ],
                         ['_name' => 'search']
        );

        /**
         * ...and connect the rest of 'Pages' controller's urls.
         */
        $routes->connect('/pages/*',
                         ['action' => 'display']
        );
        /**
         * Connect a route for any Controller index.
         * And a general catch-all for any action.
         *
         * Once specific routes are hooked up, these can be removed.
         */
        $routes->connect('/:controller',
                         ['action' => 'index'],
                         ['routeClass' => 'InflectedRoute']
        );
        $routes->connect('/:controller/:action/*',
                         [],
                         ['routeClass' => 'InflectedRoute']
        );

        /**
         * Here, we are connecting '/' (base path) to controller called 'Pages',
         * its action called 'display', and we pass a param to select the view file
         * to use (in this case, /app/View/Pages/home.ctp)...
         */
        // TODO: Convert this into the main INDEX
        $routes->connect('/',
                         [
                             'controller' => 'Pages',
                             'action'     => 'index',
                         ],
                         ['_name' => 'home']
        );
        // DEBUG-MODE ROUTES
        if ($debug) {
            $routes->connect('/admin/cake',
                             [
                                 'controller' => 'Pages',
                                 'action'     => 'display',
                                 'home'
                             ],
                             ['_name' => 'cake-info']
            );
        }
    }
);

// ADMIN ROUTES
Router::prefix('admin',
    function (RouteBuilder $routes) {
        //$debug = Configure::read('debug');
        /**
         * Connect a route for any Controller index.
         * And a general catch-all for any action.
         *
         * Once specific routes are hooked up, these can be removed.
         */
        $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
        //$routes->connect('/:controller/:action', [], ['routeClass' => 'InflectedRoute']);
        $routes->connect('/:controller/:action/*', [], ['routeClass' => 'InflectedRoute']);
        $routes->connect('/login',
                         [
                             'controller' => 'Users',
                             'action'     => 'login'
                         ],
                         ['_name' => 'admin-login']
        );
        $routes->connect('/logout',
                         [
                             'controller' => 'Users',
                             'action'     => 'logout'
                         ],
                         ['_name' => 'admin-logout']
        );
        $routes->connect('/',
                         [
                             'controller' => 'Admin',
                             'action'     => 'index'
                         ],
                         ['_name' => 'admin-index']
        );
    }
);

//Router::plugin('Thook/Websites', ['path' => '/captchas'], function($routes) {
//	$routes->connect('/:controller');
//});

// WEBSITES ROUTES
//Router::prefix('websites', function (RouteBuilder $routes) {
//	/**
//	 * Connect a route for any Controller index.
//	 * And a general catch-all for any action.
//	 *
//	 * Once specific routes are hooked up, these can be removed.
//	 */
//	$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
//	$routes->connect('/:controller/:action', [], ['routeClass' => 'InflectedRoute']);
//});

//Router::scope('/admin', ['prefix' => 'admin'], function(ScopedRouteCollection $routes) {
//	/**
//	 * Connect a route for any Controller index.
//	 * And a general catch-all for any action.
//	 *
//	 * Once specific routes are hooked up, these can be removed.
//	 */
//	$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
//	$routes->connect('/:controller/:action', [], ['routeClass' => 'InflectedRoute']);
//});

/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
try {
    Plugin::routes();
}
catch (\Exception $e) {
    die($e->getFile() . '::' . $e->getLine() . ': ' . $e->getMessage());
}

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
//require CAKE . 'Config/routes.php';
