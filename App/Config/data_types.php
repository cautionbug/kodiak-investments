<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 9/14/2014
 * Time: 8:51 PM
 */

//use App\Controller\Admin\AbstractAdminController as Admin;

$config = [
    'DataExpressions' => [
        'LatLng'           => '^\d{1,3}(?:\.\d{1,7})?$',
        'Money'            => '^(?:\d{1,3}(?:(?:\d{3})*|(?:,\d{3})*))$',
        'PasswordStrength' =>
            '/^(?:' .                                               // Start, non-capture group
            '(?=.*[A-Z]?)(?=.*[a-z])(?=.*[\d])(?=.*[[:punct:]])|' . // At least 1 from lower, digit, punct (upper optional)
            '(?=.*[A-Z])(?=.*[a-z]?)(?=.*[\d])(?=.*[[:punct:]])|' . // At least 1 from upper, digit, punct (lower optional)
            '(?=.*[A-Z])(?=.*[a-z])(?=.*[\d]?)(?=.*[[:punct:]])|' . // At least 1 from upper, lower, punct (digit optional)
            '(?=.*[A-Z])(?=.*[a-z])(?=.*[\d])(?=.*[[:punct:]]?)' .  // At least 1 from upper, lower, digit (punct optional)
            ').{8,}$/',                                             // End group, single character, at least 8 characters, End
        'Zip'              => '^\d{5}(?:-?\d{4})?$'
    ],
    'DataLists'       => [
        //'UserRoles' => Admin::$ROLES,
        'US_States' => [
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            'CA' => 'California',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'DC' => 'District Of Columbia',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            'NH' => 'New Hampshire',
            'NJ' => 'New Jersey',
            'NM' => 'New Mexico',
            'NY' => 'New York',
            'NC' => 'North Carolina',
            'ND' => 'North Dakota',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            'OR' => 'Oregon',
            'PA' => 'Pennsylvania',
            'RI' => 'Rhode Island',
            'SC' => 'South Carolina',
            'SD' => 'South Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming',
        ]
    ],
    'DataTypes'       => [
        'Bool' => [
            'Status' => [
                TRUE  => 'Active',
                FALSE => 'Inactive',
            ],
            'YesNo'  => [
                TRUE  => 'Yes',
                FALSE => 'No',
            ]
        ],
    ],
];
