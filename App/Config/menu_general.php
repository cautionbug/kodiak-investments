<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 8/2/2014
 * Time: 4:55 PM
 */

$menu = [
    // BROWSE
    [
        'a-attrs'  => [],
        'li-attrs' => [
            'id'           => 'browse',
            'class'        => '',
        ],
        'text'     => $this->Html->tag('span', '', ['class' => 'fi-magnifying-glass']) . '&nbsp;Browse Homes',
        'url'      => ['_name' => 'browse']
    ],
    // SEARCH
    //[
    //    'a-attrs'  => [
    //        'id'            => 'nav-search',
    //        'data-dropdown' => 'search-box',
    //    ],
    //    'li-attrs' => [
    //        'id'           => 'search',
    //        'class'        => '',
    //        'data-tooltip',
    //        'data-options' => 'disable_for_touch:true',
    //        'title'        => 'Click to open the Search bar',
    //    ],
    //    'text'     => $this->Html->tag('span', '', ['class' => 'fi-magnifying-glass']) . '&nbsp;Search',
    //    'url'      => NULL
    //],
    // ABOUT
    [
        'a-attrs'  => [],
        'li-attrs' => [
            'id'    => 'about',
            'class' => '',
        ],
        'text'     => $this->Html->tag('span', '', ['class' => 'fi-torsos-all-female']) . '&nbsp;About Kodiak',
        'url'      => ['_name' => 'about'],
    ],
    [
        'a-attrs'  => [],
        'li-attrs' => [
            'id'    => 'contact',
            'class' => '',
        ],
        'text'     => $this->Html->tag('span', '', ['class' => 'fi-mail']) . '&nbsp;Contact Us',
        'url'      => ['_name' => 'contact'],
    ],
    // DOCS
    [
        'a-attrs'  => [],
        'li-attrs' => [
            'id'    => 'info',
            'class' => 'not-click',
        ],
        'text'     => $this->Html->tag('span', '', ['class' => 'im-info']) . '&nbsp;Info',
        'url'      => NULL,
        // SUB-MENU
        'sub'      => [
            // PET POLICY
            [
                'a-attrs'  => [],
                'li-attrs' => [
                    'id'    => 'pets_policy',
                    'class' => '',
                ],
                'text'     => $this->Html->tag('span', '', ['class' => 'fi-paw']) . '&nbsp;Pets Policy',
                'url'      => [
                    'controller' => 'Docs',
                    'action'     => 'pets_policy',
                ],
            ],
            // PRIVACY POLICY
            [
                'a-attrs'  => [],
                'li-attrs' => [
                    'id'    => 'website_policy',
                    'class' => '',
                ],
                'text'     => $this->Html->tag('span', '', ['class' => 'fi-web']) . '&nbsp;Website Use Policy',
                'url'      => [
                    'controller' => 'Docs',
                    'action'     => 'website_policy',
                ],
            ],
        ],
    ],
];
