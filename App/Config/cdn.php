<?php
/**
 * Created by PhpStorm.
 * User: joe
 * Date: 10/20/2014
 * Time: 5:27 PM
 */

$config = [
    'CDN' => [
        // Modernizr, JQuery, FastClick, Foundation Core
        'basics'   => [
            'css' => [],
            'js'  => [
                '//cdn.jsdelivr.net/g/modernizr,jquery,fastclick,foundation(js/foundation.min.js)',
                //'libs/foundation/foundation.min'
            ],
        ],
        'ckeditor' => [
            '//cdn.ckeditor.com/4.4.5.1/standard/ckeditor.js',
        ],
        'slick' => [
            'css' => '//cdn.jsdelivr.net/jquery.slick/1.3.6/slick.css',
            'js'  => '//cdn.jsdelivr.net/jquery.slick/1.3.6/slick.min.js',
        ]
    ]
];
