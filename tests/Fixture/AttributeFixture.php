<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AttributeFixture
 *
 */
class AttributeFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = [
		'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
		'name' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'comment' => 'Attribute Name', 'precision' => null, 'fixed' => null],
		'abbrev' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'comment' => 'Abbreviation', 'precision' => null, 'fixed' => null],
		'icon_class' => ['type' => 'string', 'length' => 20, 'null' => true, 'default' => null, 'comment' => 'Attribute Icon', 'precision' => null, 'fixed' => null],
		'requires_value' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null],
		'active' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null],
		'_constraints' => [
			'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
			'u_idx_attrs_name' => ['type' => 'unique', 'columns' => ['name'], 'length' => []],
			'u_idx_attrs_abbrev' => ['type' => 'unique', 'columns' => ['abbrev'], 'length' => []],
		],
		'_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
		],
	];

/**
 * Records
 *
 * @var array
 */
	public $records = [
		[
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'abbrev' => 'Lorem ip',
			'icon_class' => 'Lorem ipsum dolor ',
			'requires_value' => 1,
			'active' => 1
		],
	];

}
