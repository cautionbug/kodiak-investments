<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ListingsPhotoFixture
 *
 */
class ListingsPhotoFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = [
		'listing_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'photo_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'caption' => ['type' => 'string', 'length' => 150, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'_indexes' => [
			'idx_listings_photos_photo_id' => ['type' => 'index', 'columns' => ['photo_id'], 'length' => []],
			'idx_listings_photos_listing_id' => ['type' => 'index', 'columns' => ['listing_id'], 'length' => []],
		],
		'_constraints' => [
			'primary' => ['type' => 'primary', 'columns' => ['listing_id', 'photo_id'], 'length' => []],
			'listings_photos_ibfk_1' => ['type' => 'foreign', 'columns' => ['photo_id'], 'references' => ['photos', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
			'listings_photos_ibfk_2' => ['type' => 'foreign', 'columns' => ['listing_id'], 'references' => ['listings', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
		],
		'_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
		],
	];

/**
 * Records
 *
 * @var array
 */
	public $records = [
		[
			'listing_id' => 1,
			'photo_id' => 1,
			'caption' => 'Lorem ipsum dolor sit amet'
		],
	];

}
