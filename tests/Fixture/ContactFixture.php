<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ContactFixture
 *
 */
class ContactFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = [
		'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
		'name' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'email' => ['type' => 'string', 'length' => 75, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'phone' => ['type' => 'string', 'length' => 10, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'source' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'comment' => 'How did you hear about us?', 'precision' => null, 'fixed' => null],
		'move_deadline' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => 'How soon do you need to move in?', 'precision' => null],
		'price_range' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'term_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'home_size' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'comment' => 'What size home?', 'precision' => null, 'fixed' => null],
		'family_size' => ['type' => 'integer', 'length' => 3, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => 'How many people are moving?', 'precision' => null, 'autoIncrement' => null],
		'home_traits' => ['type' => 'string', 'length' => 200, 'null' => true, 'default' => null, 'comment' => 'What kind of home are you looking for?', 'precision' => null, 'fixed' => null],
		'ip_address' => ['type' => 'string', 'length' => 15, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'comments' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
		'remove_hash' => ['type' => 'string', 'length' => 50, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
		'_indexes' => [
			'idx_contacts_email' => ['type' => 'index', 'columns' => ['email'], 'length' => []],
			'idx_contacts_terms_id' => ['type' => 'index', 'columns' => ['term_id'], 'length' => []],
			'idx_contacts_price_range' => ['type' => 'index', 'columns' => ['price_range'], 'length' => []],
		],
		'_constraints' => [
			'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
			'contacts_ibfk_1' => ['type' => 'foreign', 'columns' => ['term_id'], 'references' => ['terms', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
		],
		'_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
		],
	];

/**
 * Records
 *
 * @var array
 */
	public $records = [
		[
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'email' => 'Lorem ipsum dolor sit amet',
			'phone' => 'Lorem ip',
			'source' => 'Lorem ipsum dolor sit amet',
			'move_deadline' => '2014-08-31',
			'price_range' => 1,
			'term_id' => 1,
			'home_size' => 'Lorem ipsum dolor sit amet',
			'family_size' => 1,
			'home_traits' => 'Lorem ipsum dolor sit amet',
			'ip_address' => 'Lorem ipsum d',
			'comments' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'remove_hash' => 'Lorem ipsum dolor sit amet',
			'created' => '2014-08-31 19:14:04'
		],
	];

}
