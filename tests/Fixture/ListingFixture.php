<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ListingFixture
 *
 */
class ListingFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = [
		'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
		'title' => ['type' => 'string', 'length' => 200, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'street' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'comment' => 'Street Address', 'precision' => null, 'fixed' => null],
		'city' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'comment' => 'City', 'precision' => null, 'fixed' => null],
		'county' => ['type' => 'string', 'length' => 25, 'null' => true, 'default' => null, 'comment' => 'County', 'precision' => null, 'fixed' => null],
		'state' => ['type' => 'string', 'fixed' => true, 'length' => 2, 'null' => false, 'default' => null, 'comment' => 'State', 'precision' => null],
		'zip' => ['type' => 'string', 'length' => 9, 'null' => false, 'default' => null, 'comment' => 'Zip Code', 'precision' => null, 'fixed' => null],
		'lat' => ['type' => 'decimal', 'length' => 10, 'precision' => 7, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'Latitude'],
		'lng' => ['type' => 'decimal', 'length' => 10, 'precision' => 7, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'Longitude'],
		'lot_no' => ['type' => 'integer', 'length' => 8, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => 'Lot #', 'precision' => null, 'autoIncrement' => null],
		'area' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => '0', 'comment' => 'Square Footage', 'precision' => null, 'autoIncrement' => null],
		'bedrooms' => ['type' => 'integer', 'length' => 3, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'bathrooms' => ['type' => 'integer', 'length' => 3, 'unsigned' => true, 'null' => false, 'default' => '0', 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'price' => ['type' => 'decimal', 'length' => 10, 'precision' => 2, 'unsigned' => true, 'null' => false, 'default' => '0.00', 'comment' => 'List Price'],
		'house_condition_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'park_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'term_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'house_style_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'info_body' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => 'Listing Information', 'precision' => null],
		'ready_date' => ['type' => 'date', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
		'active' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null],
		'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
		'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
		'_indexes' => [
			'idx_listings_title' => ['type' => 'index', 'columns' => ['title'], 'length' => []],
			'idx_listings_area' => ['type' => 'index', 'columns' => ['area'], 'length' => []],
			'idx_listings_condition_id' => ['type' => 'index', 'columns' => ['house_condition_id'], 'length' => []],
			'idx_listings_parks_id' => ['type' => 'index', 'columns' => ['park_id'], 'length' => []],
			'idx_listings_terms_id' => ['type' => 'index', 'columns' => ['term_id'], 'length' => []],
			'idx_listings_type_id' => ['type' => 'index', 'columns' => ['house_style_id'], 'length' => []],
			'ft_idx_listings_info_body' => ['type' => 'fulltext', 'columns' => ['info_body'], 'length' => []],
		],
		'_constraints' => [
			'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
			'fk_listings__house_conditions_id' => ['type' => 'foreign', 'columns' => ['house_condition_id'], 'references' => ['house_conditions', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
			'fk_listings__parks_id' => ['type' => 'foreign', 'columns' => ['park_id'], 'references' => ['parks', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
			'fk_listings__terms_id' => ['type' => 'foreign', 'columns' => ['term_id'], 'references' => ['terms', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
			'fk_listings__house_styles_id' => ['type' => 'foreign', 'columns' => ['house_style_id'], 'references' => ['house_styles', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
		],
		'_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
		],
	];

/**
 * Records
 *
 * @var array
 */
	public $records = [
		[
			'id' => 1,
			'title' => 'Lorem ipsum dolor sit amet',
			'street' => 'Lorem ipsum dolor sit amet',
			'city' => 'Lorem ipsum dolor sit amet',
			'county' => 'Lorem ipsum dolor sit a',
			'state' => '',
			'zip' => 'Lorem i',
			'lat' => '',
			'lng' => '',
			'lot_no' => 1,
			'area' => 1,
			'bedrooms' => 1,
			'bathrooms' => 1,
			'price' => '',
			'house_condition_id' => 1,
			'park_id' => 1,
			'term_id' => 1,
			'house_style_id' => 1,
			'info_body' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'ready_date' => '2014-09-12',
			'active' => 1,
			'created' => '2014-09-12 00:13:34',
			'modified' => '2014-09-12 00:13:34'
		],
	];

}
