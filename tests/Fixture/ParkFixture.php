<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ParkFixture
 *
 */
class ParkFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = [
		'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
		'name' => ['type' => 'string', 'length' => 100, 'null' => false, 'default' => null, 'comment' => 'Park Name', 'precision' => null, 'fixed' => null],
		'street' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'comment' => 'Street Address', 'precision' => null, 'fixed' => null],
		'city' => ['type' => 'string', 'length' => 50, 'null' => false, 'default' => null, 'comment' => 'City', 'precision' => null, 'fixed' => null],
		'county' => ['type' => 'string', 'length' => 25, 'null' => false, 'default' => null, 'comment' => 'County', 'precision' => null, 'fixed' => null],
		'state' => ['type' => 'string', 'fixed' => true, 'length' => 2, 'null' => false, 'default' => null, 'comment' => 'State', 'precision' => null],
		'zip' => ['type' => 'string', 'length' => 9, 'null' => false, 'default' => null, 'comment' => 'Zip Code', 'precision' => null, 'fixed' => null],
		'lat' => ['type' => 'decimal', 'length' => 10, 'precision' => 7, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'Latitude'],
		'lng' => ['type' => 'decimal', 'length' => 10, 'precision' => 7, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => 'Longitude'],
		'info_body' => ['type' => 'text', 'length' => null, 'null' => true, 'default' => null, 'comment' => 'Park Information', 'precision' => null],
		'file_name_no_ext' => ['type' => 'string', 'length' => 100, 'null' => true, 'default' => null, 'comment' => 'Park Map File Name (ext removed)', 'precision' => null, 'fixed' => null],
		'active' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null],
		'created' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
		'modified' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
		'_indexes' => [
			'ft_idx_parks_info_body' => ['type' => 'fulltext', 'columns' => ['info_body'], 'length' => []],
		],
		'_constraints' => [
			'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
			'u_idx_parks_file_name' => ['type' => 'unique', 'columns' => ['file_name_no_ext'], 'length' => []],
		],
		'_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
		],
	];

/**
 * Records
 *
 * @var array
 */
	public $records = [
		[
			'id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'street' => 'Lorem ipsum dolor sit amet',
			'city' => 'Lorem ipsum dolor sit amet',
			'county' => 'Lorem ipsum dolor sit a',
			'state' => '',
			'zip' => 'Lorem i',
			'lat' => '',
			'lng' => '',
			'info_body' => 'Lorem ipsum dolor sit amet, aliquet feugiat. Convallis morbi fringilla gravida, phasellus feugiat dapibus velit nunc, pulvinar eget sollicitudin venenatis cum nullam, vivamus ut a sed, mollitia lectus. Nulla vestibulum massa neque ut et, id hendrerit sit, feugiat in taciti enim proin nibh, tempor dignissim, rhoncus duis vestibulum nunc mattis convallis.',
			'file_name_no_ext' => 'Lorem ipsum dolor sit amet',
			'active' => 1,
			'created' => '2014-09-12 00:13:51',
			'modified' => '2014-09-12 00:13:51'
		],
	];

}
