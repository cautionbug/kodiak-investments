<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ListingsAttributesFixture
 *
 */
class ListingsAttributesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'listing_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'attribute_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'value' => ['type' => 'string', 'length' => 250, 'null' => true, 'default' => null, 'comment' => '{{name}} Value', 'precision' => null, 'fixed' => null],
        '_indexes' => [
            'idx_listings_attributes_listing_id' => ['type' => 'index', 'columns' => ['listing_id'], 'length' => []],
            'idx_listings_attributes_attribute_id' => ['type' => 'index', 'columns' => ['attribute_id'], 'length' => []],
            'idx_listings_attributes_value' => ['type' => 'index', 'columns' => ['value'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'u_idx_listings_attributes_listing_attr' => ['type' => 'unique', 'columns' => ['attribute_id', 'listing_id'], 'length' => []],
            'fk_listings_attributes__attributes' => ['type' => 'foreign', 'columns' => ['attribute_id'], 'references' => ['attributes', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
            'fk_listings_attributes__listings' => ['type' => 'foreign', 'columns' => ['listing_id'], 'references' => ['listings', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'listing_id' => 1,
            'attribute_id' => 1,
            'value' => 'Lorem ipsum dolor sit amet'
        ],
    ];
}
