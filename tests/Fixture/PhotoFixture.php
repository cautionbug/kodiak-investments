<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PhotoFixture
 *
 */
class PhotoFixture extends TestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = [
		'id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
		'mime_type_id' => ['type' => 'integer', 'length' => 10, 'unsigned' => true, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
		'file_path' => ['type' => 'string', 'length' => 200, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'fixed' => null],
		'hash' => ['type' => 'string', 'length' => 64, 'null' => true, 'default' => null, 'comment' => 'SHA256 Hash to check for duplicates', 'precision' => null, 'fixed' => null],
		'_indexes' => [
			'idx_photos_mime_type_id' => ['type' => 'index', 'columns' => ['mime_type_id'], 'length' => []],
		],
		'_constraints' => [
			'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
			'file_path' => ['type' => 'unique', 'columns' => ['file_path'], 'length' => []],
			'photos_ibfk_1' => ['type' => 'foreign', 'columns' => ['mime_type_id'], 'references' => ['mime_types', 'id'], 'update' => 'cascade', 'delete' => 'restrict', 'length' => []],
		],
		'_options' => [
'engine' => 'InnoDB', 'collation' => 'utf8_general_ci'
		],
	];

/**
 * Records
 *
 * @var array
 */
	public $records = [
		[
			'id' => 1,
			'mime_type_id' => 1,
			'file_path' => 'Lorem ipsum dolor sit amet',
			'hash' => 'Lorem ipsum dolor sit amet'
		],
	];

}
