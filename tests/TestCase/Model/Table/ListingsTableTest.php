<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ListingsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ListingsTable Test Case
 */
class ListingsTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'Listings' => 'app.listings',
        'HouseConditions' => 'app.house_conditions',
        'Parks' => 'app.parks',
        'Terms' => 'app.terms',
        'Contacts' => 'app.contacts',
        'HouseStyles' => 'app.house_styles',
        'Attributes' => 'app.attributes',
        'ListingsAttributes' => 'app.listings_attributes',
        'Photos' => 'app.photos',
        'MimeTypes' => 'app.mime_types',
        'ListingsPhotos' => 'app.listings_photos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Listings') ? [] : ['className' => 'App\Model\Table\ListingsTable'];        $this->Listings = TableRegistry::get('Listings', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Listings);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
