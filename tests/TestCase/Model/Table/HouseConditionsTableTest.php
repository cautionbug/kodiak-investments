<?php
namespace App\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use App\Model\Table\HouseConditionsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HouseConditionsTable Test Case
 */
class HouseConditionsTableTest extends TestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'app.house_condition'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('HouseConditions') ? [] : ['className' => 'App\Model\Table\HouseConditionsTable'];
		$this->HouseConditions = TableRegistry::get('HouseConditions', $config);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->HouseConditions);

		parent::tearDown();
	}

}
