<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ListingsAttributesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ListingsAttributesTable Test Case
 */
class ListingsAttributesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'ListingsAttributes' => 'app.listings_attributes',
        'Listings' => 'app.listings',
        'HouseConditions' => 'app.house_conditions',
        'Parks' => 'app.parks',
        'Terms' => 'app.terms',
        'Contacts' => 'app.contacts',
        'HouseStyles' => 'app.house_styles',
        'Attributes' => 'app.attributes',
        'Photos' => 'app.photos',
        'MimeTypes' => 'app.mime_types',
        'ListingsPhotos' => 'app.listings_photos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ListingsAttributes') ? [] : ['className' => 'App\Model\Table\ListingsAttributesTable'];        $this->ListingsAttributes = TableRegistry::get('ListingsAttributes', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ListingsAttributes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
