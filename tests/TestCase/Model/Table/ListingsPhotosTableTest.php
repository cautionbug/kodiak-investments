<?php
namespace App\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use App\Model\Table\ListingsPhotosTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ListingsPhotosTable Test Case
 */
class ListingsPhotosTableTest extends TestCase {

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('ListingsPhotos') ? [] : ['className' => 'App\Model\Table\ListingsPhotosTable'];
		$this->ListingsPhotos = TableRegistry::get('ListingsPhotos', $config);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->ListingsPhotos);

		parent::tearDown();
	}

}
