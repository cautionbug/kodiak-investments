<?php
namespace App\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use App\Model\Table\TermsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TermsTable Test Case
 */
class TermsTableTest extends TestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'app.term',
		'app.contact',
		'app.listing',
		'app.house_condition',
		'app.park',
		'app.house_style',
		'app.photo',
		'app.mime_type',
		'app.listings_photo',
		'app.parks_photo'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('Terms') ? [] : ['className' => 'App\Model\Table\TermsTable'];
		$this->Terms = TableRegistry::get('Terms', $config);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Terms);

		parent::tearDown();
	}

}
