<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AttributesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AttributesTable Test Case
 */
class AttributesTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'Attributes' => 'app.attributes',
        'Listings' => 'app.listings',
        'HouseConditions' => 'app.house_conditions',
        'Parks' => 'app.parks',
        'Terms' => 'app.terms',
        'Contacts' => 'app.contacts',
        'HouseStyles' => 'app.house_styles',
        'ListingsAttributes' => 'app.listings_attributes',
        'Photos' => 'app.photos',
        'MimeTypes' => 'app.mime_types',
        'ListingsPhotos' => 'app.listings_photos'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Attributes') ? [] : ['className' => 'App\Model\Table\AttributesTable'];        $this->Attributes = TableRegistry::get('Attributes', $config);    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Attributes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
