<?php
namespace App\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use App\Model\Table\HouseStylesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HouseStylesTable Test Case
 */
class HouseStylesTableTest extends TestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'app.house_style',
		'app.listing',
		'app.house_condition',
		'app.park',
		'app.photo',
		'app.mime_type',
		'app.listings_photo',
		'app.parks_photo',
		'app.attr',
		'app.parks_attr',
		'app.term',
		'app.contact'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('HouseStyles') ? [] : ['className' => 'App\Model\Table\HouseStylesTable'];
		$this->HouseStyles = TableRegistry::get('HouseStyles', $config);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->HouseStyles);

		parent::tearDown();
	}

}
