<?php
namespace App\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use App\Model\Table\ParksTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ParksTable Test Case
 */
class ParksTableTest extends TestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'app.park',
		'app.listing',
		'app.house_condition',
		'app.term',
		'app.contact',
		'app.house_style',
		'app.photo',
		'app.mime_type',
		'app.listings_photo',
		'app.parks_photo',
		'app.attr',
		'app.parks_attr'
	];

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$config = TableRegistry::exists('Parks') ? [] : ['className' => 'App\Model\Table\ParksTable'];
		$this->Parks = TableRegistry::get('Parks', $config);
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Parks);

		parent::tearDown();
	}

}
