<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\PhotosController;
use Cake\TestSuite\ControllerTestCase;

/**
 * App\Controller\Admin\PhotosController Test Case
 */
class PhotosControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'app.photo',
		'app.mime_type',
		'app.listing',
		'app.house_condition',
		'app.park',
		'app.parks_photo',
		'app.attr',
		'app.parks_attr',
		'app.term',
		'app.contact',
		'app.house_style',
		'app.listings_photo'
	];

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
