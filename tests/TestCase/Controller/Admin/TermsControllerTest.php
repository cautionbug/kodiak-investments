<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\TermsController;
use Cake\TestSuite\ControllerTestCase;

/**
 * App\Controller\Admin\TermsController Test Case
 */
class TermsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = [
		'app.term',
		'app.contact',
		'app.listing',
		'app.house_condition',
		'app.park',
		'app.photo',
		'app.mime_type',
		'app.listings_photo',
		'app.parks_photo',
		'app.attr',
		'app.parks_attr',
		'app.house_style'
	];

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
