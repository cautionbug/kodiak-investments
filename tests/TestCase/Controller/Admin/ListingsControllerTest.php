<?php
namespace App\Test\TestCase\Controller\Admin;

use App\Controller\Admin\ListingsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\Admin\ListingsController Test Case
 */
class ListingsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'Listings' => 'app.listings',
        'HouseConditions' => 'app.house_conditions',
        'Parks' => 'app.parks',
        'Terms' => 'app.terms',
        'Contacts' => 'app.contacts',
        'HouseStyles' => 'app.house_styles',
        'Attributes' => 'app.attributes',
        'ListingsAttributes' => 'app.listings_attributes',
        'Photos' => 'app.photos',
        'MimeTypes' => 'app.mime_types',
        'ListingsPhotos' => 'app.listings_photos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
